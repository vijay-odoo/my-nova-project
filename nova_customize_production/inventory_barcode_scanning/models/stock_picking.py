# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.exceptions import Warning


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    barcode = fields.Char(string='Barcode')

    @api.onchange('barcode')
    def barcode_scanning(self):
        barcode = False
        lot_number = False
        stock_producttion_obj = self.env['stock.production.lot']
        stock_pack_operation_obj = self.env['stock.pack.operation.lot']  # lot_id
        if self.barcode:
            add_qty = False
            barcode_list = self.barcode.split(" ")
            if len(barcode_list) == 2:
                barcode = barcode_list[0]
                lot_number = barcode_list[1]
            else:
                barcode = barcode_list[0]
            if barcode and self.pack_operation_product_ids:
                for line in self.pack_operation_product_ids.filtered(lambda l: l.product_qty != l.qty_done):
                    if not line.product_qty == line.qty_done and not line.product_qty < line.qty_done:
                        if barcode and lot_number and line.lots_visible:
                            lot_ids = line.pack_lot_ids.filtered(
                                lambda l: l.lot_id and l.lot_id.product_id and l.lot_id.product_id.barcode == barcode
                                and l.lot_id.name == lot_number and l.lot_id.product_id.barcode == line.product_id.barcode
                            )
                            if lot_ids:
                                line.qty_done += 1
                                lot_ids[0].do_plus()
                                self.barcode = None
                                add_qty = True
                                break
                            else:
                                production_barcode_lot = stock_producttion_obj.search([
                                    ('name', '=', lot_number),
                                    ('product_id.barcode', '=', barcode),
                                    ('product_id.barcode', '=', line.product_id.barcode)
                                ])
                                if production_barcode_lot:
                                    lot = stock_pack_operation_obj.create({
                                        'lot_id': production_barcode_lot and production_barcode_lot[0] and production_barcode_lot[0].id or False,
                                        'qty': 0,
                                        'lot_name': production_barcode_lot and production_barcode_lot[0] and production_barcode_lot[0].name or '',
                                        'operation_id': line and line.id or False,
                                    })
                                    line.qty_done += 1
                                    lot.do_plus()
                                    self.barcode = None
                                    add_qty = True
                                    break
                        elif barcode and line.lots_visible:
                            # Only Barcode
                            lot_ids = line.pack_lot_ids.filtered(
                                lambda l: l.lot_id and l.lot_id.product_id and l.lot_id.product_id.barcode == barcode and l.lot_id.product_id.barcode == line.product_id.barcode
                            )
                            if lot_ids:
                                line.qty_done += 1
                                lot_ids[0].do_plus()
                                self.barcode = None
                                add_qty = True
                                break

                            # Only lot number
                            lot_number_ids = line.pack_lot_ids.filtered(
                                lambda l: l.lot_id and l.lot_id.name == barcode and l.lot_id.product_id == line.product_id and l.lot_id.product_id.barcode == line.product_id.barcode
                            )
                            if lot_number_ids:
                                line.qty_done += 1
                                lot_number_ids[0].do_plus()
                                self.barcode = None
                                add_qty = True
                                break
                            else:
                                production_barcode_lot = stock_producttion_obj.search([
                                    ('name', '=', barcode),
                                    ('product_id', '=', line.product_id.id),
                                    ('product_id.barcode', '=', line.product_id.barcode)
                                ])
                                if production_barcode_lot:
                                    lot = stock_pack_operation_obj.create({
                                        'lot_id': production_barcode_lot and production_barcode_lot[0] and production_barcode_lot[0].id or False,
                                        'qty': 0,
                                        'lot_name': production_barcode_lot and production_barcode_lot[0] and production_barcode_lot[0].name or '',
                                        'operation_id': line and line.id or False,
                                    })
                                    line.qty_done += 1
                                    lot.do_plus()
                                    self.barcode = None
                                    add_qty = True
                                    break
                                else:
                                    if line.product_id and line.product_id.barcode == barcode:
                                        line.qty_done += 1
                                        self.barcode = None
                                        add_qty = True
                                        break
                        elif barcode and not line.lots_visible:
                            if line.product_id and line.product_id.barcode == barcode:
                                line.qty_done += 1
                                self.barcode = None
                                add_qty = True
                                break

                if not add_qty:
                    for line in self.pack_operation_product_ids.filtered(lambda l: l.product_qty == l.qty_done):
                        if barcode and lot_number and line.lots_visible:
                            lot_ids = line.pack_lot_ids.filtered(
                                lambda l: l.lot_id and l.lot_id.product_id and l.lot_id.product_id.barcode == barcode
                                and l.lot_id.name == lot_number and l.lot_id.product_id.barcode == line.product_id.barcode
                            )
                            if lot_ids:
                                return {
                                    'warning': {
                                        'title': 'Warning',
                                        'message': 'This product lot is already assigned.',
                                    },
                                    'value': {
                                        'barcode': None
                                    },
                                }
                                # self.barcode = None
                                # raise Warning(
                                #     'This product lot is already assigned.'
                                # )
                        elif barcode and line.lots_visible:
                            lot_ids = line.pack_lot_ids.filtered(
                                lambda l: l.lot_id and l.lot_id.product_id and l.lot_id.product_id.barcode == barcode
                                and l.lot_id.product_id.barcode == line.product_id.barcode
                            )
                            if lot_ids:
                                return {
                                    'warning': {
                                        'title': 'Warning',
                                        'message': 'This product lot is already assigned.',
                                    },
                                    'value': {
                                        'barcode': None
                                    },
                                }
                                # self.barcode = None
                                # raise Warning(
                                #     'This product lot is already assigned.'
                                # )
                            lot_number_ids = line.pack_lot_ids.filtered(
                                lambda l: l.lot_id and l.lot_id.name == barcode and l.lot_id.product_id == line.product_id
                            )
                            if lot_number_ids:
                                return {
                                    'warning': {
                                        'title': 'Warning',
                                        'message': 'This product lot is already assigned.',
                                    },
                                    'value': {
                                        'barcode': None
                                    },
                                }
                                # self.barcode = None
                                # raise Warning(
                                #     'This product lot is already assigned.'
                                # )
                        elif barcode and not line.lots_visible:
                            if line.product_id and line.product_id.barcode == barcode:
                                return {
                                    'warning': {
                                        'title': 'Warning',
                                        'message': 'This product lot is already assigned.',
                                    },
                                    'value': {
                                        'barcode': None
                                    },
                                }
                                # self.barcode = None
                                # raise Warning(
                                #     'This product lot is already assigned.'
                                # )
                    return {
                        'warning': {
                            'title': 'Warning',
                            'message': 'This barcode or lot number is not available in shipping.',
                        },
                        'value': {
                            'barcode': None
                        },
                    }
                    # self.barcode = None
                    # raise Warning('This barcode or lot number is not available in shipping.')
            # else:
            #     self.barcode = None
            #     raise Warning(
            #         'This product is not available in the order.'
            #         'You can add this product by clicking the "Add an item" and scan')


class StockPickingOperation(models.Model):
    _inherit = 'stock.pack.operation'

    barcode = fields.Char(string='Barcode')

    @api.onchange('barcode')
    def _onchange_barcode_scan(self):
        product_rec = self.env['product.product']
        if self.barcode:
            product = product_rec.search([('barcode', '=', self.barcode)])
            self.product_id = product.id

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
