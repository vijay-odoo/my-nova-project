# -*- coding: utf-8 -*-

from odoo import models, api, fields


class wiz_delivery_method(models.Model):
    _name = "wiz.delivery.method"
    _rec_name = 'sale_order'

    sale_order = fields.Many2one("sale.order", string="Sale Order")
    delivery_method_rates_line = fields.One2many(
        'wiz.delivery.method.rates', 'wiz_delivery_id', string="lines"
    )
    currency_id = fields.Many2one('res.currency', string='Currency')

    @api.multi
    def apply_develivery_rules_wiz(self):
        for rec in self:
            delivery_carriers = rec.sale_order.with_context(delivery_backend=True)._get_delivery_methods().with_context(order_id=rec.sale_order.id)
            for delivery_carrier in delivery_carriers:
                vals = {
                    'delivery_method': delivery_carrier.id,
                    'delivery_rates': delivery_carrier.price,
                    'sale_order': rec.sale_order.id,
                    'wiz_delivery_id': rec.id
                }
                if delivery_carrier.fedex_servicetype.code == 'GROUND_HOME_DELIVERY' or delivery_carrier.is_ground_delivery and rec.sale_order.amount_total >= delivery_carrier.free_delivery_above:
                    vals.update({
                        'delivery_rates': 0.0,
                    })
                self.env['wiz.delivery.method.rates'].create(vals)
            return True


class wiz_delivery_method_rates(models.Model):
    _name = "wiz.delivery.method.rates"

    delivery_method = fields.Many2one("delivery.carrier", string="Delivery Method")
    delivery_rates = fields.Float(string="Delivery Rates")
    sale_order = fields.Many2one("sale.order", string="Sale Order")
    wiz_delivery_id = fields.Many2one("wiz.delivery.method", string="Wizard Delivery Method")

    @api.multi
    def set_delivery_rates_wiz(self):
        for rec in self:
            if rec.sale_order:
                rec.sale_order.with_context(order_id=rec.sale_order.id).write({
                    'carrier_id': rec.delivery_method and rec.delivery_method.id or False
                })
                rec.sale_order.with_context(order_id=rec.sale_order.id)._compute_delivery_price()
                rec.sale_order.with_context(order_id=rec.sale_order.id).delivery_set()
        # return {
        #     'type': 'ir.actions.client',
        #     'tag': 'reload',
        # }
        return {
            'name': 'Delivery Rates',
            'res_model': 'sale.order',
            'view_id': self.env.ref('sale.view_order_form').id,
            'type': 'ir.actions.act_window',
            'res_id': rec.sale_order and rec.sale_order.id or False,
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(self.env.ref('sale.view_order_form').id, 'form')],
        }
