# -*- coding: utf-8 -*-

{
    'name': 'Delivery Method customize',
    'version': '1.0',
    'category': 'Sales',
    'description': """
Delivery Method customize
====================================
    """,
    'depends': ['account', 'sale', 'delivery', 'odoo_shipping_service_apps'],
    'data': [
        'views/sale_views.xml',
        'wizard/views/delivery_view.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
