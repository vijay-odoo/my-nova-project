# -*- coding: utf-8 -*-
from odoo.addons.delivery.models.sale_order import SaleOrder
from odoo import models, api, _
from odoo.exceptions import UserError


@api.multi
def delivery_set(self):
    # Remove delivery products from the sale order
    self._delivery_unset()

    for order in self:
        carrier = order.carrier_id
        if carrier:
            if order.state not in ('draft', 'sent'):
                raise UserError(_('The order state have to be draft to add delivery lines.'))

            if carrier.delivery_type not in ['fixed', 'base_on_rule']:
                # Shipping providers are used when delivery_type is other than 'fixed' or 'base_on_rule'
                price_unit = order.carrier_id.get_shipping_price_from_so(order)[0]
            else:
                # Classic grid-based carriers
                carrier = order.carrier_id.verify_carrier(order.partner_shipping_id)
                if not carrier:
                    raise UserError(_('No carrier matching.'))
                price_unit = carrier.get_price_available(order)
                if order.company_id.currency_id.id != order.pricelist_id.currency_id.id:
                    price_unit = order.company_id.currency_id.with_context(date=order.date_order).compute(price_unit, order.pricelist_id.currency_id)

            if carrier.fedex_servicetype.code == 'GROUND_HOME_DELIVERY' or carrier.is_ground_delivery and order.amount_total >= carrier.free_delivery_above:
                final_price = 0.0
            else:
                final_price = price_unit * (1.0 + (float(self.carrier_id.margin) / 100.0))
            order._create_delivery_line(carrier, final_price)

        else:
            raise UserError(_('No carrier set for this order.'))

    return True


SaleOrder.delivery_set = delivery_set


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.multi
    def get_delivery_rates_wiz(self):
        for rec in self:
            wiz_id = self.env['wiz.delivery.method'].create({
                'sale_order': rec.id,
                'currency_id': rec.currency_id and rec.currency_id.id or False
            })
            view_id = self.env.ref('delivery_method_wiz.view_wiz_delivery_method_form').id
            wiz_id.apply_develivery_rules_wiz()
            return {
                'name': 'Delivery Rates',
                'res_model': 'wiz.delivery.method',
                'view_id': view_id,
                'type': 'ir.actions.act_window',
                'res_id': wiz_id and wiz_id.id or False,
                'context': self.env.context,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'views': [(view_id, 'form')],
            }
