    # -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.addons.sale.models.sale import SaleOrderLine
from odoo.addons.sale.models.sale import SaleOrder
from odoo.exceptions import UserError
from odoo.tools import float_compare
from odoo.tools import float_is_zero
from datetime import datetime


@api.depends('order_line.price_total')
def _amount_all(self):
    """
    Compute the total amounts of the SO.
    """
    for order in self:
        amount_untaxed = amount_tax = 0.0
        for line in order.order_line:
            if not line.is_delivery:
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
        order.update({
            'amount_untaxed': (order.pricelist_id.currency_id.round(amount_untaxed) + abs(order.discount_amount)),
            'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
            'amount_total': amount_untaxed + amount_tax + order.delivery_amount,
        })


SaleOrder._amount_all = _amount_all


@api.model
def create(self, values):
    onchange_fields = ['name', 'price_unit', 'product_uom', 'tax_id']
    if values.get('order_id') and values.get('product_id') and any(f not in values for f in onchange_fields):
        line = self.new(values)
        line.product_id_change()
        for field in onchange_fields:
            if field not in values:
                values[field] = line._fields[field].convert_to_write(line[field], line)
    line = super(SaleOrderLine, self).create(values)
    if line.invoice_status == 'invoiced':
        line._action_procurement_create()
    return line

SaleOrderLine.create = create


@api.multi
def write(self, values):
    lines = False
    if 'product_uom_qty' in values:
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        lines = self.filtered(
            lambda r: r.invoice_status == 'invoiced' and float_compare(r.product_uom_qty, values['product_uom_qty'], precision_digits=precision) == -1
        )
    result = super(SaleOrderLine, self).write(values)
    if lines:
        lines._action_procurement_create()
    return result


SaleOrderLine.write = write


@api.multi
def action_confirm(self):
    for order in self:
        order.state = 'sale'
        order.confirmation_date = fields.Datetime.now()
        # if self.env.context.get('send_email'):
        #self.force_quotation_send()
    if self.env['ir.values'].get_default('sale.config.settings', 'auto_done_setting'):
        self.action_done()
    return True

SaleOrder.action_confirm = action_confirm


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_invoice_paid(self):
        res = super(AccountInvoice, self).action_invoice_paid()
        sale_order_id = self.env['sale.order'].search([('name', '=', self.origin)])
        if sale_order_id and sale_order_id.invoice_status == 'invoiced' and not sale_order_id[0].is_picking:
            sale_order_id[0].order_line._action_procurement_create()
            sale_order_id[0].is_picking = True
        elif self.origin:
            query = "select id from sale_order where name = '%s'" % self.origin
            self.env.cr.execute(query)
            sale_order_ids = self.env.cr.fetchone()
            if sale_order_ids:
                sale_order_id = self.env['sale.order'].browse(int(sale_order_ids[0]))
                sale_order_id[0].order_line._action_procurement_create()
                sale_order_id[0].is_picking = True
        return res


class saleorder(models.Model):
    _inherit = 'sale.order'

    @api.depends('order_line.price_total')
    def _delivery_amount_all(self):
        """
        Compute the total Delivery amounts of the SO.
        """
        for order in self:
            delivery_amount = 0.0
            discount_amount = 0.0
            subtotal_amount = 0.0
            for line in order.order_line:
                if line.is_delivery:
                    delivery_amount += line.price_subtotal
                if line.promotion_line:
                    discount_amount += abs(line.price_subtotal)
                if not (line.is_delivery or line.promotion_line):
                    subtotal_amount += abs(line.price_subtotal)
            order.update({
                'delivery_amount': delivery_amount,
                'discount_amount': discount_amount,
                'subtotal_amount': subtotal_amount
            })

    is_picking = fields.Boolean(string='is picking', default=False)
    is_invoiced = fields.Boolean(string='is invoiced', compute="_get_invoiced_status")
    is_payment_done = fields.Boolean(string='is payment done', compute="_get_invoiced_status")
    delivery_amount = fields.Monetary(
        string='Delivery Amount', store=True,
        readonly=True, compute='_delivery_amount_all',
        track_visibility='always'
    )
    subtotal_amount = fields.Monetary(
        string='Subtotal', store=True,
        readonly=True, compute='_delivery_amount_all',
        track_visibility='always'
    )
    discount_amount = fields.Monetary(
        string='Discount Amount', store=True,
        readonly=True, compute='_delivery_amount_all',
        track_visibility='always'
    )

    @api.onchange('user_id')
    def _onchange_user_id(self):
        if self.user_id:
            team_ids = self.env['crm.team'].search([
                ('member_ids', 'in', self.user_id.id)
            ])
            if team_ids:
                self.team_id = team_ids and team_ids[0].id or False
        elif not self.team_id:
            self.team_id = False
        else:
            self.team_id = False

    @api.one
    def _get_invoiced_status(self):
        is_payment_done = False
        for order in self:
            invoices = self.env['account.invoice']
            for line in order.order_line:
                invoices |= line.invoice_lines.mapped('invoice_id')

            refund_ids = self.env['account.invoice'].browse()
            if invoices:
                for inv in invoices:
                    refund_ids += refund_ids.search([('type', '=', 'out_refund'), ('origin', '=', inv.number), ('origin', '!=', False), ('journal_id', '=', inv.journal_id.id)])
            invoices = invoices + refund_ids
            if invoices:
                is_payment_done = invoices.filtered(lambda r: r.state == 'paid')
                if is_payment_done:
                    order.is_payment_done = True
                order.is_invoiced = True

    @api.multi
    def action_unconfirm(self):
        for rec in self:
            invoice_ids = rec.order_line.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            if not invoice_ids:
                rec.action_cancel()
                rec.action_draft()
            else:
                raise UserError(_('You can not unconfirm a sales order !. Try to cancel it before.'))

    @api.multi
    def action_cancel(self):
        res = super(saleorder, self).action_cancel()
        for rec in self:
            invoice_ids = rec.order_line.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            if invoice_ids:
                for invoice_id in invoice_ids:
                    if invoice_id.state == 'paid':
                        if invoice_id.payment_move_line_ids:
                            for payment in invoice_id.payment_move_line_ids:
                                payment_currency_id = False
                                if invoice_id.type in ('out_invoice', 'in_refund'):
                                    amount = sum([p.amount for p in payment.matched_debit_ids if p.debit_move_id in invoice_id.move_id.line_ids])
                                    amount_currency = sum([p.amount_currency for p in payment.matched_debit_ids if p.debit_move_id in invoice_id.move_id.line_ids])
                                    if payment.matched_debit_ids:
                                        payment_currency_id = all([p.currency_id == payment.matched_debit_ids[0].currency_id for p in payment.matched_debit_ids]) and payment.matched_debit_ids[0].currency_id or False
                                elif invoice_id.type in ('in_invoice', 'out_refund'):
                                    amount = sum([p.amount for p in payment.matched_credit_ids if p.credit_move_id in invoice_id.move_id.line_ids])
                                    amount_currency = sum([p.amount_currency for p in payment.matched_credit_ids if p.credit_move_id in invoice_id.move_id.line_ids])
                                    if payment.matched_credit_ids:
                                        payment_currency_id = all([p.currency_id == payment.matched_credit_ids[0].currency_id for p in payment.matched_credit_ids]) and payment.matched_credit_ids[0].currency_id or False
                                # get the payment value in invoice currency
                                if payment_currency_id and payment_currency_id == invoice_id.currency_id:
                                    amount_to_show = amount_currency
                                else:
                                    amount_to_show = payment.company_id.currency_id.with_context(date=payment.date).compute(amount, invoice_id.currency_id)
                                if float_is_zero(amount_to_show, precision_rounding=invoice_id.currency_id.rounding):
                                    continue
                                if payment.move_id:
                                    for line in payment.move_id.line_ids:
                                        line.with_context(invoice_id=invoice_id.id).remove_move_reconcile()
                    invoice_id.action_invoice_cancel()
        return res

    @api.multi
    def action_confirm(self):
        res = super(saleorder, self).action_confirm()
        for rec in self:
            template = self.env.ref('sale_order_customize.email_template_confirm_sale_order', raise_if_not_found=False)
            if template:
                template.send_mail(rec.id, force_send=True)
            payment_term_30_days = self.env.ref('account.account_payment_term_net').id
            payment_term_15_days = self.env.ref('account.account_payment_term_15days').id
            if rec.state == 'sale'\
                and rec.payment_term_id and \
                    rec.payment_term_id.id in [payment_term_30_days, payment_term_15_days] and \
                    not rec.is_picking:
                        rec.order_line._action_procurement_create()
                        rec.is_picking = True
        return res

    # @api.multi
    # def action_picking_forced(self):
    #     payment_term_30_days = self.env.ref('account.account_payment_term_net').id
    #     payment_term_15_days = self.env.ref('account.account_payment_term_15days').id
    #     for rec in self:
    #         if rec.is_picking:
    #             raise UserError(_('Picking Already Created.'))
    #         if rec.state in ['draft', 'sent']:
    #             raise UserError(_('Please Confirm order first.'))
    #         if rec.payment_term_id.id not in [payment_term_30_days, payment_term_15_days]:
    #             raise UserError(_('Order have no longer payment term so please pay full payment of order.'))
    #         if rec.state == 'sale'\
    #             and rec.payment_term_id and \
    #                 rec.payment_term_id.id in [payment_term_30_days, payment_term_15_days] and \
    #                 not rec.is_picking:
    #                     rec.order_line._action_procurement_create()
    #                     rec.is_picking = True
    #     return True


class saleorderline(models.Model):
    _inherit = 'sale.order.line'

    barcode = fields.Char('Barcode')

    @api.onchange('barcode')
    def _onchange_barcode(self):
        if self.barcode:
            product_ids = self.env['product.product'].search([('barcode', 'ilike', self.barcode)])
            if product_ids:
                self.product_id = product_ids and product_ids[0].id or False
                self.product_id_change()
            else:
                raise UserError(_("No product is available for this barcode."))

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        res = super(saleorderline, self).product_id_change()
        for rec in self:
            if rec.product_id:
                rec.barcode = rec.product_id and rec.product_id.barcode or ''
        return res


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.multi
    def name_get(self):
        # TDE: this could be cleaned a bit I think

        def _name_get(d):
            name = d.get('name', '')
            name = '%s' % (name)
            return (d['id'], name)

        partner_id = self._context.get('partner_id')
        if partner_id:
            partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
        else:
            partner_ids = []

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights("read")
        self.check_access_rule("read")

        result = []
        for product in self.sudo():
            # display only the attributes with multiple possible values on the template
            variable_attributes = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped('attribute_id')
            variant = product.attribute_value_ids._variant_name(variable_attributes)

            name = variant and "%s (%s)" % (product.name, variant) or product.name
            sellers = []
            if partner_ids:
                sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and (x.product_id == product)]
                if not sellers:
                    sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and not x.product_id]
            if sellers:
                for s in sellers:
                    seller_variant = s.product_name and (
                        variant and "%s (%s)" % (s.product_name, variant) or s.product_name
                    ) or False
                    mydict = {
                        'id': product.id,
                        'name': seller_variant or name,
                        'default_code': s.product_code or product.default_code,
                    }
                    temp = _name_get(mydict)
                    if temp not in result:
                        result.append(temp)
            else:
                mydict = {
                    'id': product.id,
                    'name': name,
                    'default_code': product.default_code,
                }
                result.append(_name_get(mydict))
        return result
