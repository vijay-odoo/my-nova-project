# -*- coding: utf-8 -*-
from odoo import api, models


class Website(models.Model):
    _inherit = 'website'

    @api.multi
    def sale_get_order(
        self, force_create=False, code=None,
        update_pricelist=False, force_pricelist=False
    ):
        res = super(Website, self).sale_get_order(
            force_create=force_create,
            code=code,
            update_pricelist=update_pricelist,
            force_pricelist=force_pricelist
        )
        if res:
            res.onchange_partner_id()
        return res
