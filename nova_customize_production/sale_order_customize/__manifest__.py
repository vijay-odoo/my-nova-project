# -*- coding: utf-8 -*-
{
    'name': 'Sales Order Customize',
    'version': '0.2017.12.14.1',
    'category': 'Sales',
    'description': """
        Sales Order Customize
    """,
    'depends': [
        'sale', 'stock', 'account',
        'authorized_net_cim_10', 'delivery',
        'commercial_rules', 'website_sale'
    ],
    'data': [
        'views/sale_order_view.xml',
        'data/mail_template_data.xml'
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
