# -*- coding: utf-8 -*-
{
    'name': 'Website Commercial Rules',
    'version': '10.0.0.0.1',
    'author': 'Jaimik',
    'category': 'Generic Modules/Sales & Purchases',
    'depends': ['commercial_rules', 'website_sale'],
    'description': """
        Website Commercial Rules
    """,
    'data': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
