# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale


class WebsiteCommercialRules(WebsiteSale):

    @http.route(['/shop/pricelist'], type='http', auth="public", website=True)
    def pricelist(self, promo, **post):
        pricelist = request.env['product.pricelist'].sudo().search([('code', '=', promo)], limit=1)
        if pricelist and not request.website.is_pricelist_available(pricelist.id):
            return request.redirect("/shop/cart?code_not_available=1")
        request.website.sale_get_order(code=promo)
        order = request.website.sale_get_order()
        if order:
            order.apply_commercial_rules()
        return request.redirect("/shop/cart")

    @http.route(['/shop/cart/update'], type='http', auth="public", methods=['POST'], website=True, csrf=False)
    def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
        res = super(WebsiteCommercialRules, self).cart_update(product_id, add_qty=add_qty, set_qty=set_qty, **kw)
        order = request.website.sale_get_order()
        if order:
            order.apply_commercial_rules()

        return res

    @http.route(['/shop/cart/update_json'], type='json', auth="public", methods=['POST'], website=True, csrf=False)
    def cart_update_json(self, product_id, line_id=None, add_qty=None, set_qty=None, display=True):
        res = super(WebsiteCommercialRules, self).cart_update_json(
            product_id, line_id=line_id, add_qty=add_qty, set_qty=set_qty, display=display
        )
        order = request.website.sale_get_order()
        if order:
            order.apply_commercial_rules()
        return res

    @http.route(['/shop/payment'], type='http', auth="public", website=True)
    def payment(self, **post):
        res = super(WebsiteCommercialRules, self).payment(
            **post
        )
        order = request.website.sale_get_order()
        if order:
            order.apply_commercial_rules()
        return res
