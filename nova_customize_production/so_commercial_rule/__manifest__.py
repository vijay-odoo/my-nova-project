# -*- coding: utf-8 -*-

{
    'name': 'Sales Order Auto Apply Commercial Rules',
    'version': '2018.01.24.1',
    'category': 'Sales',
    'description': """
Sales Order Auto Apply Commercial Rules
====================================
    """,
    'depends': ['commercial_rules', 'sale'],
    'data': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
