# -*- coding: utf-8 -*-

from odoo import models, api


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.multi
    def write(self, vals):
        if 'website_id' in self.env.context:
            res = super(sale_order, self.with_context(mail_auto_subscribe_no_notify=1)).write(vals)
        else:
            res = super(sale_order, self).write(vals)
        if not 'is_apply_commercial' in self.env.context and vals.get('order_line', False):
            self.with_context(is_apply_commercial=True).apply_commercial_rules()
        return res
