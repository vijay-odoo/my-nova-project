# -*- coding: utf-8 -*-
{
    'name': 'Stock Critical',
    'version': '0.2018.02.03.1',
    'summary': 'List of stock for Qty On Hand < Critical Stock',
    'category': 'Sale',
    'author': 'Jaimik',
    'depends': [
        'sale', 'stock', 'procurement', 'product', 'mail'
    ],
    'data': [
        'views/critical_stock_view.xml',
        'views/purchase_settings_view.xml',
        'data/data.xml',
        'wizard/views/wiz_product_threshold_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
