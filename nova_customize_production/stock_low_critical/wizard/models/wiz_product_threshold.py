# -*- coding: utf-8 -*-
#from openerp import models, api, pooler, SUPERUSER_ID
from odoo import models, api
from odoo.tools.safe_eval import safe_eval


class WizproductThresholdReset(models.TransientModel):
    _name = 'wiz.product.threshold.reset'

    @api.multi
    def confirm_product_threshold_reset(self):
        IrConfigParam = self.env['ir.config_parameter']
        conf_threshold_low = safe_eval(IrConfigParam.get_param(
            'stock_low_critical.conf_threshold_low', 'False')) and int(IrConfigParam.get_param(
                'stock_low_critical.conf_threshold_low')) or 100
        conf_threshold_low_critical = safe_eval(IrConfigParam.get_param(
            'stock_low_critical.conf_threshold_low_critical', 'False')) and int(IrConfigParam.get_param(
                'stock_low_critical.conf_threshold_low_critical')) or 50
        self.env.cr.execute("""
            update product_template set
            threshold_low = '%s', threshold_low_critical = '%s'
        """ % (conf_threshold_low[0], conf_threshold_low_critical[0]))
        # product_obj = self.env['product.product']
        # product_ids = product_obj.search([], order='id')
        # for product in product_ids:
        #     res = product.write({
        #         'threshold_low': conf_threshold_low[0] or 0,
        #         'threshold_low_critical': conf_threshold_low_critical[0] or 0
        #     })
        return True
