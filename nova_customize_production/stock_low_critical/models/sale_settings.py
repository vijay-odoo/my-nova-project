# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval


class SSaleConfigSettings(models.TransientModel):
    _inherit = 'sale.config.settings'

    conf_threshold_low = fields.Integer('Low Inventory', default="100")
    conf_threshold_low_critical = fields.Integer(
        'Low Inventory Critical', default="50")
    conf_email = fields.Many2many(
        'res.users', "product_template_sale_setting_rel",
        'settings_id', 'user_id', string=_('E-mail')
    )

    @api.model
    def get_default_conf_threshold_low(self, fields):
        IrConfigParam = self.env['ir.config_parameter']
        conf_email_ids = []
        conf_email = safe_eval(
            IrConfigParam.get_param
            (
                'stock_low_critical.conf_email', 'False'
            )
        )
        if conf_email:
            conf_email_ids = self.env['res.users'].search(
                [('id', 'in', conf_email)]
            )
        return {
            'conf_threshold_low': safe_eval(IrConfigParam.get_param('stock_low_critical.conf_threshold_low', 'False')) and int(IrConfigParam.get_param('stock_low_critical.conf_threshold_low')) or 100,
            'conf_threshold_low_critical': safe_eval(IrConfigParam.get_param('stock_low_critical.conf_threshold_low_critical', 'False')) and int(IrConfigParam.get_param('stock_low_critical.conf_threshold_low_critical')) or 50,
            'conf_email': conf_email_ids and [(6, 0, [email.id for email in conf_email_ids if email.partner_id and email.partner_id.email])] or False,
        }

    @api.multi
    def set_default_channel_id(self):
        self.ensure_one()
        conf_email_ids = []
        for email in self.conf_email:
            if email.id:
                conf_email_ids.append(email.id)
        IrConfigParam = self.env['ir.config_parameter']
        IrConfigParam.set_param('stock_low_critical.conf_threshold_low', repr(self.conf_threshold_low or 100))
        IrConfigParam.set_param('stock_low_critical.conf_threshold_low_critical', repr(self.conf_threshold_low_critical or 50))
        IrConfigParam.set_param('stock_low_critical.conf_email', repr(conf_email_ids))

    @api.multi
    def set_threshold_low(self):
        view_id = self.env.ref('stock_low_critical.wiz_product_threshold_reset_view').id
        action = {
            'type': 'ir.actions.act_window',
            'name': _('Set All Products Threshold'),
            'res_model': "wiz.product.threshold.reset",
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': view_id,
            'context': self._context,
            'target': 'new',
        }
        return action
