# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.tools.safe_eval import safe_eval
from email.utils import formataddr


class StockCritical(models.Model):
    _inherit = 'product.template'

    stock_critical = fields.Float(
        'Min. Stock Level', default="0.00", compute='_get_minimum_quantity_from_inventory')
    is_product_stock_critical = fields.Boolean('Bellow Min. Stock Level?', default=False)
    is_threshold = fields.Boolean(
        'Low Inventory Alarm', default=False)
    threshold_low = fields.Integer('Low Inventory', default="0")
    threshold_low_critical = fields.Integer(
        'Low Inventory Critical')
    threshold_low_status = fields.Char('Low Inventory Status', default="Low Inventory Critical.")
    threshold_low_critical_status = fields.Char(
        'Low Inventory Critical Status', default="Is Too Low Inventory Critical.")
    is_threshold_low = fields.Boolean(
        'Is Low Inventory', default=False)
    is_threshold_low_critical = fields.Boolean(
        'Is Low Inventory Critical', default=False)

    @api.multi
    @api.depends('qty_available', 'reordering_min_qty', 'threshold_low', 'threshold_low_critical')
    def _get_minimum_quantity_from_inventory(self):
        for product in self:
            is_threshold_low = 'False'
            is_threshold_low_critical = 'False'
            if not isinstance(product.id, models.NewId):
                product.stock_critical = product.reordering_min_qty
                if product.qty_available < product.reordering_min_qty and \
                        product.virtual_available < product.reordering_min_qty and \
                        product.type == 'product':
                    if product.is_threshold:
                        if product.qty_available < product.threshold_low and product.virtual_available < product.threshold_low:
                            is_threshold_low = 'True'
                        if product.qty_available < product.threshold_low_critical and product.virtual_available < product.threshold_low_critical:
                            is_threshold_low = 'False'
                            is_threshold_low_critical = 'True'
                    self.env.cr.execute("""
                        update product_template set
                        is_product_stock_critical = 'True', is_threshold_low_critical = '%s', is_threshold_low = '%s' where id = %s
                    """ % (is_threshold_low_critical, is_threshold_low, product.id))
                else:
                    self.env.cr.execute("""
                        update product_template set
                        is_product_stock_critical = 'False', is_threshold_low_critical = 'False', is_threshold_low = 'False' where id = %s
                    """ % (product.id))

    @api.multi
    def send_low_stock_via_email(self):
        header_label_list = ["Code", "Name", "Qty On Hand", "Qty Incoming", "Low Inventory", "Low Inventory Critical"]
        ## Get email template
        # template_obj = self.env['mail.template']
        template_ids = self.env['ir.model.data'].sudo().get_object('stock_low_critical', 'email_low_stock_notify_template')
        # template_ids = template_obj.search(
            # [('name', '=', 'Low Stock Automated Report')])
        IrConfigParam = self.env['ir.config_parameter']
        conf_email_ids = []
        conf_email = safe_eval(
            IrConfigParam.get_param
            (
                'stock_low_critical.conf_email', 'False'
            )
        )
        if not conf_email:
            conf_email = [1]
        conf_email_ids = self.env['res.users'].search(
            [('id', 'in', conf_email)]
        )
        if template_ids:
            email_to = ','.join(formataddr((user.partner_id.name, user.partner_id.email)) for user in conf_email_ids if user.partner_id and user.partner_id.email)
            template_ids.email_to = email_to
            template_ids.email_recipients = email_to
            default_body = template_ids.body_html
            custom_body = """
                <table class="table table-bordered table-striped">
                    <th>%s</th>
                    <th>%s</th>
                    <th style="text-align:center;">%s</th>
                    <th style="text-align:center;">%s</th>
                    <th style="text-align:center;">%s</th>
            """ % (header_label_list[0], header_label_list[1], header_label_list[2], header_label_list[4], header_label_list[5])
            ## Check for low stock products
            product_obj = self.env['product.product']
            product_ids = product_obj.search([
                ('active', '=', True),
                ('is_threshold', '!=', False)])
            # self._get_minimum_quantity_from_inventory()
            for product in product_ids:
                product_sku = product.default_code
                if not product_sku or product_sku == '':
                    continue
                qty_available = product.qty_available
                # qty_incoming = product.incoming_qty
                threshold_low = product.threshold_low
                threshold_low_critical = product.threshold_low_critical
                if qty_available <= threshold_low and threshold_low >= 0 or qty_available <= threshold_low_critical and threshold_low_critical >= 0:
                    custom_body += """
                        <tr style="font-size:14px;">
                            <td>%s</td>
                            <td>%s</td>
                            <td style="text-align:center;">%s</td>
                            <td style="text-align:center;">%s</td>
                            <td style="text-align:center;">%s</td>
                        </tr>
                    """ % (product_sku, product.name, str(qty_available), str(threshold_low), str(threshold_low_critical))
            custom_body += "</table>"
            template_ids.body_html = default_body + custom_body
            template_ids.send_mail(template_ids.id, force_send=True)
            template_ids.body_html = default_body
            return True
