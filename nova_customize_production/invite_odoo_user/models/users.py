# -*- coding: utf-8 -*-
from odoo import models, api, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    invitation_sent = fields.Boolean("Invitation Sent From Customer")

    @api.multi
    def write(self, values):
        if values.get('password', False):
            for rec in self:
                if rec.invitation_sent and rec.partner_id:
                    rec.partner_id.invitation_accept = True
        res = super(ResUsers, self).write(values)
        return res
