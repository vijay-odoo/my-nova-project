# -*- coding: utf-8 -*-
from odoo import models, api, fields, _
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    invitation_sent = fields.Boolean("Invitation Sent")
    invitation_accept = fields.Boolean("Invitation Accept")

    @api.model
    def create(self, values):
        res = super(ResPartner, self).create(values)
        return res

    @api.multi
    def send_invitation_email(self):
        for rec in self:
            rec.invite_create_users([rec.email])
            rec.invitation_sent = True
        return True

    @api.model
    def invite_create_users(self, emails):
        activate_users = self.env['res.users'].search([
            '|',
            ('login', 'in', emails),
            ('email', 'in', emails)
        ])
        if activate_users:
            raise UserError(_('Invitation already Sent.'))
        deactivate_users = self.env['res.users'].with_context(active_test=False).search([
            ('active', '=', False),
            '|',
            ('login', 'in', emails),
            ('email', 'in', emails)
        ])
        for user in deactivate_users:
            user.active = True

        new_emails = set(emails) - set(deactivate_users.mapped('email'))

        for email in new_emails:
            default_values = {
                'login': email,
                'name': self.name or email.split('@')[0],
                'email': email,
                'active': True,
                'partner_id': self.id,
                'invitation_sent': True,
                'groups_id': [(6, 0, [self.env.ref('base.group_portal').id])],
            }
            user = self.env['res.users'].with_context(signup_valid=True).create(
                default_values
            )
        return True
