# -*- coding: utf-8 -*-
{
    'name': 'Invite New User',
    'version': '1.0',
    'summary': 'Invite New User',
    'category': 'Extra Tools',
    'author': 'Jaimik',
    'description':
    """
        Invite New User
    """,
    'data': [
        'views/partner_view.xml',
    ],
    'depends': ['web_settings_dashboard', 'base'],
    'auto_install': True,
}
