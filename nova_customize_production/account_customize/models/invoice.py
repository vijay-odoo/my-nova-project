# -*- coding: utf-8 -*-

from odoo import api, models, fields
from odoo.addons.account.models.account_invoice import AccountInvoice


@api.one
@api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice')
def _compute_amount(self):
    self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids if not line.is_delivery)
    self.amount_tax = sum(line.amount for line in self.tax_line_ids)
    self.amount_total = self.amount_untaxed + self.amount_tax + self.delivery_amount
    amount_total_company_signed = self.amount_total
    amount_untaxed_signed = self.amount_untaxed
    if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
        currency_id = self.currency_id.with_context(date=self.date_invoice)
        amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
        amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
    sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
    self.amount_total_company_signed = amount_total_company_signed * sign
    self.amount_total_signed = self.amount_total * sign
    self.amount_untaxed_signed = amount_untaxed_signed * sign


AccountInvoice._compute_amount = _compute_amount


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    is_delivery = fields.Boolean(string="Is a Delivery", default=False)


class accountinvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_invoice_send(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('account_customize', 'email_template_account_invoice')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'account.invoice',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "account_customize.mail_template_data_notification_email_account_invoice"
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice')
    def _delivery_amount_all(self):
        self.delivery_amount = sum(line.price_subtotal for line in self.invoice_line_ids if line.is_delivery)

    delivery_amount = fields.Monetary(
        string='Delivery Amount',
        store=True, readonly=True,
        compute='_delivery_amount_all', track_visibility='always'
    )

    @api.onchange('user_id')
    def _onchange_user_id(self):
        if self.user_id:
            team_ids = self.env['crm.team'].search([
                ('member_ids', 'in', self.user_id.id)
            ])
            if team_ids:
                self.team_id = team_ids and team_ids[0].id or False
        elif not self.team_id:
            self.team_id = False
        else:
            self.team_id = False


class saleorderline(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(saleorderline, self)._prepare_invoice_line(qty)
        res.update({
            'is_delivery': self.is_delivery or False
        })
        return res
