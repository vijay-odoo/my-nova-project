# -*- coding: utf-8 -*-
{
    'name': 'Invoice Customize',
    'version': '1.0',
    'category': 'Account',
    'description': """
        Invoice Customize
    """,
    'depends': ['sale', 'stock', 'account', 'delivery'],
    'data': [
        'data/email_template.xml',
        'views/invoice_view.xml',
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
