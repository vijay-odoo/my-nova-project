# -*- coding: utf-8 -*-
{
    'name': 'Partner Address Change',
    'version': '0.2017.14.12.1',
    'category': 'Base',
    'description': """
        Partner Address
    """,
    'depends': ['base', 'sale'],
    'data': [
        'sale_view.xml',
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
