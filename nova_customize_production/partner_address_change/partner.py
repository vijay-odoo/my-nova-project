# -*- coding: utf-8 -*-
from odoo import api
from odoo.addons.base.res.res_partner import Partner

ADDRESS_FIELDS = ('city', 'state_id', 'zip')


@api.model
def _address_fields(self):
    """Returns the list of address fields that are synced from the parent."""
    return list(ADDRESS_FIELDS)

Partner._address_fields = _address_fields


@api.multi
def _display_address(self, without_company=False):

    '''
    The purpose of this function is to build and return an address formatted accordingly to the
    standards of the country where it belongs.

    :param address: browse record of the res.partner to format
    :returns: the address formatted in a display that fit its country habits (or the default ones
        if not country is specified)
    :rtype: string
    '''
    # get the information that will be injected into the display format
    # get the address format
    address_format = "%(city)s %(state_code)s %(zip)s"
    args = {
        'state_code': self.state_id.code or '',
        'state_name': self.state_id.name or '',
        'country_code': self.country_id.code or '',
        'country_name': self.country_id.name or '',
        'company_name': self.commercial_company_name or '',
        'zip': self.zip or ''
    }
    for field in self._address_fields():
        args[field] = getattr(self, field) or ''
    if without_company:
        args['company_name'] = ''
    elif self.commercial_company_name:
        address_format = '%(company_name)s\n' + address_format
    return address_format % args

Partner._display_address = _display_address
