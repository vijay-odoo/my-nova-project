# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import http
from odoo.http import request
from odoo.addons.website_sale_delivery.controllers.main import WebsiteSaleDelivery
from odoo.addons.website_sale.controllers.main import WebsiteSale


class WebsiteSaleDeliveryInherit(WebsiteSaleDelivery):

    @http.route(['/website/get_delivery'], type='json', auth="public", methods=['POST'], website=True)
    def front_get_delivery(self, **post):
        values = {}
        order = request.website.sale_get_order()
        if order:
            delivery_carriers = order._get_delivery_methods()
            values.update({
                'deliveries': delivery_carriers.sudo().with_context(order_id=order.id),
                'order': order,
            })
        return request.env['ir.ui.view'].render_template(
            "website_sale_delivery_custom.delivery_method_details",
            values
        )

    @http.route(['/website/set_delivery/<int:order_id>/<int:delivery_id>'], type='http', auth="public", website=True, methods=['GET', 'POST'])
    def delivery_confirm(self, order_id, delivery_id, **post):
        order = request.env['sale.order'].sudo().browse(int(order_id))
        order.with_context(order_id=order.id).sudo().write({
            'carrier_id': delivery_id or False
        })
        order.with_context(order_id=order.id)._compute_delivery_price()
        order.with_context(order_id=order.id).sudo().delivery_set()
        return request.redirect("/shop/payment")


class WebsiteSaleDeliveryInfo(WebsiteSale):

    @http.route(['/shop/confirm_order'], type='http', auth="public", website=True)
    def confirm_order(self, **post):
        order = request.website.sale_get_order()

        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        order.onchange_partner_shipping_id()
        order.order_line._compute_tax_id()
        request.session['sale_last_order_id'] = order.id
        request.website.sale_get_order(update_pricelist=True)
        return request.redirect("/shop/delivery_info")

    @http.route(['/shop/delivery_info'], type='http', auth="public", website=True)
    def delivery_info(self, **post):
        values = {}
        order = request.website.sale_get_order()
        if order:
            delivery_carriers = order._get_delivery_methods()
            values.update({
                'deliveries': delivery_carriers.sudo().with_context(order_id=order.id),
                'order': order,
                'website_sale_order': order,
                'post': post,
            })
            values.update(request.env['sale.order']._get_website_data(order))
        return request.render("website_sale_delivery_custom.delivery_rate_info", values)
