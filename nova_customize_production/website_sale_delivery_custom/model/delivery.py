# -*- coding: utf-8 -*-
from odoo.addons.website_sale_delivery.models.sale_order import SaleOrder
import logging
from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
_logger = logging.getLogger(__name__)


def _check_carrier_quotation(self, force_carrier_id=None):
    # check to add or remove carrier_id
    if not self:
        return False
    self.ensure_one()
    DeliveryCarrier = self.env['delivery.carrier']
    if self.only_services:
        self.write({'carrier_id': None})
        self._delivery_unset()
        return True
    else:
        carrier = force_carrier_id and DeliveryCarrier.browse(force_carrier_id) or self.carrier_id
        if force_carrier_id or carrier:
            self.write({'carrier_id': carrier and carrier.id or force_carrier_id})
            self.with_context(order_id=self.id).delivery_set()
        else:
            self._delivery_unset()
    return bool(carrier)


SaleOrder._check_carrier_quotation = _check_carrier_quotation


def _get_delivery_methods(self):
    """Return the available and published delivery carriers"""
    self.ensure_one()
    available_carriers = DeliveryCarrier = self.env['delivery.carrier']
    # Following loop is done to avoid displaying delivery methods who are not available for this order
    # This can surely be done in a more efficient way, but at # available_carriers = self._get_delivery_methods()
        # if carrier:
        #     if carrier not in available_carriers:
        #         carrier = DeliveryCarrier
        #     else:
        #         # set the forced carrier at the beginning of the list to be verfied first below
        #         available_carriers -= carrier
        #         available_carriers = carrier + available_carriers
        # if force_carrier_id or not carrier or carrier not in available_carriers:
        #     for delivery in available_carriers:
        #         verified_carrier = delivery.verify_carrier(self.partner_shipping_id)
        #         if verified_carrier:
        #             carrier = delivery
        #             break
        #     self.write({'carrier_id': carrier.id})
        # if carrier:
            # self.with_context(order_id=self.id).delivery_set()
        # else:the moment, it mimics the way it's
    # done in delivery_set method of sale.py, from delivery module
    context = dict(self.env.context or {})
    if context.get('delivery_backend', False):
        domain = []
    else:
        domain = [('website_published', '=', True)]
    carrier_ids = DeliveryCarrier.sudo().search(domain).ids
    for carrier_id in carrier_ids:
        carrier = DeliveryCarrier.browse(carrier_id)
        try:
            _logger.debug("Checking availability of carrier #%s" % carrier_id)
            available = carrier.with_context(order_id=self.id).read(fields=['available'])[0]['available']
            if available:
                available_carriers += carrier
        except ValidationError as e:
            # RIM TODO: hack to remove, make available field not depend on a SOAP call to external shipping provider
            # The validation error is used in backend to display errors in fedex config, but should fail silently in frontend
            _logger.debug("Carrier #%s removed from e-commerce carrier list. %s" % (carrier_id, e))
    return available_carriers


SaleOrder._get_delivery_methods = _get_delivery_methods


@api.model
def _get_errors(self, order):
    return super(SaleOrder, self)._get_errors(order)

SaleOrder._get_errors = _get_errors


@api.model
def _get_website_data(self, order):
    """ Override to add delivery-related website data. """
    values = super(SaleOrder, self)._get_website_data(order)
    # We need a delivery only if we have stockable products
    has_stockable_products = any(order.order_line.filtered(lambda line: line.product_id.type in ['consu', 'product']))
    if not has_stockable_products:
        return values

    #delivery_carriers = order._get_delivery_methods()
    #values['deliveries'] = delivery_carriers.sudo().with_context(order_id=order.id)
    return values


SaleOrder._get_website_data = _get_website_data


class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    @api.one
    def get_price(self):
        SaleOrder = self.env['sale.order']

        self.available = False
        self.price = False

        order_id = self.env.context.get('order_id')
        if order_id:
            # FIXME: temporary hack until we refactor the delivery API in master

            order = SaleOrder.browse(order_id)
            if self.delivery_type not in ['fixed', 'base_on_rule']:
                try:
                    computed_price = self.get_shipping_price_from_so(order)[0]
                    self.available = True
                except UserError as e:
                    # No suitable delivery method found, probably configuration error
                    _logger.info("Carrier %s: %s, not found", self.name, e.name)
                    computed_price = 0.0
            else:
                carrier = self.verify_carrier(order.partner_shipping_id)
                if carrier:
                    try:
                        computed_price = carrier.get_price_available(order)
                        self.available = True
                    except UserError as e:
                        # No suitable delivery method found, probably configuration error
                        _logger.info("Carrier %s: %s", carrier.name, e.name)
                        computed_price = 0.0
                else:
                    computed_price = 0.0
            if self.fedex_servicetype.code == 'GROUND_HOME_DELIVERY' or self.is_ground_delivery and order.amount_total >= self.free_delivery_above:
                computed_price = 0.0

            self.price = computed_price * (1.0 + (float(self.margin) / 100.0))

    price = fields.Float(compute='get_price')
    available = fields.Boolean(compute='get_price')
