# -*- coding: utf-8 -*-
{
    'name': 'Website Delivery Customize',
    'version': '0.2017.21.12.1',
    'author': 'Jaimik',
    'category': 'Generic Modules/Sales & Purchases',
    'depends': [
        'delivery_method_wiz', 'website_sale_delivery', 'website_sale',
        'delivery', 'fedex_delivery_carrier', 'commercial_rules'
    ],
    'description': """
        Website Delivery Customize
    """,
    'data': [
        'view/website_delivery.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
