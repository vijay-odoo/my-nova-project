# -*- coding: utf-8 -*-
{
    'name': 'Project Access',
    'version': '2018.1.30.1',
    'category': 'Project',
    'description': """
Customized project access.
=============================================================
""",
    'author': 'Jaimik',
    'depends': [
        'base',
        'hr',
        'project',
        'hr_timesheet',
	'account',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/project_security.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
