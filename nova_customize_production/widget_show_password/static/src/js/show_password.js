odoo.define('widget_show_password.show_password', function(require) {
    "use strict";
    
    var core = require('web.core');
    var widgets = require('web.form_widgets');
    var FieldChar = core.form_widget_registry.get('char');
    FieldChar.include({
        events: {
            'change': 'store_dom_value',
            'click button#show_password': 'input_show_password',
        },
        init: function (field_manager, node) {
            this._super(field_manager, node);
            this.password = this.node.attrs.password === 'True' || this.node.attrs.password === '1';
            this.show_password = this.node.attrs.show_password === 'True' || this.node.attrs.show_password === '1';
        },
        store_dom_value: function () {
            if (this.$input && this.is_syntax_valid()) {
                this.internal_set_value(this.parse_value(this.$input.val()));
            }
        },
        input_show_password: function(event) {
            event.preventDefault();
            var button = event.target.id.length;
            var icon = $(event.target).closest('i.fa.fa-eye').length;
            if (button >= 1)
            {
                var children_icon = $(event.target).children('i.fa.fa-eye').length;
                if (children_icon == 1) {
                    $(event.target).children('i.fa.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash');
                    this.$input.prop('type', 'text');
                } else {
                    $(event.target).children('i.fa.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye');
                    this.$input.prop('type', 'password');
                }
            }
            else{
                if (icon == 1) {
                    $(event.target).closest('i.fa.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash');
                    this.$input.prop('type', 'text');
                } else {
                    $(event.target).closest('i.fa.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye');
                    this.$input.prop('type', 'password');
                }
            }
        },
    });
});
