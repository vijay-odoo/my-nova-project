# -*- coding: utf-8 -*-
{
  "name"                 :  "Show Password",
  "summary"              :  "Show Password",
  "category"             :  "Web",
  "version"              :  "1.0",
  "description"          :  """Show Password""",
  "depends"              :  ['web'],
  "data"                 :  ['views/template.xml'],
  "qweb"                 :  ['static/src/xml/base.xml'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
}