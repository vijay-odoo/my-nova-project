# -*- coding: utf-8 -*-
{
    'name': 'Stock Customize',
    'version': '1.2017.11.15.2',
    'category': 'Inventory',
    'description': """
        Stock Customize
    """,
    'depends': ['sale', 'stock', 'account', 'odoo_shipping_service_apps', 'fedex_delivery_carrier'],
    'data': [
        'views/stock_view.xml',
        'data/mail_template_data.xml',
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
