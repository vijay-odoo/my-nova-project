# -*- coding: utf-8 -*-

from odoo import api, models, fields
from lxml import etree
from odoo.osv.orm import setup_modifiers


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.model_cr
    def init(self):
        cr = self._cr
        cr.execute("""
            update sale_order set invoice_payment_status='unpaid'
        """)
        cr.execute("""
            update sale_order set invoice_payment_status= CASE WHEN spso.state in ('paid') THEN 'paid'
                 WHEN spso.state in  ('draft', 'proforma', 'proforma2', 'open') THEN 'unpaid'
                 WHEN spso.state in  ('cancel') THEN 'Cancel' ELSE 'unpaid'
                 END
            from (
                select ai.state, ai.id, so.id as order_id from sale_order as so
                inner join account_invoice as ai on ai.origin = so.name
                group by ai.id, so.id
                order by so.id
             ) as spso where sale_order.id = spso.order_id
        """)
        cr.execute("""
            update sale_order set invoice_payment_status=CASE WHEN spso.state in ('paid') THEN 'paid'
                 WHEN spso.state in  ('draft', 'proforma', 'proforma2', 'open') THEN 'unpaid'
                 WHEN spso.state in  ('cancel') THEN 'Cancel' ELSE 'unpaid'
                 END, track_number = spso.number
             from (
                select st.carrier_tracking_ref as number, ai.state, ai.id, so.id as order_id from sale_order as so
                inner join account_invoice as ai on ai.origin = so.name
                inner join stock_picking as st on st.origin = so.name
                group by ai.id, so.id, st.id
                order by so.id
             ) as spso where sale_order.id = spso.order_id
         """)

    track_number = fields.Char(string='Tracking Reference Number')
    invoice_payment_status = fields.Selection([
        ('paid', 'Paid'),
        ('unpaid', 'Unpaid'),
    ], string='Invoice Payment Status', default='unpaid')


class stockpicking(models.Model):
    _inherit = 'stock.picking'

    @api.model_cr
    def init(self):
        cr = self._cr
        cr.execute("""
            update stock_picking set invoice_status= spso.invoice_status from (
            select s.invoice_status, sp.id from stock_picking as sp
            inner join sale_order as s on s.name = sp.origin
            ) as spso where stock_picking.id = spso.id
        """)
        cr.execute("""
            update stock_picking set invoice_status= spso.invoice_status, invoice_payment_status=
            CASE WHEN spso.state='paid' THEN 'paid'
            ELSE 'unpaid'
            END
            from (
            select inv.state, s.invoice_status, sp.id from stock_picking as sp
            inner join sale_order as s on s.name = sp.origin
            inner join account_invoice as inv on inv.origin = sp.origin
            ) as spso where stock_picking.id = spso.id
        """)

    invoice_status = fields.Selection([
        ('upselling', 'Upselling Opportunity'),
        ('invoiced', 'Fully Invoiced'),
        ('to invoice', 'To Invoice'),
        ('no', 'Nothing to Invoice')
    ], string='Invoice Status', readonly=True)

    invoice_payment_status = fields.Selection([
        ('paid', 'Paid'),
        ('unpaid', 'Unpaid'),
    ], string='Invoice Payment Status', readonly=True)

    @api.model
    def create(self, vals):
        res = super(stockpicking, self).create(vals)
        if res.carrier_tracking_ref and res.origin:
            sale_order_id = self.env['sale.order'].search([('name', '=', res.origin)])
            if sale_order_id:
                sale_order_id[0].track_number = res.carrier_tracking_ref or False
        if 'origin' in vals and vals.get('origin', False):
            sale_order_id = self.env['sale.order'].search([('name', '=', vals.get('origin'))])
            if sale_order_id:
                is_paid = False
                res.invoice_status = sale_order_id[0].invoice_status
                invoice_ids = self.env['account.invoice'].search([('name', '=', vals.get('origin')), ('type', 'in', ['out_invoice'])])
                for invoice_id in invoice_ids:
                    if invoice_id.state != 'paid':
                        is_paid = True
                if not is_paid and invoice_ids:
                    res.invoice_payment_status = 'unpaid'
                    sale_order_id[0].invoice_payment_status = 'unpaid'
                elif not is_paid and sale_order_id[0].invoice_status == 'invoiced':
                    res.invoice_payment_status = 'paid'
                    sale_order_id[0].invoice_payment_status = 'paid'
        return res

    @api.multi
    def write(self, values):
        res = super(stockpicking, self).write(values)
        if self.carrier_tracking_ref and self.origin:
            sale_order_id = self.env['sale.order'].search([('name', '=', self.origin)])
            if sale_order_id:
                sale_order_id[0].track_number = self.carrier_tracking_ref or False
        if 'origin' in values and values.get('origin', False):
            sale_order_id = self.env['sale.order'].search([('name', '=', values.get('origin'))])
            if sale_order_id:
                is_paid = False
                res.invoice_status = sale_order_id[0].invoice_status
                invoice_ids = self.env['account.invoice'].search([('name', '=', values.get('origin')), ('type', 'in', ['out_invoice'])])
                for invoice_id in invoice_ids:
                    if invoice_id.state != 'paid':
                        is_paid = True
                if not is_paid and invoice_ids:
                    res.invoice_payment_status = 'unpaid'
                    sale_order_id[0].invoice_payment_status = 'unpaid'
                elif not is_paid and sale_order_id[0].invoice_status == 'invoiced':
                    res.invoice_payment_status = 'paid'
                    sale_order_id[0].invoice_payment_status = 'paid'
        if 'carrier_tracking_ref' in values and values.get('carrier_tracking_ref', False):
            template = self.env.ref('stock_customize.email_template_tracking_referance', raise_if_not_found=False)
            for rec in self:
                if template:
                    template.send_mail(rec.id, force_send=True)
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(stockpicking, self).fields_view_get(
            view_id, view_type, toolbar, submenu)
        stock_invetory_group = True
        current_user_group_ids = self.env['res.users'].browse(self.env.uid).groups_id
        stock_invetory = self.env.ref('stock.group_stock_manager').id
        for group in current_user_group_ids:
            if group.id == stock_invetory:
                stock_invetory_group = False
        if stock_invetory_group:
            if self.user_has_groups('stock.group_stock_user'):
                doc = etree.XML(res['arch'])
                if doc.xpath("//field[@name='partner_id']"):
                    list_price_node = doc.xpath("//field[@name='partner_id']")[0]
                    list_price_node.set('readonly', '1')
                    setup_modifiers(list_price_node, res['fields']['partner_id'])
                if doc.xpath("//field[@name='origin']"):
                    lst_price_node = doc.xpath("//field[@name='origin']")[0]
                    lst_price_node.set('readonly', '1')
                    setup_modifiers(lst_price_node, res['fields']['origin'])
                if doc.xpath("//field[@name='min_date']"):
                    saleprice_per_box_node = doc.xpath("//field[@name='min_date']")[0]
                    saleprice_per_box_node.set('readonly', '1')
                    setup_modifiers(saleprice_per_box_node, res['fields']['min_date'])
                if doc.xpath("//field[@name='pack_operation_product_ids']"):
                    saleprice_per_piece_node = doc.xpath("//field[@name='pack_operation_product_ids']")[0]
                    saleprice_per_piece_node.set('readonly', '1')
                    setup_modifiers(saleprice_per_piece_node, res['fields']['pack_operation_product_ids'])
                if doc.xpath("//field[@name='move_lines']"):
                    saleprice_per_piece_node = doc.xpath("//field[@name='move_lines']")[0]
                    saleprice_per_piece_node.set('readonly', '1')
                    setup_modifiers(saleprice_per_piece_node, res['fields']['move_lines'])
                res['arch'] = etree.tostring(doc)
        return res


class accountinvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def create(self, values):
        res = super(accountinvoice, self).create(values)
        if 'origin' in values and values.get('origin', False):
            sale_order_id = self.env['sale.order'].search([('name', '=', values.get('origin'))])
            if sale_order_id:
                is_paid = False
                res.invoice_status = sale_order_id[0].invoice_status
                invoice_ids = self.env['account.invoice'].search([('origin', '=', sale_order_id.name), ('type', 'in', ['out_invoice'])])
                for invoice_id in invoice_ids:
                    if invoice_id.state != 'paid':
                        is_paid = True
                if not is_paid and invoice_ids:
                    sale_order_id[0].invoice_payment_status = 'unpaid'
                elif not is_paid and sale_order_id[0].invoice_status == 'invoiced':
                    sale_order_id[0].invoice_payment_status = 'paid'
        return res

    @api.multi
    def write(self, values):
        res = super(accountinvoice, self).write(values)
        if 'origin' in values and values.get('origin', False):
            sale_order_id = self.env['sale.order'].search([('name', '=', values.get('origin'))])
            if sale_order_id:
                is_paid = False
                res.invoice_status = sale_order_id[0].invoice_status
                invoice_ids = self.env['account.invoice'].search([('origin', '=', sale_order_id.name), ('type', 'in', ['out_invoice'])])
                for invoice_id in invoice_ids:
                    if invoice_id.state != 'paid':
                        is_paid = True
                if not is_paid and invoice_ids:
                    sale_order_id[0].invoice_payment_status = 'unpaid'
                elif not is_paid and sale_order_id[0].invoice_status == 'invoiced':
                    sale_order_id[0].invoice_payment_status = 'paid'
        return res
