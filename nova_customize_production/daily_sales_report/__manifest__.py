# -*- coding: utf-8 -*-
{
    'name': 'Sales Report',
    'version': '2018.01.20.1',
    'summary': 'Sales Report with Dates',
    'category': 'Sale',
    'author': 'Jaimik',
    'website': '',
    'depends': [
        'sale', 'mail'
    ],
    'data': [
        'views/daily_sale_report_view.xml',
        'views/daily_sale_report_template.xml',
        'views/weekly_sale_report_template.xml',
        'security/ir.model.access.csv',
        'data/mail_template_data.xml',
    ],
    'installable': True,
    'auto_install': False,
}
