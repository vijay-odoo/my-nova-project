# -*- coding: utf-8 -*-
from odoo import models, fields, api, tools
from datetime import datetime, timedelta
from collections import OrderedDict

month_name = {
    '01': [],
    '02': [],
    '03': [],
    '04': [],
    '05': [],
    '06': [],
    '07': [],
    '08': [],
    '09': [],
    '10': [],
    '11': [],
    '12': [],
}


class WeeklySalesReport(models.TransientModel):
    _name = 'weekly.sale.report'

    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.user.company_id
    )
    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")

    def _get_utc_time_range(self, form):
        start_date = False
        end_date = False
        if form.get('date_start', False):
            start_date = fields.Datetime.from_string(form['date_start'])
            start_date = fields.Datetime.context_timestamp(self, timestamp=start_date)
            start_date = start_date.date().strftime(tools.DEFAULT_SERVER_DATE_FORMAT)
        if form.get('date_end', False):
            end_date = fields.Datetime.from_string(form['date_end'])
            end_date = fields.Datetime.context_timestamp(self, timestamp=end_date)
            end_date = end_date.date().strftime(tools.DEFAULT_SERVER_DATE_FORMAT)
        return start_date, end_date

    @api.multi
    def get_weekly_sales_grand_total(self):
        res = {}
        order_ids = []
        form = {
            'date_start': self.start_date,
            'date_end': self.end_date,
        }

        date_start, date_end = self._get_utc_time_range(form)
        if self:
            team_id = self.get_sale_team()
            sale_ids = self.env['sale.report'].search([
                ('date', '>=', date_start),
                ('date', '<=', date_end),
                ('state', 'not in', ('draft', 'cancel', 'sent'))
            ])
            for sale_id in sale_ids:
                start_date = fields.Datetime.from_string(sale_id.date)
                start_date = fields.Datetime.context_timestamp(self, timestamp=start_date)
                start_date = start_date.date().strftime('%d %b %Y')
                amount = 0
                amount = self.env['sale.order'].search([
                    ('name', '=', sale_id.name)
                ]).amount_total
                team_name = 'Undefined'
                if sale_id.team_id:
                    team_name = sale_id.team_id.name
                total_amount = 0
                count = 0
                if start_date in res:
                    if sale_id.name not in order_ids:
                        order_ids.append(sale_id.name)
                        total_amount = res[start_date][team_name][0] + amount
                    else:
                        total_amount = res[start_date][team_name][0]
                    count = res[start_date][team_name][1] + 1
                    res[start_date][team_name] = [total_amount, count]
                else:
                    res.update({
                        start_date.decode("utf-8"): dict(team_id)
                    })
                    if sale_id.name not in order_ids:
                        order_ids.append(sale_id.name)
                        total_amount = res[start_date][team_name][0] + amount
                    else:
                        total_amount = res[start_date][team_name][0]
                    count = res[start_date][team_name][1] + 1
                    res[start_date][team_name] = [total_amount, count]
        return OrderedDict(sorted(res.items()))

    @api.multi
    def get_sale_team(self):
        res = {}
        if self:
            query = """
                    select
                        name
                    from
                        crm_team
                """
            self.env.cr.execute(query)
            records = self.env.cr.fetchall()
            for rec in records:
                res.update({
                    rec[0].strip().decode("utf-8"): [0, 0]
                })
            res.update({
                'Undefined': [0, 0]
            })
        return res

    # @api.multi
    # def get_sale_person_wise_order(self):
    #     res = self.get_sale_person()
    #     result = {}
    #     temp_data = []
    #     form = {
    #         'date_start': self.start_date,
    #         'date_end': self.end_date,
    #     }

    #     date_start, date_end, start_year, end_year = self._get_utc_time_range(form)
    #     ex_start_date = datetime.strptime(
    #         date_start, '%Y-%m-%d'
    #     )
    #     ex_stop_date = datetime.strptime(
    #         date_end, '%Y-%m-%d'
    #     )
    #     diffrence = ex_stop_date - ex_start_date
    #     if self:
    #         query = """
    #                 select
    #                     so.date_order::date as sdate,
    #                     user_partner.name as sale_person,
    #                     ROUND(sum(so.amount_untaxed), 2) as sub_total
    #                 from sale_order as so
    #                     left join res_partner as partner on partner.id = so.partner_id
    #                     left join res_users as usr on usr.id = so.user_id
    #                     left join res_partner as user_partner on user_partner.id = usr.partner_id
    #                 where so.date_order::date >= '%s' and so.date_order::date <= '%s'and so.state in ('sale', 'done') and user_partner.name is not null
    #                 group by sale_person, sdate
    #                 order by sdate, sale_person
    #             """ % (date_start, date_end)

    #         self.env.cr.execute(query)

    #         records = self.env.cr.fetchall()
    #         temp_data = []
    #         for rec in records:
    #             temp_data.append([rec[0], rec[1], rec[2]])
    #             res.update({
    #                 rec[1].strip(): []
    #             })
    #         for days in range(diffrence.days + 1):
    #             ex_start_date = ex_start_date
    #             for rec in res:
    #                 date = datetime.strftime(
    #                     ex_start_date, '%Y-%m-%d'
    #                 )
    #                 amount = 0.0
    #                 if temp_data:
    #                     for data in temp_data:
    #                         if data[0] == date and rec == data[1]:
    #                             amount = data[2]
    #                             if rec in result:
    #                                 result[rec].append((
    #                                     date, amount
    #                                 ))
    #                             else:
    #                                 result.update({
    #                                     rec: [(
    #                                         date, amount
    #                                     )]
    #                                 })
    #             days = days + 1
    #             ex_start_date = ex_start_date + timedelta(days=1)
    #         result = OrderedDict(sorted(result.items()))
    #     return result

    # @api.multi
    # def get_month_sales_report(self):
    #     res = {}
    #     result = {}
    #     start_year_month = month_name
    #     form = {
    #         'date_start': self.start_date,
    #         'date_end': self.end_date,
    #     }

    #     date_start, date_end, start_year, end_year = self._get_utc_time_range(form)
    #     res = {
    #         start_year: dict(start_year_month)
    #     }
    #     if self:
    #         query = """
    #                 SELECT
    #                     to_char(date_order, 'YYYY') as year,
    #                     to_char(date_order, 'MM') as month_int,
    #                     initcap(lower(to_char(date_order, 'month'))) as month_name,
    #                     count(id),
    #                      ROUND(sum(amount_total), 2)
    #                 from
    #                     sale_order
    #                 where
    #                     date_order is not NULL and
    #                     to_char(date_order, 'YYYY') = '%s'
    #                     and state in ('sale', 'done')
    #                 group by year, month_int, month_name
    #                 order by year, month_int
    #             """ % (str(start_year))
    #         self.env.cr.execute(query)
    #         start_year_records = self.env.cr.fetchall()
    #         for rec in start_year_records:
    #             if rec[1].strip() in start_year_month:
    #                 res[start_year][rec[1].strip()].append((rec[3], rec[4]))
    #         for mth in res[start_year]:
    #             if len(res[start_year][mth]) == 0:
    #                 res[start_year][mth].append((0, 0.0))

    #         for year in res:
    #             result.update({
    #                 year: OrderedDict(sorted(res[year].items(), key=lambda t: t[0]))
    #             })
    #     return result

    @api.multi
    def print_weekly_sale_report(self):
        if self.env.context is None:
            self.env.context = {}
        return self.env['report'].get_action(
            self,
            'daily_sales_report.report_weekly_sale',
        )

    @api.multi
    def action_mail_weekly_sale_report(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('daily_sales_report', 'email_template_weekly_sale_order')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'weekly.sale.report',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "daily_sales_report.mail_template_data_notification_weekly_email_sale_order"
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


# class DailySalesReport(models.TransientModel):
#     _name = 'daily.sale.report'

#     date = fields.Date(string="Date", required=True)
#     company_id = fields.Many2one(
#         'res.company', string='Company',
#         default=lambda self: self.env.user.company_id
#     )

#     def _get_utc_time_range(self, form):
#         start_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_start'], tools.DEFAULT_SERVER_DATE_FORMAT))
#         start_date = start_date.date().strftime(tools.DEFAULT_SERVER_DATE_FORMAT)
#         return start_date

#     @api.multi
#     def get_daily_sales_grand_total(self):
#         res = 0.0
#         if self:
#             form = {
#                 'date_start': self.date,
#             }

#             date_start = self._get_utc_time_range(form)
#             query = """
#                 select
#                     so.name,
#                     partner.name,
#                     user_partner.name,
#                     pt.name,
#                     pp.barcode,
#                     sum(sol.product_uom_qty) as qty,
#                     sol.price_unit,
#                     sum(sol.price_subtotal) as sub_total,
#                     sum(so.amount_total) as total
#                 from sale_order as so
#                     left join sale_order_line as sol on sol.order_id = so.id
#                     left join product_product as pp on pp.id = sol.product_id
#                     left join product_template as pt on pt.id = pp.product_tmpl_id
#                     left join res_partner as partner on partner.id = so.partner_id
#                     left join res_users as usr on usr.id = so.user_id
#                     left join res_partner as user_partner on user_partner.id = usr.partner_id
#                 where so.date_order::date = '%s' and pt.name is not NULL
#                 group by pt.id, sol.price_unit, partner.id, user_partner.id, pp.id, so.id
#                 order by so.id
#             """ % (date_start)
#             self.env.cr.execute(query)
#             records = self.env.cr.fetchall()
#             if len(records) > 0:
#                 for rec in records:
#                     if len(rec) > 8 and rec[7]:
#                         res += float(rec[7]) or 0
#         return res

#     @api.multi
#     def get_daily_sale_report_details(self):
#         res = {}
#         if self:
#             form = {
#                 'date_start': self.date,
#             }

#             date_start = self._get_utc_time_range(form)
#             query = """
#                 select
#                     so.name,
#                     partner.name,
#                     user_partner.name,
#                     pt.name,
#                     pp.barcode,
#                     sum(sol.product_uom_qty) as qty,
#                     sol.price_unit,
#                     sum(sol.price_subtotal) as sub_total,
#                     sum(so.amount_total) as total
#                 from sale_order as so
#                     left join sale_order_line as sol on sol.order_id = so.id
#                     left join product_product as pp on pp.id = sol.product_id
#                     left join product_template as pt on pt.id = pp.product_tmpl_id
#                     left join res_partner as partner on partner.id = so.partner_id
#                     left join res_users as usr on usr.id = so.user_id
#                     left join res_partner as user_partner on user_partner.id = usr.partner_id
#                 where so.date_order::date = '%s' and pt.name is not NULL
#                 group by pt.id, sol.price_unit, partner.id, user_partner.id, pp.id, so.id
#                 order by so.id
#             """ % (date_start)
#             self.env.cr.execute(query)
#             records = self.env.cr.fetchall()
#             if len(records) > 0:
#                 for rec in records:
#                     vals = {
#                         'sale_order': rec[0],
#                         'cust_name': rec[1],
#                         'user': rec[2],
#                         'product': rec[3],
#                         'barcode': rec[4],
#                         'qty': rec[5],
#                         'price': rec[6],
#                         'subtotal': rec[7],
#                         'grand_total': rec[8],
#                     }
#                     if rec[0] in res:
#                         res[rec[0]].append(vals)
#                     else:
#                         res.update({
#                             rec[0]: [vals]
#                         })
    #                 if res:
#                     res = sorted(res.items(), key=operator.itemgetter(1))
#         return res

#     @api.multi
#     def print_daily_sale_report(self):
#         if self.env.context is None:
#             self.env.context = {}
#         return self.env['report'].get_action(
#             self,
#             'daily_sales_report.report_daily_sale',
#         )

#     @api.multi
#     def action_mail_daily_sale_report(self):
#         '''
#         This function opens a window to compose an email, with the edi sale template message loaded by default
#         '''
#         self.ensure_one()
#         ir_model_data = self.env['ir.model.data']
#         try:
#             template_id = ir_model_data.get_object_reference('daily_sales_report', 'email_template_daily_sale_order')[1]
#         except ValueError:
#             template_id = False
#         try:
#             compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
#         except ValueError:
#             compose_form_id = False
#         ctx = dict()
#         ctx.update({
#             'default_model': 'daily.sale.report',
#             'default_res_id': self.ids[0],
#             'default_use_template': bool(template_id),
#             'default_template_id': template_id,
#             'default_composition_mode': 'comment',
#             'mark_so_as_sent': True,
#             'custom_layout': "daily_sales_report.mail_template_data_notification_daily_email_sale_order"
#         })
#         return {
#             'type': 'ir.actions.act_window',
#             'view_type': 'form',
#             'view_mode': 'form',
#             'res_model': 'mail.compose.message',
#             'views': [(compose_form_id, 'form')],
#             'view_id': compose_form_id,
#             'target': 'new',
#             'context': ctx,
#         }
