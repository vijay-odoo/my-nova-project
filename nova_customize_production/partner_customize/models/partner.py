# -*- coding: utf-8 -*-

from odoo import api, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.multi
    @api.depends('name', 'email')
    def name_get(self):
        result = []
        for partner in self:
            name = '%s(%s)' % (partner.name, partner.email)
            result.append((partner.id, name))
        return result
