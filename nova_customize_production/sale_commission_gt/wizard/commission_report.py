# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime
# from odoo.exceptions import ValidationError


class SaleCommissionReport(models.TransientModel):
    _name = 'sale.commission.report'

    sale_person = fields.Many2one('res.users', 'Sale Person')
    start_date = fields.Date('Start Date', default=lambda *a:datetime.today() + relativedelta(day=1))
    end_date = fields.Date('End Date', default=lambda *a:datetime.today())
    type = fields.Selection([('payable','Payable'),('paid','Paid')], 'Payable/Paid', default='payable')

    @api.multi
    def action_commission_report(self):
        if self.type == 'payable':
            return self.env['report'].get_action(self, 'sale_commission_gt.payable_commission_report')
        if self.type == 'paid':
            return self.env['report'].get_action(self, 'sale_commission_gt.paid_commission_report')



class AllCommissionReport(models.TransientModel):
    _name = 'all.commission.report'

    start_date = fields.Date('Start Date', default=lambda *a: datetime.today() + relativedelta(day=1))
    end_date = fields.Date('End Date', default=lambda *a: datetime.today())
    type = fields.Selection([('payable', 'Payable'), ('paid', 'Paid'), ('all', 'All')], 'Payable/Paid/All', default='all')

    @api.multi
    def all_commission_report(self):
        return self.env['report'].get_action(self, 'sale_commission_gt.all_commission_report')


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: