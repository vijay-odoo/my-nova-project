# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT as DTF, DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError
from datetime import datetime
from openerp.tools.safe_eval import safe_eval


class SaleConfigSetting(models.TransientModel):
    _inherit = 'sale.config.settings'

    when_to_pay = fields.Selection([
            ('0', 'Sale Validation'),
            ('1', 'Invoice Validation'),
            ('2', 'Payment Validation'),
        ], "When To Pay", default='0'
    )

    commission_calculation = fields.Selection([
        ('0', 'Product'),
        ('1', 'Product Category'),
        ('2', 'Sales Team'),
        ], "Calculation Based On", default='0')

    @api.multi
    def set_when_to_pay(self):
        data = self.env['ir.values'].sudo().set_default('sale.config.settings', 'when_to_pay', self.when_to_pay)
        return data

    @api.multi
    def set_commission_calculation(self):
        product_obj = self.env['product.template']
        product_categ_obj = self.env['product.category']
        crm_team_obj = self.env['crm.team']

        data = self.env['ir.values'].sudo().set_default('sale.config.settings', 'commission_calculation', self.commission_calculation)

        if self.commission_calculation == '0':
            # print "Product : "
            product_search = product_obj.search([]).ids
            product_browse = product_obj.browse(product_search)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': True})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': False})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': False})

        if self.commission_calculation == '1':
            # print "Product : "
            product_search = product_obj.search([]).ids
            product_browse = product_obj.browse(product_search)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': False})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': True})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': False})

        if self.commission_calculation == '2':
            # print "Product : "
            product_search = product_obj.search([]).ids
            product_browse = product_obj.browse(product_search)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': False})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': False})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': True})
        return data


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        res = super(SaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        res.write({'order_pay_state': order.order_pay_state})
        return res


class SaleTeam(models.Model):
    _inherit = 'crm.team'

    sale_manage_comm = fields.Float("Sale Manager Commission(%)")
    sale_member_comm = fields.Float("Sale Member Commission(%)")
    first_order_comm = fields.Float("First Order Commission(%)",  default=10.0)
    is_sale_team_comm = fields.Boolean("Is Sale Team Commission ?")


class ResUsers(models.Model):
    _inherit = 'res.users'

    commission_category = fields.Many2one('product.category', string="Commission Category")
    commission_category_amt = fields.Float("Commission Product Amount (%)", default=10.0)
    product_commission = fields.Boolean("Product Commission")

    first_commission = fields.Boolean("First Order Commission")
    first_commission_old_start_date = fields.Date("Old Order Start Date")
    first_commission_old_end_date = fields.Date("Old Order End Date")
    first_commission_old_amt = fields.Float("Old Order Amount (%)", default=10.0)
    first_commission_new_start_date = fields.Date("New Order Start Date")
    first_commission_new_end_date = fields.Date("New Order End Date")
    first_commission_new_amt = fields.Float("New Order Amount(%)", default=10.0)

    reorder_commission = fields.Boolean("ReOrder Commission")
    reorder_commission_old_start_date = fields.Date("Old Order Start Date")
    reorder_commission_old_end_date = fields.Date("Old Order End Date")
    reorder_commission_old_amt = fields.Float("Old Reorder Amount (%)", default=10.0)
    reorder_commission_new_start_date = fields.Date("New Order Start Date")
    reorder_commission_new_end_date = fields.Date("New Order End Date")
    reorder_commission_new_amt = fields.Float("New Reorder Amount(%)", default=20.0)

    commission_amount_limit = fields.Float("Total Sale Amount Limit", default=25000.00)
    extra_commission = fields.Boolean("Extra Commission")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    total_sale_amount = fields.Float("Total Sale Amount")
    extra_commission_amount = fields.Float("Commission Amount", default=500.0)

    tier_commission = fields.Boolean("Tier Commission")
    tier_commission_new_start_date = fields.Date("Order Start Date")
    tier_commission_new_end_date = fields.Date("Order End Date")
    tier_low_commission = fields.Float("Tier 0<5000 (%)", default=10.0)
    tier_medium_commission = fields.Float("Tier 5000>20000 (%)", default=15.0)
    tier_high_commission = fields.Float("Tier 20000> (%)", default=20.0)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_appy_commission_person = fields.Boolean("Apply Commission person")
    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")
    commission_count_manage = fields.Float('Sale Manager Commission', default=0.0)
    commission_count_member = fields.Float('Sale Person Commission', default=0.0)

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        payment_setting_state = self.env['ir.values'].get_default('sale.config.settings', 'when_to_pay')
        res.write({'order_pay_state': payment_setting_state})
        return res

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        res = super(SaleOrder, self).action_invoice_create(grouped=grouped, final=final)
        invoice_obj = self.env['account.invoice']
        invoice_search = invoice_obj.search([('id', '=', res[0])])
        invoice_browse = invoice_obj.browse(invoice_search.id)
        invoice_browse.write({'order_pay_state': self.order_pay_state})
        return res

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref, sale_commission, order_commission_status, sale_id):
        commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_journal)
        if sale_commission == 0:
            sale_commission = '0'
        else:
            sale_commission = sale_commission and str(sale_commission) or '3'
        if order_commission_status == 0:
            order_commission_status = '0'
        else:
            order_commission_status = order_commission_status and str(order_commission_status) or '2'
        if account_id and sale_person and commission_amt > 0:
            commission_vals = {
                'name': account_id.default_debit_account_id.name,
                'debit': commission_amt,
                'credit': 0.0,
                'account_id': account_id.default_debit_account_id.id,
                'partner_id': sale_person.partner_id.id,
                'commission_user': sale_person.id,
                'commission_on': self.order_pay_state,
                'sales_commission': sale_commission,
                'order_commission_status': order_commission_status,
                'ref_invoice': journal_ref,
                'ref': sale_id.name
            }
            sale_person_vals = {
                'name': sale_person.name,
                'debit': 0.0,
                'credit': commission_amt,
                'account_id': sale_person.property_account_payable_id.id,
                'partner_id': sale_person.partner_id.id,
                'commission_user': sale_person.id,
                'commission_on': self.order_pay_state,
                'sales_commission': sale_commission,
                'order_commission_status': order_commission_status,
                'ref_invoice': journal_ref,
                'ref': sale_id.name
            }
            now = datetime.now()
            vals = {
                'journal_id': account_id.id,
                'date': sale_id and sale_id.confirmation_date or now.strftime('%Y-%m-%d'),
                'state': 'draft',
                'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                'ref': journal_ref,
                'partner_id': sale_person.partner_id.id,
                'commission_user': sale_person.id,
            }
            move = self.env['account.move'].create(vals)
            move.post()
            return move.id

    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for order in self:
            commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
            if order.order_pay_state == '0':
                if commission_type == '0':
                    total_order_commission = 0
                    sale_commission = 2
                    is_commision_order_product = False
                    is_commision_order_product = order.order_line.filtered(
                        lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                    )
                    if is_commision_order_product:
                        order_commission_status = 0
                    else:
                        order_commission_status = 1
                    for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
                            product_commission = (
                                                 (line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.prod_comm_per) / 100
                            total_order_commission += product_commission
                    order.commission_count_entries(total_order_commission, order.user_id, order.name, sale_commission, order_commission_status, order)

                if commission_type == '1':
                    total_order_commission = 0
                    sale_commission = 2
                    is_commision_order_product = False
                    is_commision_order_product = order.order_line.filtered(
                        lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                    )
                    if is_commision_order_product:
                        order_commission_status = 0
                    else:
                        order_commission_status = 1
                    for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (
                                                       (line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.categ_id.prod_categ_comm_per) / 100
                            total_order_commission += product_categ_commission
                    order.commission_count_entries(total_order_commission, order.user_id, order.name, sale_commission, order_commission_status, order)

                if commission_type == '2':
                    if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                        sale_commission = 0
                        is_commision_order_product = False
                        is_commision_order_product = order.order_line.filtered(
                            lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                        )
                        if is_commision_order_product:
                            order_commission_status = 0
                        else:
                            order_commission_status = 1
                        manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                        order.commission_count_entries(manager_commission, order.team_id.user_id, order.name, sale_commission, order_commission_status, order)
                    if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                        sale_commission = 1
                        is_commision_order_product = False
                        is_commision_order_product = order.order_line.filtered(
                            lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                        )
                        if is_commision_order_product:
                            order_commission_status = 0
                        else:
                            order_commission_status = 1
                        member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                        order.commission_count_entries(member_commission, order.user_id, order.name, sale_commission, order_commission_status, order)

            if order.order_pay_state == '2':
                if commission_type == '0':
                    total_order_commission = 0
                    for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
                            product_commission = (
                                                 (line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.prod_comm_per) / 100
                            total_order_commission += product_commission
                    order.write({'commission_count_member': total_order_commission})

                if commission_type == '1':
                    total_order_commission = 0
                    for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (
                                                       (line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.categ_id.prod_categ_comm_per) / 100
                            total_order_commission += product_categ_commission
                    order.write({'commission_count_member': total_order_commission})

                if commission_type == '2':
                    if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                        total_order_amt = 0
                        is_commision_order_product = False
                        is_commision_order_product = order.order_line.filtered(
                            lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                        )
                        for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
                            product_commission = line_id.price_unit * line_id.product_uom_qty
                            total_order_amt += product_commission
                        for line_id in order.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                            product_commission = line_id.price_unit * line_id.product_uom_qty
                            total_order_amt -= abs(product_commission)
                        if is_commision_order_product:
                            manager_commission = (total_order_amt * order.team_id.first_order_comm) / 100
                        else:
                            manager_commission = (s * order.team_id.sale_manage_comm) / 100
                        order.write({'commission_count_manage': manager_commission})
                        #order.commission_count_entries(manager_commission, order.user_id, order.name)

                    if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                        total_order_amt = 0
                        is_commision_order_product = False
                        is_commision_order_product = order.order_line.filtered(
                            lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                        )
                        for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
                            product_commission = line_id.price_unit * line_id.product_uom_qty
                            total_order_amt += product_commission
                        for line_id in order.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                            product_commission = line_id.price_unit * line_id.product_uom_qty
                            total_order_amt -= abs(product_commission)
                        if is_commision_order_product:
                            member_commission = (total_order_amt * order.team_id.first_order_comm) / 100
                        else:
                            member_commission = (total_order_amt * order.team_id.sale_member_comm) / 100
                        order.write({'commission_count_member': member_commission})
                        #order.commission_count_entries(member_commission, order.user_id, order.name)
                    # if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                    #     manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                    #     order.write({'commission_count_manage': manager_commission})

                    # if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                    #     member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                    #     order.write({'commission_count_member': member_commission})
        return res

    def _get_utc_time_range_commission(self, form):
        start_date = False
        end_date = False
        if form.get('date_start', False):
            start_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_start'], DF))
            start_date = start_date.date()
        if form.get('date_end', False):
            end_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_end'], DF))
            end_date = end_date.date()
        return start_date, end_date

    @api.multi
    def reset_commission(self):
        # orders = self.search([('state', 'in', ['sale', 'done'])])
        # payment_setting_state = self.env['ir.values'].get_default('sale.config.settings', 'when_to_pay')
        # commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
        # for order in orders:
        #     move = self.env['account.move.line'].search([('ref', '=', order.name), ('source_amount', '!=', False)], limit=1)
        #     if move:
        #         return

        #     order.write({'order_pay_state': payment_setting_state})
        #     if order.order_pay_state == '0':
        #         if commission_type == '0':
        #             total_order_commission = 0
        #             sale_commission = 2
        #             is_commision_order_product = False
        #             is_commision_order_product = order.order_line.filtered(
        #                 lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #             )
        #             if is_commision_order_product:
        #                 order_commission_status = 0
        #             else:
        #                 order_commission_status = 1
        #             for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                 if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
        #                     product_commission = ((line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.prod_comm_per) / 100
        #                     total_order_commission += product_commission
        #             order.commission_count_entries(total_order_commission, order.user_id, order.name, sale_commission, order_commission_status)

        #         if commission_type == '1':
        #             total_order_commission = 0
        #             sale_commission = 2
        #             is_commision_order_product = False
        #             is_commision_order_product = order.order_line.filtered(
        #                 lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #             )
        #             if is_commision_order_product:
        #                 order_commission_status = 0
        #             else:
        #                 order_commission_status = 1
        #             for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                 if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
        #                     product_categ_commission = ((line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.categ_id.prod_categ_comm_per) / 100
        #                     total_order_commission += product_categ_commission
        #             order.commission_count_entries(total_order_commission, order.user_id, order.name, sale_commission, order_commission_status)

        #         if commission_type == '2':
        #             if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
        #                 sale_commission = 0
        #                 is_commision_order_product = False
        #                 is_commision_order_product = order.order_line.filtered(
        #                     lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #                 )
        #                 if is_commision_order_product:
        #                     order_commission_status = 0
        #                 else:
        #                     order_commission_status = 1
        #                 manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
        #                 order.commission_count_entries(manager_commission, order.team_id.user_id, order.name, sale_commission, order_commission_status)
        #             if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
        #                 sale_commission = 1
        #                 is_commision_order_product = False
        #                 is_commision_order_product = order.order_line.filtered(
        #                     lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #                 )
        #                 if is_commision_order_product:
        #                     order_commission_status = 0
        #                 else:
        #                     order_commission_status = 1
        #                 member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
        #                 order.commission_count_entries(member_commission, order.user_id, order.name, sale_commission, order_commission_status)

        #     if order.order_pay_state == '2':
        #         if commission_type == '0':
        #             total_order_commission = 0
        #             for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                 if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
        #                     product_commission = ((line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.prod_comm_per) / 100
        #                     total_order_commission += product_commission
        #             order.write({'commission_count_member': total_order_commission})

        #         if commission_type == '1':
        #             total_order_commission = 0
        #             for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                 if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
        #                     product_categ_commission = ((line_id.price_unit * line_id.product_uom_qty) * line_id.product_id.categ_id.prod_categ_comm_per) / 100
        #                     total_order_commission += product_categ_commission
        #             order.write({'commission_count_member': total_order_commission})

        #         if commission_type == '2':
        #             if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
        #                 total_order_amt = 0
        #                 sale_commission = 0
        #                 is_commision_order_product = False
        #                 is_commision_order_product = order.order_line.filtered(
        #                     lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #                 )
        #                 for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                     product_commission = line_id.price_unit * line_id.product_uom_qty
        #                     total_order_amt += product_commission
        #                 for line_id in order.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
        #                     product_commission = line_id.price_unit * line_id.product_uom_qty
        #                     total_order_amt -= abs(product_commission)
        #                 if is_commision_order_product:
        #                     order_commission_status = 0
        #                     manager_commission = (total_order_amt * order.team_id.first_order_comm) / 100
        #                 else:
        #                     order_commission_status = 1
        #                     manager_commission = (total_order_amt * order.team_id.sale_manage_comm) / 100
        #                 order.write({'commission_count_manage': manager_commission})
        #                 order.commission_count_entries(manager_commission, order.user_id, order.name, sale_commission, order_commission_status)

        #             if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
        #                 total_order_amt = 0
        #                 sale_commission = 1
        #                 is_commision_order_product = False
        #                 is_commision_order_product = order.order_line.filtered(
        #                     lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #                 )
        #                 for line_id in order.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                     product_commission = line_id.price_unit * line_id.product_uom_qty
        #                     total_order_amt += product_commission
        #                 for line_id in order.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
        #                     product_commission = line_id.price_unit * line_id.product_uom_qty
        #                     total_order_amt -= abs(product_commission)
        #                 if is_commision_order_product:
        #                     member_commission = (total_order_amt * order.team_id.first_order_comm) / 100
        #                     order_commission_status = 0
        #                 else:
        #                     member_commission = (total_order_amt * order.team_id.sale_member_comm) / 100
        #                     order_commission_status = 1
        #                 order.write({'commission_count_member': member_commission})
        #                 order.commission_count_entries(member_commission, order.user_id, order.name, sale_commission, order_commission_status)

                    # if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                    #     manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                    #     order.write({'commission_count_manage': manager_commission})
                    #     self.commission_count_entries(manager_commission, order.user_id, order.name)

                    # if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                    #     member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                    #     order.write({'commission_count_member': member_commission})
                    #     self.commission_count_entries(member_commission, order.user_id, order.name)
        invoice_obj = self.env['account.invoice']
        self.env.cr.execute("""
            UPDATE res_users set total_sale_amount = 0.0
        """)
        sale_obj = self.env['sale.order']
        invoices = invoice_obj.search([('state', 'in', ['paid'])])
        for invoice in invoices:
            if invoice.order_pay_state == '2':
                sale_id = sale_obj.search([('name', '=', invoice.origin)])
                if sale_id and sale_id.user_id and sale_id.user_id.first_commission:
                    if sale_id.confirmation_date:
                        order_date = fields.Datetime.context_timestamp(self, datetime.strptime(sale_id.confirmation_date, DTF))
                        order_date = order_date.date()
                        dates = {
                            'date_start': sale_id.user_id.first_commission_old_start_date,
                            'date_end': sale_id.user_id.first_commission_old_end_date,
                        }
                        old_date_start, old_date_end = sale_id._get_utc_time_range_commission(dates)
                        dates = {
                            'date_start': sale_id.user_id.first_commission_new_start_date,
                            'date_end': sale_id.user_id.first_commission_new_end_date,
                        }
                        new_date_start, new_date_end = sale_id._get_utc_time_range_commission(dates)
                        first_commission_amt = 0.0
                        is_first_commission = False
                        if old_date_start and old_date_end and old_date_start <= order_date <= old_date_end:
                            first_commission_amt = sale_id.user_id.first_commission_old_amt
                            is_first_commission = True
                        if new_date_start and new_date_end and new_date_start <= order_date <= new_date_end:
                            first_commission_amt = sale_id.user_id.first_commission_new_amt
                            is_first_commission = True
                        if is_first_commission:
                            is_commision_order_product = False
                            is_commision_order_product = sale_id.order_line.filtered(
                                lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                            )
                            if is_commision_order_product:
                                order_commission_status = 0
                                sale_commission = 1
                                total_order_amt = 0
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt += product_commission
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt -= abs(product_commission)
                                manage_total_commission = (total_order_amt * first_commission_amt or 10) / 100
                                reference = invoice.number
                                invoice.commission_count_entries(
                                    manage_total_commission, sale_id.user_id,
                                    reference, sale_commission, order_commission_status, sale_id
                                )
                if sale_id and sale_id.user_id and sale_id.user_id.reorder_commission:
                    is_commision_order_product = sale_id.order_line.filtered(
                        lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                    )
                    if not is_commision_order_product:
                        if sale_id.confirmation_date:
                            order_date = fields.Datetime.context_timestamp(self, datetime.strptime(sale_id.confirmation_date, DTF))
                            order_date = order_date.date()
                            dates = {
                                'date_start': sale_id.user_id.reorder_commission_old_start_date,
                                'date_end': sale_id.user_id.reorder_commission_old_end_date,
                            }
                            old_date_start, old_date_end = sale_id._get_utc_time_range_commission(dates)
                            dates = {
                                'date_start': sale_id.user_id.reorder_commission_new_start_date,
                                'date_end': sale_id.user_id.reorder_commission_new_end_date,
                            }
                            new_date_start, new_date_end = sale_id._get_utc_time_range_commission(dates)
                            reorder_commission_amt = 0.0
                            is_reorder_commission = False
                            if old_date_start and old_date_end and old_date_start <= order_date <= old_date_end:
                                reorder_commission_amt = sale_id.user_id.reorder_commission_old_amt
                                is_reorder_commission = True
                            if new_date_start and new_date_end and new_date_start <= order_date <= new_date_end:
                                reorder_commission_amt = sale_id.user_id.reorder_commission_new_amt
                                is_reorder_commission = True
                            if is_reorder_commission:
                                order_commission_status = 1
                                sale_commission = 1
                                total_order_amt = 0
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt += product_commission
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt -= abs(product_commission)
                                manage_total_commission = (total_order_amt * reorder_commission_amt or 10) / 100
                                reference = invoice.number
                                invoice.commission_count_entries(
                                    manage_total_commission, sale_id.user_id,
                                    reference, sale_commission, order_commission_status, sale_id
                                )

                if sale_id and sale_id.user_id and sale_id.user_id.product_commission:
                    total_product_commission = 0.0
                    if sale_id.user_id.commission_category:
                        for line in sale_id.order_line.filtered(
                            lambda r: r.product_id.product_tmpl_id.categ_id.id == sale_id.user_id.commission_category.id and r.product_id.type != 'service'
                        ):
                            if sale_id.user_id.commission_category_amt:
                                product_commission = (line.price_subtotal * sale_id.user_id.commission_category_amt) / 100
                                total_product_commission += product_commission
                            if total_product_commission:
                                sale_commission = 2
                                order_commission_status = 4
                                reference = invoice.number
                                invoice.commission_count_entries(
                                    total_product_commission, sale_id.user_id,
                                    reference, sale_commission, order_commission_status, sale_id
                                )

                if sale_id and sale_id.user_id and sale_id.user_id.extra_commission:
                    dates = {
                        'date_start': sale_id.user_id.start_date,
                        'date_end': sale_id.user_id.end_date,
                    }
                    date_start, date_end = invoice._get_utc_time_range(dates)
                    order_date = fields.Datetime.context_timestamp(
                        invoice, datetime.strptime(
                            sale_id.date_order, DTF
                        )
                    )
                    oder_date = order_date.date()
                    if date_start <= oder_date <= date_end:
                        reference = invoice.number
                        total_user_sale_amount = sale_id.user_id.total_sale_amount or 0.0
                        total_order_amt = 0
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt += product_commission
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt -= abs(product_commission)
                        sale_amount = total_order_amt or 0.0
                        total_amount = sale_amount + total_user_sale_amount
                        sale_id.is_appy_commission_person = True
                        if total_amount >= sale_id.user_id.commission_amount_limit:
                            if (total_amount - sale_id.user_id.commission_amount_limit) >= sale_id.user_id.commission_amount_limit:
                                amount = total_amount
                                count = 0
                                while (amount >= sale_id.user_id.commission_amount_limit):
                                    amount = amount - sale_id.user_id.commission_amount_limit
                                    count = count + 1
                                sale_id.user_id.total_sale_amount = total_amount - (sale_id.user_id.commission_amount_limit * (count or 1))
                                total_extra_commission = (sale_id.user_id.extra_commission_amount or 500) * (count or 1)
                            else:
                                sale_id.user_id.total_sale_amount = total_amount - sale_id.user_id.commission_amount_limit
                                total_extra_commission = sale_id.user_id.extra_commission_amount or 500
                            sale_commission = 1
                            order_commission_status = 3
                            invoice.commission_count_entries(
                                total_extra_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )
                        else:
                            sale_id.user_id.total_sale_amount = invoice.amount_total
                if sale_id and sale_id.partner_id and sale_id.user_id and sale_id.user_id.tier_commission:
                    dates = {
                        'date_start': sale_id.user_id.tier_commission_new_start_date,
                        'date_end': sale_id.user_id.tier_commission_new_end_date,
                    }
                    date_start, date_end = sale_id._get_utc_time_range_commission(dates)
                    order_date = fields.Datetime.context_timestamp(
                        invoice, datetime.strptime(
                            sale_id.date_order, DTF
                        )
                    )
                    oder_date = order_date.date()
                    if date_start <= oder_date <= date_end:
                        total_order_amt = 0
                        total_product_commission = 0
                        total_invoiced = sale_id.partner_id.total_invoiced or 0.0
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt += product_commission
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt -= abs(product_commission)
                        total_invoiced = total_invoiced + total_order_amt
                        if total_invoiced <= 5000:
                            product_commission = (total_order_amt * sale_id.user_id.tier_low_commission) / 100
                            total_product_commission += product_commission
                        elif total_invoiced > 5000 and total_invoiced <= 20000:
                            product_commission = (total_order_amt * sale_id.user_id.tier_medium_commission) / 100
                            total_product_commission += product_commission
                        elif total_invoiced > 20000:
                            product_commission = (total_order_amt * sale_id.user_id.tier_high_commission) / 100
                            total_product_commission += product_commission
                        if total_product_commission:
                            sale_commission = 2
                            order_commission_status = 5
                            reference = invoice.number
                            invoice.commission_count_entries(
                                total_product_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
