# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    prod_comm_per = fields.Float("Sale Commission(%)")
    is_prod_comm = fields.Boolean("Is Product Commission ?", default=True)


class ProductCategory(models.Model):
    _inherit = 'product.category'

    prod_categ_comm_per = fields.Float("Sale Commission(%)")
    is_prod_categ_comm = fields.Boolean("Is Product Category Commission ?")


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: