# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT as DTF, DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError
from datetime import datetime


class AccountConfigSetting(models.TransientModel):
    _inherit = 'account.config.settings'

    commission_journal = fields.Many2one('account.journal', 'Commission Journal')

    @api.multi
    def set_commission_journal(self):
        data = self.env['ir.values'].sudo().set_default('account.config.settings', 'commission_journal', self.commission_journal.id)
        return data


class AccountMove(models.Model):
    _inherit = 'account.move'

    commission_user = fields.Many2one('res.users', 'Sale Person')


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    ref_invoice = fields.Char(string="Refence Invoice Number")
    commission_user = fields.Many2one('res.users', 'Sale Person')
    paid = fields.Boolean('Commission Paid')
    source_amount = fields.Float('Source Amount', )
    source_currency = fields.Many2one('res.currency', store=True, string="Source Currency")
    commission_on = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")
    sales_commission = fields.Selection([
        ('0', 'Sales Manager'),
        ('1', 'Sales Member'),
        ('2', 'Product'),
        ('3', 'None')
    ], "Sales Commission")
    order_commission_status = fields.Selection([
        ('0', 'First Order'),
        ('1', 'Reorder'),
        ('2', 'None'),
        ('3', 'Extra'),
        ('4', 'Product Category'),
        ('5', 'Tier'),
    ], "Commission Status")


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref, sale_commission, order_commission_status, sale_id):
        commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_journal)
        if sale_commission == 0:
            sale_commission = '0'
        else:
            sale_commission = sale_commission and str(sale_commission) or '3'
        if order_commission_status == 0:
            order_commission_status = '0'
        else:
            order_commission_status = order_commission_status and str(order_commission_status) or '2'
        if account_id and sale_person and commission_amt > 0:
            commission_vals = {
                'name': account_id.default_debit_account_id.name,
                'debit': commission_amt,
                'credit': 0.0,
                'account_id': account_id.default_debit_account_id.id,
                'partner_id': sale_person.partner_id.id,
                'commission_user': sale_person.id,
                'commission_on': self.order_pay_state,
                'sales_commission': sale_commission,
                'order_commission_status': order_commission_status,
                'ref_invoice': journal_ref,
                'ref': sale_id.name
            }
            sale_person_vals = {
                'name': sale_person.name,
                'debit': 0.0,
                'credit': commission_amt,
                'account_id': sale_person.property_account_payable_id.id,
                'partner_id': sale_person.partner_id.id,
                'commission_user': sale_person.id,
                'commission_on': self.order_pay_state,
                'sales_commission': sale_commission,
                'order_commission_status': order_commission_status,
                'ref_invoice': journal_ref,
                'ref': sale_id.name
            }
            now = datetime.now()
            vals = {
                'journal_id': account_id.id,
                'date': sale_id and sale_id.confirmation_date or now.strftime('%Y-%m-%d'),
                'state': 'draft',
                'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                'ref': journal_ref,
                'partner_id': sale_person.partner_id.id,
                'commission_user': sale_person.id
            }
            move = self.env['account.move'].create(vals)
            move.post()
            return move.id

    @api.multi
    def action_invoice_open(self):
        commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
        res = super(AccountInvoice, self).action_invoice_open()
        if self.order_pay_state == '1':
            if commission_type == '0':
                total_invoice_commission = 0
                sale_obj = self.env['sale.order']
                sale_search = sale_obj.search([('name', '=', self.origin)])
                is_commision_order_product = False
                is_commision_order_product = sale_search[0].order_line.filtered(
                    lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                )
                sale_commission = 2
                if is_commision_order_product:
                    order_commission_status = 0
                else:
                    order_commission_status = 1
                for invoice_line_id in self.invoice_line_ids:
                    for product_line in sale_search.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                            product_commission = (invoice_line_id.price_subtotal * product_line.product_id.prod_comm_per) / 100
                            total_invoice_commission += product_commission
                reference = self.number
                self.commission_count_entries(total_invoice_commission, self.user_id, reference, sale_commission, order_commission_status, sale_search)

            if commission_type == '1':
                total_invoice_commission = 0
                sale_obj = self.env['sale.order']
                sale_search = sale_obj.search([('name','=',self.origin)])
                is_commision_order_product = False
                is_commision_order_product = sale_search[0].order_line.filtered(
                    lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                )
                sale_commission = 2
                if is_commision_order_product:
                    order_commission_status = 0
                else:
                    order_commission_status = 1
                for invoice_line_id in self.invoice_line_ids:
                    for product_line in sale_search.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (invoice_line_id.price_subtotal * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                            total_invoice_commission += product_categ_commission
                reference = self.number
                self.commission_count_entries(total_invoice_commission, self.user_id, reference, sale_commission, order_commission_status, sale_search)

            if commission_type == '2':
                if self.team_id.sale_manage_comm and self.team_id.sale_manage_comm > 0:
                    sale_commission = 0
                    sale_obj = self.env['sale.order']
                    sale_search = sale_obj.search([('name','=',self.origin)])
                    is_commision_order_product = False
                    is_commision_order_product = sale_search[0].order_line.filtered(
                        lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                    )
                    if is_commision_order_product:
                        order_commission_status = 0
                    else:
                        order_commission_status = 1
                    manager_commission = (self.amount_untaxed * self.team_id.sale_manage_comm) / 100
                    reference = self.number
                    self.commission_count_entries(manager_commission, self.team_id.user_id, reference, sale_commission, order_commission_status, sale_search)

                if self.team_id.sale_member_comm and self.team_id.sale_member_comm > 0:
                    sale_commission = 1
                    sale_commission = 0
                    sale_obj = self.env['sale.order']
                    sale_search = sale_obj.search([('name','=',self.origin)])
                    is_commision_order_product = False
                    is_commision_order_product = sale_search[0].order_line.filtered(
                        lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                    )
                    if is_commision_order_product:
                        order_commission_status = 0
                    else:
                        order_commission_status = 1
                    member_commission = (self.amount_untaxed * self.team_id.sale_member_comm) / 100
                    reference = self.number
                    self.commission_count_entries(member_commission, self.user_id, reference, sale_commission, order_commission_status, sale_search)
        return res

    def _get_utc_time_range(self, form):
        start_date = False
        end_date = False
        if form.get('date_start', False):
            start_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_start'], DF))
            start_date = start_date.date()
        if form.get('date_end', False):
            end_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_end'], DF))
            end_date = end_date.date()
        return start_date, end_date

    @api.multi
    def reset_commission(self):
        # invoices = self.search([('state', 'in', ['paid'])])
        # commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
        # for invoice in invoices:
        #     if invoice.order_pay_state == '1':
        #         if commission_type == '0':
        #             total_invoice_commission = 0
        #             sale_obj = self.env['sale.order']
        #             sale_search = sale_obj.search([('name', '=', invoice.origin)])
        #             is_commision_order_product = False
        #             is_commision_order_product = sale_search[0].order_line.filtered(
        #                 lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #             )
        #             sale_commission = 2
        #             if is_commision_order_product:
        #                 order_commission_status = 0
        #             else:
        #                 order_commission_status = 1
        #             for invoice_line_id in invoice.invoice_line_ids:
        #                 for product_line in sale_search.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                     if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
        #                         product_commission = (invoice_line_id.price_subtotal * product_line.product_id.prod_comm_per) / 100
        #                         total_invoice_commission += product_commission
        #             reference = '[' + invoice.name + '] ' + invoice.number
        #             invoice.commission_count_entries(total_invoice_commission, invoice.user_id, reference, sale_commission, order_commission_status)

        #         if commission_type == '1':
        #             total_invoice_commission = 0
        #             sale_obj = self.env['sale.order']
        #             sale_search = sale_obj.search([('name', '=', invoice.origin)])
        #             is_commision_order_product = False
        #             is_commision_order_product = sale_search[0].order_line.filtered(
        #                 lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #             )
        #             sale_commission = 2
        #             if is_commision_order_product:
        #                 order_commission_status = 0
        #             else:
        #                 order_commission_status = 1
        #             for invoice_line_id in invoice.invoice_line_ids:
        #                 for product_line in sale_search.order_line.filtered(lambda r: r.product_id.type != 'service'):
        #                     if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
        #                         product_categ_commission = (invoice_line_id.price_subtotal * product_line.product_id.categ_id.prod_categ_comm_per) / 100
        #                         total_invoice_commission += product_categ_commission
        #             reference = '[' + invoice.name + '] ' + invoice.number
        #             invoice.commission_count_entries(total_invoice_commission, invoice.user_id, reference, sale_commission, order_commission_status)

        #         if commission_type == '2':
        #             if invoice.team_id.sale_manage_comm and invoice.team_id.sale_manage_comm > 0:
        #                 sale_commission = 0
        #                 sale_obj = self.env['sale.order']
        #                 sale_search = sale_obj.search([('name','=',invoice.origin)])
        #                 is_commision_order_product = False
        #                 is_commision_order_product = sale_search[0].order_line.filtered(
        #                     lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #                 )
        #                 if is_commision_order_product:
        #                     order_commission_status = 0
        #                 else:
        #                     order_commission_status = 1
        #                 manager_commission = (invoice.amount_untaxed * invoice.team_id.sale_manage_comm) / 100
        #                 reference = '[' + invoice.name + '] ' + invoice.number
        #                 invoice.commission_count_entries(manager_commission, invoice.team_id.user_id, reference)

        #             if invoice.team_id.sale_member_comm and invoice.team_id.sale_member_comm > 0:
        #                 sale_commission = 1
        #                 sale_obj = self.env['sale.order']
        #                 sale_search = sale_obj.search([('name','=',invoice.origin)])
        #                 is_commision_order_product = False
        #                 is_commision_order_product = sale_search[0].order_line.filtered(
        #                     lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
        #                 )
        #                 if is_commision_order_product:
        #                     order_commission_status = 0
        #                 else:
        #                     order_commission_status = 1
        #                 member_commission = (invoice.amount_untaxed * invoice.team_id.sale_member_comm) / 100
        #                 reference = '[' + invoice.name + '] ' + invoice.number
        #                 invoice.commission_count_entries(member_commission, invoice.user_id, reference, sale_commission, order_commission_status)
        self.env.cr.execute("""
            UPDATE res_users set total_sale_amount = 0.0
        """)
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        invoices = invoice_obj.search([('state', 'in', ['paid'])])
        for invoice in invoices:
            if invoice.order_pay_state == '2':
                sale_id = sale_obj.search([('name', '=', invoice.origin)])
                if sale_id and sale_id.user_id and sale_id.user_id.first_commission:
                    if sale_id.confirmation_date:
                        order_date = fields.Datetime.context_timestamp(self, datetime.strptime(sale_id.confirmation_date, DTF))
                        order_date = order_date.date()
                        dates = {
                            'date_start': sale_id.user_id.first_commission_old_start_date,
                            'date_end': sale_id.user_id.first_commission_old_end_date,
                        }
                        old_date_start, old_date_end = sale_id._get_utc_time_range_commission(dates)
                        dates = {
                            'date_start': sale_id.user_id.first_commission_new_start_date,
                            'date_end': sale_id.user_id.first_commission_new_end_date,
                        }
                        new_date_start, new_date_end = sale_id._get_utc_time_range_commission(dates)
                        first_commission_amt = 0.0
                        is_first_commission = False
                        if old_date_start and old_date_end and old_date_start <= order_date <= old_date_end:
                            first_commission_amt = sale_id.user_id.first_commission_old_amt
                            is_first_commission = True
                        if new_date_start and new_date_end and new_date_start <= order_date <= new_date_end:
                            first_commission_amt = sale_id.user_id.first_commission_new_amt
                            is_first_commission = True
                        if is_first_commission:
                            is_commision_order_product = False
                            is_commision_order_product = sale_id.order_line.filtered(
                                lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                            )
                            if is_commision_order_product:
                                order_commission_status = 0
                                sale_commission = 1
                                total_order_amt = 0
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt += product_commission
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt -= abs(product_commission)
                                manage_total_commission = (total_order_amt * first_commission_amt or 10) / 100
                                reference = invoice.number
                                invoice.commission_count_entries(
                                    manage_total_commission, sale_id.user_id,
                                    reference, sale_commission, order_commission_status, sale_id
                                )
                if sale_id and sale_id.user_id and sale_id.user_id.reorder_commission:
                    is_commision_order_product = sale_id.order_line.filtered(
                        lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                    )
                    if not is_commision_order_product:
                        if sale_id.confirmation_date:
                            order_date = fields.Datetime.context_timestamp(self, datetime.strptime(sale_id.confirmation_date, DTF))
                            order_date = order_date.date()
                            dates = {
                                'date_start': sale_id.user_id.reorder_commission_old_start_date,
                                'date_end': sale_id.user_id.reorder_commission_old_end_date,
                            }
                            old_date_start, old_date_end = sale_id._get_utc_time_range_commission(dates)
                            dates = {
                                'date_start': sale_id.user_id.reorder_commission_new_start_date,
                                'date_end': sale_id.user_id.reorder_commission_new_end_date,
                            }
                            new_date_start, new_date_end = sale_id._get_utc_time_range_commission(dates)
                            reorder_commission_amt = 0.0
                            is_reorder_commission = False
                            if old_date_start and old_date_end and old_date_start <= order_date <= old_date_end:
                                reorder_commission_amt = sale_id.user_id.reorder_commission_old_amt
                                is_reorder_commission = True
                            if new_date_start and new_date_end and new_date_start <= order_date <= new_date_end:
                                reorder_commission_amt = sale_id.user_id.reorder_commission_new_amt
                                is_reorder_commission = True
                            if is_reorder_commission:
                                order_commission_status = 1
                                sale_commission = 1
                                total_order_amt = 0
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt += product_commission
                                for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                                    product_commission = line_id.price_unit * line_id.quantity
                                    total_order_amt -= abs(product_commission)
                                manage_total_commission = (total_order_amt * reorder_commission_amt or 10) / 100
                                reference = invoice.number
                                invoice.commission_count_entries(
                                    manage_total_commission, sale_id.user_id,
                                    reference, sale_commission, order_commission_status, sale_id
                                )

                if sale_id and sale_id.user_id and sale_id.user_id.product_commission:
                    total_product_commission = 0.0
                    if sale_id.user_id.commission_category:
                        for line in sale_id.order_line.filtered(
                            lambda r: r.product_id.product_tmpl_id.categ_id.id == sale_id.user_id.commission_category.id and r.product_id.type != 'service'
                        ):
                            if sale_id.user_id.commission_category_amt:
                                product_commission = (line.price_subtotal * sale_id.user_id.commission_category_amt) / 100
                                total_product_commission += product_commission
                            if total_product_commission:
                                sale_commission = 2
                                order_commission_status = 4
                                reference = invoice.number
                                invoice.commission_count_entries(
                                    total_product_commission, sale_id.user_id,
                                    reference, sale_commission, order_commission_status, sale_id
                                )

                if sale_id and sale_id.user_id and sale_id.user_id.extra_commission:
                    dates = {
                        'date_start': sale_id.user_id.start_date,
                        'date_end': sale_id.user_id.end_date,
                    }
                    date_start, date_end = invoice._get_utc_time_range(dates)
                    order_date = fields.Datetime.context_timestamp(
                        invoice, datetime.strptime(
                            sale_id.date_order, DTF
                        )
                    )
                    oder_date = order_date.date()
                    if date_start <= oder_date <= date_end:
                        reference = invoice.number
                        total_user_sale_amount = sale_id.user_id.total_sale_amount or 0.0
                        total_order_amt = 0
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt += product_commission
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt -= abs(product_commission)
                        sale_amount = total_order_amt or 0.0
                        total_amount = sale_amount + total_user_sale_amount
                        sale_id.is_appy_commission_person = True
                        if total_amount >= sale_id.user_id.commission_amount_limit:
                            if (total_amount - sale_id.user_id.commission_amount_limit) >= sale_id.user_id.commission_amount_limit:
                                amount = total_amount
                                count = 0
                                while (amount >= sale_id.user_id.commission_amount_limit):
                                    amount = amount - sale_id.user_id.commission_amount_limit
                                    count = count + 1
                                sale_id.user_id.total_sale_amount = total_amount - (sale_id.user_id.commission_amount_limit * (count or 1))
                                total_extra_commission = (sale_id.user_id.extra_commission_amount or 500) * (count or 1)
                            else:
                                sale_id.user_id.total_sale_amount = total_amount - sale_id.user_id.commission_amount_limit
                                total_extra_commission = sale_id.user_id.extra_commission_amount or 500
                            sale_commission = 1
                            order_commission_status = 3
                            invoice.commission_count_entries(
                                total_extra_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )
                        else:
                            sale_id.user_id.total_sale_amount = invoice.amount_total

                if sale_id and sale_id.partner_id and sale_id.user_id and sale_id.user_id.tier_commission:
                    dates = {
                        'date_start': sale_id.user_id.tier_commission_new_start_date,
                        'date_end': sale_id.user_id.tier_commission_new_end_date,
                    }
                    date_start, date_end = invoice._get_utc_time_range(dates)
                    order_date = fields.Datetime.context_timestamp(
                        invoice, datetime.strptime(
                            sale_id.date_order, DTF
                        )
                    )
                    oder_date = order_date.date()
                    if date_start <= oder_date <= date_end:
                        total_order_amt = 0
                        total_product_commission = 0
                        total_invoiced = sale_id.partner_id.total_invoiced or 0.0
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type != 'service'):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt += product_commission
                        for line_id in invoice.invoice_line_ids.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                            product_commission = line_id.price_unit * line_id.quantity
                            total_order_amt -= abs(product_commission)
                        total_invoiced = total_invoiced + total_order_amt
                        if total_invoiced <= 5000:
                            product_commission = (total_order_amt * sale_id.user_id.tier_low_commission) / 100
                            total_product_commission += product_commission
                        elif total_invoiced > 5000 and total_invoiced <= 20000:
                            product_commission = (total_order_amt * sale_id.user_id.tier_medium_commission) / 100
                            total_product_commission += product_commission
                        elif total_invoiced > 20000:
                            product_commission = (total_order_amt * sale_id.user_id.tier_high_commission) / 100
                            total_product_commission += product_commission
                        if total_product_commission:
                            sale_commission = 2
                            order_commission_status = 5
                            reference = invoice.number
                            invoice.commission_count_entries(
                                total_product_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref, sale_commission, order_commission_status, sale_id):
        commission_journal = self.env['ir.values'].get_default('account.config.settings',
                                                               'commission_journal')
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_journal)
        if sale_commission == 0:
            sale_commission = '0'
        else:
            sale_commission = sale_commission and str(sale_commission) or '3'
        if order_commission_status == 0:
            order_commission_status = '0'
        else:
            order_commission_status = order_commission_status and str(order_commission_status) or '2'

        commission_vals = {
            'name': account_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': account_id.default_debit_account_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
            'sales_commission':  sale_commission,
            'order_commission_status': order_commission_status,
            'ref_invoice': journal_ref,
            'ref': sale_id.name
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
            'sales_commission':  sale_commission,
            'order_commission_status': order_commission_status,
            'ref_invoice': journal_ref,
            'ref': sale_id.name
        }
        now = datetime.now()
        vals = {
            'journal_id': account_id.id,
            'date': sale_id and sale_id.confirmation_date or now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id

    def _get_utc_time_range(self, form):
        start_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_start'], DF))
        start_date = start_date.date()
        end_date = fields.Datetime.context_timestamp(self, datetime.strptime(form['date_end'], DF))
        end_date = end_date.date()
        return start_date, end_date

    @api.multi
    def post(self):
        commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
        res = super(AccountPayment, self).post()
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        inv_id = False
        if self._context.get('active_id', False):
            inv_id = self._context.get('active_id')
        if self._context.get('invoice_id', False):
            if type(self._context.get('invoice_id')) is int:
                inv_id = self._context.get('invoice_id')
            else:
                inv_id = self._context.get('invoice_id').id
        if inv_id:
            invoice_id = invoice_obj.browse(inv_id)
            self.write({'order_pay_state': invoice_id.order_pay_state})
        if self.order_pay_state == '2' or (invoice_id and invoice_id.order_pay_state == '2'):
            if commission_type == '0':
                sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
                is_commision_order_product = False
                is_commision_order_product = sale_id.order_line.filtered(
                    lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                )
                sale_commission = 2
                if is_commision_order_product:
                    order_commission_status = 0
                else:
                    order_commission_status = 1
                for product_line in sale_id.order_line.filtered(lambda r: r.product_id.type != 'service'):
                    if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                        total_commission = (self.amount * product_line.product_id.prod_comm_per) / 100
                reference = invoice_id.number
                self.commission_count_entries(total_commission, sale_id.user_id, reference, sale_commission, order_commission_status, sale_id)

            if commission_type == '1':
                sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
                is_commision_order_product = False
                is_commision_order_product = sale_id.order_line.filtered(
                    lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                )
                sale_commission = 2
                if is_commision_order_product:
                    order_commission_status = 0
                else:
                    order_commission_status = 1
                for product_line in sale_id.order_line.filtered(lambda r: r.product_id.type != 'service'):
                    if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                        total_commission = (self.amount * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                reference = invoice_id.number
                self.commission_count_entries(total_commission, sale_id.user_id, reference, sale_commission, order_commission_status, sale_id)

            sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
            if sale_id and sale_id.user_id and sale_id.user_id.first_commission:
                if sale_id.confirmation_date:
                    order_date = fields.Datetime.context_timestamp(self, datetime.strptime(sale_id.confirmation_date, DTF))
                    order_date = order_date.date()
                    dates = {
                        'date_start': sale_id.user_id.first_commission_old_start_date,
                        'date_end': sale_id.user_id.first_commission_old_end_date,
                    }
                    old_date_start, old_date_end = sale_id._get_utc_time_range_commission(dates)
                    dates = {
                        'date_start': sale_id.user_id.first_commission_new_start_date,
                        'date_end': sale_id.user_id.first_commission_new_end_date,
                    }
                    new_date_start, new_date_end = sale_id._get_utc_time_range_commission(dates)
                    first_commission_amt = 0.0
                    is_first_commission = False
                    if old_date_start and old_date_end and old_date_start <= order_date <= old_date_end:
                        first_commission_amt = sale_id.user_id.first_commission_old_amt
                        is_first_commission = True
                    if new_date_start and new_date_end and new_date_start <= order_date <= new_date_end:
                        first_commission_amt = sale_id.user_id.first_commission_new_amt
                        is_first_commission = True
                    if is_first_commission:
                        is_commision_order_product = False
                        is_commision_order_product = sale_id.order_line.filtered(
                            lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                        )
                        if is_commision_order_product:
                            order_commission_status = 0
                            sale_commission = 1
                            total_order_amt = 0
                            for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type != 'service'):
                                product_commission = line_id.price_unit * line_id.product_uom_qty
                                total_order_amt += product_commission
                            for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                                product_commission = line_id.price_unit * line_id.product_uom_qty
                                total_order_amt -= abs(product_commission)
                            manage_total_commission = (total_order_amt * first_commission_amt or 10) / 100
                            reference = invoice_id.number
                            self.commission_count_entries(
                                manage_total_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )
            if sale_id and sale_id.user_id and sale_id.user_id.reorder_commission:
                is_commision_order_product = sale_id.order_line.filtered(
                    lambda r: r.product_id.default_code in ['FIRST_ORDER'] or r.product_id.display_name in ['[FIRST_ORDER] First order']
                )
                if not is_commision_order_product:
                    if sale_id.confirmation_date:
                        order_date = fields.Datetime.context_timestamp(self, datetime.strptime(sale_id.confirmation_date, DTF))
                        order_date = order_date.date()
                        dates = {
                            'date_start': sale_id.user_id.reorder_commission_old_start_date,
                            'date_end': sale_id.user_id.reorder_commission_old_end_date,
                        }
                        old_date_start, old_date_end = sale_id._get_utc_time_range_commission(dates)
                        dates = {
                            'date_start': sale_id.user_id.reorder_commission_new_start_date,
                            'date_end': sale_id.user_id.reorder_commission_new_end_date,
                        }
                        new_date_start, new_date_end = sale_id._get_utc_time_range_commission(dates)
                        reorder_commission_amt = 0.0
                        is_reorder_commission = False
                        if old_date_start and old_date_end and old_date_start <= order_date <= old_date_end:
                            reorder_commission_amt = sale_id.user_id.reorder_commission_old_amt
                            is_reorder_commission = True
                        if new_date_start and new_date_end and new_date_start <= order_date <= new_date_end:
                            reorder_commission_amt = sale_id.user_id.reorder_commission_new_amt
                            is_reorder_commission = True
                        if is_reorder_commission:
                            order_commission_status = 1
                            sale_commission = 1
                            total_order_amt = 0
                            for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type != 'service'):
                                product_commission = line_id.price_unit * line_id.product_uom_qty
                                total_order_amt += product_commission
                            for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                                product_commission = line_id.price_unit * line_id.product_uom_qty
                                total_order_amt -= abs(product_commission)
                            manage_total_commission = (total_order_amt * reorder_commission_amt or 10) / 100
                            reference = invoice_id.number
                            self.commission_count_entries(
                                manage_total_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )

            if sale_id and sale_id.user_id and sale_id.user_id.product_commission:
                total_product_commission = 0.0
                if sale_id.user_id.commission_category:
                    for line in sale_id.order_line.filtered(
                        lambda r: r.product_id.product_tmpl_id.categ_id.id == sale_id.user_id.commission_category.id and r.product_id.type != 'service'
                    ):
                        if sale_id.user_id.commission_category_amt:
                            product_commission = (line.price_subtotal * sale_id.user_id.commission_category_amt) / 100
                            total_product_commission += product_commission
                        if total_product_commission:
                            sale_commission = 2
                            order_commission_status = 4
                            reference = invoice_id.number
                            self.commission_count_entries(
                                total_product_commission, sale_id.user_id,
                                reference, sale_commission, order_commission_status, sale_id
                            )

            if sale_id and sale_id.user_id and sale_id.user_id.extra_commission:
                dates = {
                    'date_start': sale_id.user_id.start_date,
                    'date_end': sale_id.user_id.end_date,
                }
                date_start, date_end = self._get_utc_time_range(dates)
                order_date = fields.Datetime.context_timestamp(
                    self, datetime.strptime(
                        sale_id.date_order, DTF
                    )
                )
                oder_date = order_date.date()
                if date_start <= oder_date <= date_end:
                    reference = invoice_id.number
                    total_order_amt = 0.0
                    total_user_sale_amount = sale_id.user_id.total_sale_amount or 0.0
                    for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        product_commission = line_id.price_unit * line_id.product_uom_qty
                        total_order_amt += product_commission
                    for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                        product_commission = line_id.price_unit * line_id.product_uom_qty
                        total_order_amt -= abs(product_commission)
                    sale_amount = total_order_amt or 0.0
                    total_amount = sale_amount + total_user_sale_amount
                    sale_id.is_appy_commission_person = True
                    if total_amount >= sale_id.user_id.commission_amount_limit:
                        if (total_amount - sale_id.user_id.commission_amount_limit) >= sale_id.user_id.commission_amount_limit:
                            amount = total_amount
                            count = 0
                            while (amount >= sale_id.user_id.commission_amount_limit):
                                amount = amount - sale_id.user_id.commission_amount_limit
                                count = count + 1
                            sale_id.user_id.total_sale_amount = total_amount - (sale_id.user_id.commission_amount_limit * (count or 1))
                            total_extra_commission = (sale_id.user_id.extra_commission_amount or 500) * (count or 1)
                        else:
                            sale_id.user_id.total_sale_amount = total_amount - sale_id.user_id.commission_amount_limit
                            total_extra_commission = sale_id.user_id.extra_commission_amount or 500
                        sale_commission = 1
                        order_commission_status = 3
                        self.commission_count_entries(
                            total_extra_commission, sale_id.user_id,
                            reference, sale_commission, order_commission_status, sale_id
                        )
                    else:
                        sale_id.user_id.total_sale_amount = self.amount
            if sale_id and sale_id.partner_id and sale_id.user_id and sale_id.user_id.tier_commission:
                dates = {
                    'date_start': sale_id.user_id.tier_commission_new_start_date,
                    'date_end': sale_id.user_id.tier_commission_new_end_date,
                }
                date_start, date_end = sale_id._get_utc_time_range_commission(dates)
                order_date = fields.Datetime.context_timestamp(
                    self, datetime.strptime(
                        sale_id.date_order, DTF
                    )
                )
                oder_date = order_date.date()
                if date_start <= oder_date <= date_end:
                    total_order_amt = 0
                    total_product_commission = 0
                    total_invoiced = sale_id.partner_id.total_invoiced or 0.0
                    for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type != 'service'):
                        product_commission = line_id.price_unit * line_id.product_uom_qty
                        total_order_amt += product_commission
                    for line_id in sale_id.order_line.filtered(lambda r: r.product_id.type == 'service' and r.price_unit < 0):
                        product_commission = line_id.price_unit * line_id.product_uom_qty
                        total_order_amt -= abs(product_commission)
                    total_invoiced = total_invoiced + total_order_amt
                    if total_invoiced <= 5000:
                        product_commission = (total_order_amt * sale_id.user_id.tier_low_commission) / 100
                        total_product_commission += product_commission
                    elif total_invoiced > 5000 and total_invoiced <= 20000:
                        product_commission = (total_order_amt * sale_id.user_id.tier_medium_commission) / 100
                        total_product_commission += product_commission
                    elif total_invoiced > 20000:
                        product_commission = (total_order_amt * sale_id.user_id.tier_high_commission) / 100
                        total_product_commission += product_commission
                    if total_product_commission:
                        sale_commission = 2
                        order_commission_status = 5
                        reference = invoice_id.number
                        self.commission_count_entries(
                            total_product_commission, sale_id.user_id,
                            reference, sale_commission, order_commission_status, sale_id
                        )
        return res


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: