1# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from openerp import fields,models ,api, _
import time

class AllCommission(models.AbstractModel):
    _name = 'report.sale_commission_gt.all_commission_report'

    def get_movelines(self, commission_report_id):
        self.env.cr.execute("""
        update account_move_line aml set invoice_id = ai.id from account_invoice ai where aml.ref_invoice=ai.number
        """)
        wizard_obj = self.env['all.commission.report']
        wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        move_line_obj = self.env['account.move.line']
        if wizard_search.type == 'payable':
            move_line_search = move_line_obj.search([('credit', '>', 0.0),('paid','=',False), ('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        if wizard_search.type == 'paid':
            move_line_search = move_line_obj.search([('credit', '>', 0.0),('paid','=',True), ('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        if wizard_search.type == 'all':
            move_line_search = move_line_obj.search([('credit', '>', 0.0),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        return move_line_search


    @api.model
    def render_html(self, docids, data=None):
        wizard_obj = self.env['all.commission.report']
        wizard_search = wizard_obj.search([('id','=',docids)])
        ctx = self._context.copy()
        ctx.update({'active_ids': docids})
        self.context = ctx
        docargs = {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': self.context['active_ids'],
            'type': wizard_search.type,
            'data': {'docs': docids},
            'get_movelines': self.get_movelines,
        }
        return self.env['report'].render('sale_commission_gt.all_commission_report', docargs)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
