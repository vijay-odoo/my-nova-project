# -*- coding: utf-8 -*-

from odoo import fields, models, _, api
from datetime import datetime
from dateutil import parser
import base64
import StringIO
import pytz
import logging

try:
    from xlwt import *
except:
    raise osv.except_osv(_('Error!'), _('Please install python xlwt module'))

logger = logging.getLogger(__name__)

COLUMN_TITLES = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
]


class SaleCommissionReport(models.TransientModel):
    _inherit = 'sale.commission.report'

    xls_name = fields.Char(string='Export XLS Data', size=256, default="Export All Commission.xls")
    xls_data = fields.Binary("Download XLS File", readonly=True)
    is_xls = fields.Boolean("Xls Export")

    @api.multi
    def sale_commission_export_excel(self):
        self.export_excel()
        return {
            "name": _('Export Commission'),
            "type": "ir.actions.act_window",
            "res_model": "sale.commission.report",
            "view_type": "form",
            "view_mode": "form",
            "res_id": self.id,
            "target": 'new'
        }

    @api.multi
    def _get_user_time(self):
        tz_name = self._context.get('tz', False) or self.env.user.tz or 'UTC'
        if tz_name:
            try:
                user_tz = pytz.timezone(tz_name)
                dt = datetime.now(user_tz)
                return dt.strftime('%Y%m%d-%H%M%S')
            except Exception:
                logger.warn(
                    "Failed to convert the value of date with server timezone"
                    "to the user's timezone (%s) to UTC",
                    tz_name,
                    exc_info=True)

    @api.multi
    def _export_fields(self):
        return [
            'Date',
            'OrderID',
            'Commission Status',
            'Customer Name',
            'Customer City',
            'Customer State',
            'Invoice Amount',
            'Commission Amount',
        ]

    @api.multi
    def _get_default_empty_char(self):
        return ''

    def get_movelines(self, commission_report_id):
        self.env.cr.execute("""
        update account_move_line aml set invoice_id = ai.id from account_invoice ai where aml.ref_invoice=ai.number
        """)
        wizard_obj = self.env['sale.commission.report']
        wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        move_line_search = move_line_obj.search([
            ('credit', '>', 0.0),
            ('commission_user', '=', salesman.id),
            ('paid', '=', False),
            ('account_id.user_type_id.name', '=', 'Payable'),
            ('date', '>=', wizard_search.start_date),
            ('date', '<=', wizard_search.end_date)
        ])
        return move_line_search

    @api.multi
    def get_movelines_paid(self, commission_report_id):
        self.env.cr.execute("""
        update account_move_line aml set invoice_id = ai.id from account_invoice ai where aml.ref_invoice=ai.number
        """)
        wizard_obj = self.env['sale.commission.report']
        wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        move_line_search = move_line_obj.search([
            ('credit', '>', 0.0),
            ('commission_user', '=', salesman.id),
            ('paid', '=', True),
            ('account_id.user_type_id.name', '=', 'Payable'),
            ('date', '>=', wizard_search.start_date),
            ('date', '<=', wizard_search.end_date)
        ])
        return move_line_search

    @api.multi
    def export_excel(self):
        default_value = self._get_default_empty_char()
        font0 = Font()
        font0.name = 'Arial'
        font0.bold = True

        style0 = XFStyle()
        style0.font = font0

        style1 = XFStyle()

        currency_style = XFStyle()
        currency_style.num_format_str = u'"$" #,##0.00'

        date_style = XFStyle()
        date_style.num_format_str = 'YYYY-MM-DD'

        book = Workbook()
        ws0 = book.add_sheet('Export Commission')
        exported_fields = self._export_fields()

        for column, title in enumerate(exported_fields):
            ws0.write(0, column, title, style0)
        if self.type == 'payable':
            line_ids = self.get_movelines(self.id)
        else:
            line_ids = self.get_movelines_paid(self.id)
        for row, line_id in enumerate(line_ids, 1):
            line = []
            order_status = None
            if line_id.order_commission_status == '0':
                order_status = 'First Order'
            elif line_id.order_commission_status == '1':
                order_status = 'Reorder'
            elif line_id.order_commission_status == '3':
                order_status = 'Extra'
            elif line_id.order_commission_status == '4':
                order_status = 'Product'
            values = [
                line_id.date,
                line_id.ref,
                order_status,
                line_id.invoice_id.partner_id.name,
                line_id.invoice_id.partner_id.city,
                line_id.invoice_id.partner_id.state_id.name,
                line_id.invoice_id.amount_total,
                line_id.credit
            ]

            line.extend(values)
            for column, value in enumerate(line):
                if column == 0 and value:
                    value = parser.parse(value)
                    ws0.write(row, column, value or default_value, date_style)
                elif column in [6, 7]:
                    ws0.write(row, column, value or 0.0, currency_style)
                else:
                    ws0.write(row, column, value or default_value, style1)
            column += 1

        file_data = StringIO.StringIO()
        book.save(file_data)
        """STRING ENCODE OF DATA IN WKSHEET"""
        xls_out = base64.encodestring(file_data.getvalue())
        self.write({
            'xls_data': xls_out,
            'xls_name': self._get_user_time() + '_commission.xls',
            'is_xls': True
        })
        return True


class AllCommissionReport(models.TransientModel):
    _inherit = 'all.commission.report'

    xls_name = fields.Char(string='Export XLS Data', size=256, default="Export All Commission.xls")
    xls_data = fields.Binary("Download XLS File", readonly=True)
    is_xls = fields.Boolean("Xls Export")

    @api.multi
    def all_commission_export_excel(self):
        self.export_excel()
        return {
            "name": _('Export Commission'),
            "type": "ir.actions.act_window",
            "res_model": "all.commission.report",
            "view_type": "form",
            "view_mode": "form",
            "res_id": self.id,
            "target": 'new'
        }

    @api.multi
    def _get_user_time(self):
        tz_name = self._context.get('tz', False) or self.env.user.tz or 'UTC'
        if tz_name:
            try:
                user_tz = pytz.timezone(tz_name)
                dt = datetime.now(user_tz)
                return dt.strftime('%Y%m%d-%H%M%S')
            except Exception:
                logger.warn(
                    "Failed to convert the value of date with server timezone"
                    "to the user's timezone (%s) to UTC",
                    tz_name,
                    exc_info=True)

    @api.multi
    def _export_fields(self):
        return [
            'Date',
            'OrderID',
            'Commission Status',
            'Customer Name',
            'Customer City',
            'Customer State',
            'Invoice Amount',
            'Commission Amount',
        ]

    @api.multi
    def _get_default_empty_char(self):
        return ''

    @api.multi
    def get_movelines(self, commission_report_id):
        self.env.cr.execute("""
        update account_move_line aml set invoice_id = ai.id from account_invoice ai where aml.ref_invoice=ai.number
        """)
        wizard_obj = self.env['all.commission.report']
        wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        move_line_obj = self.env['account.move.line']
        if wizard_search.type == 'payable':
            move_line_search = move_line_obj.search([
                ('credit', '>', 0.0), ('paid', '=', False),
                ('account_id.user_type_id.name', '=', 'Payable'),
                ('date', '>=', wizard_search.start_date),
                ('date', '<=', wizard_search.end_date)
            ])
        if wizard_search.type == 'paid':
            move_line_search = move_line_obj.search([
                ('credit', '>', 0.0), ('paid', '=', True),
                ('account_id.user_type_id.name', '=', 'Payable'),
                ('date', '>=', wizard_search.start_date),
                ('date', '<=', wizard_search.end_date)
            ])
        if wizard_search.type == 'all':
            move_line_search = move_line_obj.search([
                ('credit', '>', 0.0), ('account_id.user_type_id.name', '=', 'Payable'),
                ('date', '>=', wizard_search.start_date),
                ('date', '<=', wizard_search.end_date)
            ])
        return move_line_search

    @api.multi
    def export_excel(self):
        default_value = self._get_default_empty_char()
        font0 = Font()
        font0.name = 'Arial'
        font0.bold = True

        style0 = XFStyle()
        style0.font = font0

        style1 = XFStyle()

        currency_style = XFStyle()
        currency_style.num_format_str = u'"$" #,##0.00'

        date_style = XFStyle()
        date_style.num_format_str = 'YYYY-MM-DD'

        book = Workbook()
        ws0 = book.add_sheet('Export Commission')
        exported_fields = self._export_fields()

        for column, title in enumerate(exported_fields):
            ws0.write(0, column, title, style0)

        line_ids = self.get_movelines(self.id)
        for row, line_id in enumerate(line_ids, 1):
            line = []
            order_status = None
            if line_id.order_commission_status == '0':
                order_status = 'First Order'
            elif line_id.order_commission_status == '1':
                order_status = 'Reorder'
            elif line_id.order_commission_status == '3':
                order_status = 'Extra'
            elif line_id.order_commission_status == '4':
                order_status = 'Product'
            values = [
                line_id.date,
                line_id.ref,
                order_status,
                line_id.invoice_id.partner_id.name,
                line_id.invoice_id.partner_id.city,
                line_id.invoice_id.partner_id.state_id.name,
                line_id.invoice_id.amount_total,
                line_id.credit
            ]

            line.extend(values)
            for column, value in enumerate(line):
                if column == 0 and value:
                    value = parser.parse(value)
                    ws0.write(row, column, value or default_value, date_style)
                elif column in [6, 7]:
                    ws0.write(row, column, value or 0.0, currency_style)
                else:
                    ws0.write(row, column, value or default_value, style1)
            column += 1

        file_data = StringIO.StringIO()
        book.save(file_data)
        """STRING ENCODE OF DATA IN WKSHEET"""
        xls_out = base64.encodestring(file_data.getvalue())
        self.write({
            'xls_data': xls_out,
            'xls_name': self._get_user_time() + '_commission.xls',
            'is_xls': True
        })
        return True
