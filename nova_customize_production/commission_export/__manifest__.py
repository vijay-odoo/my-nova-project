{
    'name': 'Commission Export in Excel',
    'version': '0.2018.01.05.1',
    'author': 'Jaimik',
    'category': 'Sale',
    'description': """
        Export Commission entries to excel file
    """,
    'images': [],
    'depends': [
        'sale_commission_gt',
        'account',
        'sale',
    ],
    'data': [
        #wizard
        'wizard/views/wiz_commission_expo_view.xml',

    ],
    'installable': True,
    'auto_install': False,
}
