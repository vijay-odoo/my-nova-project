# -*- coding: utf-8 -*-
import odoo
from odoo import models, http, _
from odoo.http import request
from odoo.addons.web.controllers.main import Home, ensure_db


class Website(models.Model):
    _inherit = 'website'

    def get_website_categories(self):
        category = self.env['product.public.category'].search([], limit=10, order='sequence')
        return category


# class NovaWebsite(Home):

    # @http.route('/web/login', type='http', auth="none")
    # def web_login(self, redirect=None, **kw):
    #     pass
        # ensure_db()
        # request.params['login_success'] = False
        # if request.httprequest.method == 'GET' and redirect and request.session.uid:
        #     return http.redirect_with_hash(redirect)

        # if not request.uid:
        #     request.uid = odoo.SUPERUSER_ID

        # values = request.params.copy()
        # try:
        #     values['databases'] = http.db_list()
        # except odoo.exceptions.AccessDenied:
        #     values['databases'] = None

        # if request.httprequest.method == 'POST':
        #     old_uid = request.uid
        #     if request.params.get('login_1', False) == 'login':
        #         uid = request.session.authenticate(request.session.db, request.params['login1'], request.params['password1'])
        #     else:
        #         uid = request.session.authenticate(request.session.db, request.params['login'], request.params['password'])
        #     if uid is not False:
        #         request.params['login_success'] = True
        #         if not redirect:
        #             redirect = '/web'
        #         return http.redirect_with_hash(redirect)
        #     request.uid = old_uid
        #     values['error'] = _("Wrong login/password")
        # return request.render('web.login', values)
