{
    'name': 'Nova Themes',
    'version': '0.1',
    'author': 'Jaimik Panchal, Umang Gajjar',
    'category': 'website',
    'description': """
        Website
    """,
    'images': [],
    'depends': ['website', 'website_sale', 'auth_signup', 'website_support'],
    'data': [
        'data/website_data.xml',
        'views/assets.xml',
        'views/snippet.xml',
        'views/website_pages.xml',
        'views/aboutus_template.xml',
        # 'views/password_recovery_template.xml',
    ],
    'installable': True,
    'auto_install': False,
}
