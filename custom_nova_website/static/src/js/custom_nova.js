$(document).ready(function(){

    odoo.define('custom_nova_website.nova', function (require) {
    "use strict";

        var ajax = require('web.ajax');
        $(".shop-view a").click(function(){
            var click_id = $(this).attr("id");
            var open_id = click_id.split("_");
            $(".product-main").hide();
            $(".product-" +open_id[0] + "Main").show();
            $(".shop-view a").removeClass("active");
            $(this).addClass("active");
        });
    });

});

jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
$('#profileForm').validate({
    rules : {
        account_pass : {
            minlength : 5
        },
        account_confirm_pass : {
            minlength : 5,
            equalTo : "#account_pass"
    }
}
});
