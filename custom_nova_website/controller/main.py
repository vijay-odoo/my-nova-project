import werkzeug
import logging

from odoo import http, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_portal.controllers.main import website_account
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.addons.auth_signup.models.res_users import SignupError

_logger = logging.getLogger(__name__)

WebsiteSale.PPG = 12  # Products Per Page
WebsiteSale.PPR = 3


class NovaWebsiteAccount(website_account):

    MANDATORY_FIELDS = ["account_pass", "account_confirm_pass"]

    @http.route(['/my/profile'], auth='user', website=True)
    def myprofile(self, redirect=None, **post):
        partner = request.env.user.partner_id
        values = {
            'error': {},
            'error_message': []
        }

        if post:
            error, error_message = self.profile_validate(post)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)
            if not error:
                pass

        values.update({
            'partner': partner,
            'redirect': redirect,
        })

        return request.render("custom_nova_website.nova_profile", values)

    def profile_validate(self, data):
        error = dict()
        error_message = []

        # Validation
        for field_name in self.MANDATORY_FIELDS:
            if not data.get(field_name):
                error[field_name] = 'missing'

        # error message for empty required fields
        if [err for err in error.values() if err == 'missing']:
            error_message.append('Some required fields are empty.')

        unknown = [k for k in data.iterkeys() if k not in self.MANDATORY_FIELDS]
        if unknown:
            error['common'] = 'Unknown field'
            error_message.append("Unknown field '%s'" % ','.join(unknown))

        return error, error_message


class NovaAuth(AuthSignupHome):

    @http.route('/web/signup', type='http', auth='public', website=True)
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'signuperror' not in qcontext and request.httprequest.method == 'POST':
            try:
                self.do_signup(qcontext)
                return super(NovaAuth, self).web_login(*args, **kw)
            except (SignupError, AssertionError), e:
                if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                    qcontext["signuperror"] = _("Another user is already registered using this email address.")
                else:
                    _logger.error(e.message)
                    qcontext['signuperror'] = _("Could not create a new account.")
        return request.render('web.login', qcontext)
