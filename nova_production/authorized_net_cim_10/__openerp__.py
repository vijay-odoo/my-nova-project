# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Authorized.net',
    'version': '1.1',
    'author': 'OpenERP SA',
    'category': 'Sales Management',
    'depends': ['base','account_voucher','account', 'sale'],
    'demo': [],
    'description': """
   For management of payment
    """,
    'data': [
    'wizard/refund_wizard_view.xml',
    'views/authorized_data.xml',
    'views/authorized_view.xml',
    'views/res_partner_view.xml',
    'wizard/payment_wizard.xml',
    'views/sale_view.xml',
    'views/invoice_view.xml',
    'views/voucher_payment_receipt_view.xml'
    ],
    
    'installable': True,
    'auto_install': False,
   }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
