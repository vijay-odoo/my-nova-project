# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import api, fields, models, _
class refund_wizard(models.TransientModel):
    _name = 'refund.wizard'
    
    @api.model
    def default_get(self, fields):
        res = super(refund_wizard, self).default_get(fields)
        if self._context.get('active_id'):
            trans_obj = self.env['sale.transaction'].browse(self._context.get('active_id'))
            res.update({'amount': trans_obj.tran_amount})
        return res
   
    amount = fields.Float(string='Refund amount')
    
    def get_refund(self):
        ref_obj = self[0]
        ctx = self._context.copy()
        if self._context.get('active_id'):
            trans_obj_pool = self.env['sale.transaction']
            trans_obj = trans_obj_pool.browse(self._context.get('active_id'))
            if ref_obj.amount != trans_obj.tran_amount:
                ctx.update({'amt': ref_obj.amount})
            trans_obj.refund_get()
        view_ref = self.env['ir.model.data'].get_object_reference('sale', 'view_order_form')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': 'Sale Order',
            'res_model': 'sale.order',
            'view_type': 'form',
            'view_mode': 'form',
            'res_id': trans_obj.order_id.id,
            'view_id': view_id,
            'target': 'current',
            'context': context,
            'nodestroy': True
       }
refund_wizard()



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
