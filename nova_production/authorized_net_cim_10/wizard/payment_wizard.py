# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, api, fields,_
from odoo.exceptions import UserError , ValidationError
import urllib2
import logging
import datetime
import os , sys, stat
from xml.dom.minidom import parseString
from datetime import date, timedelta
from odoo.addons.authorized_net_cim_10.model.authorize_configuration import authorize_configuration
_logger = logging.getLogger(__name__)


class paymemt_wizard(models.TransientModel):
    _name = 'payment.wizard'


    card_id = fields.Many2one('payment.info','Select Credit Card')
    partner_id = fields.Many2one('res.partner','Customer Name')
    is_added =fields.Boolean('IS Boolean')

    @api.model
    def default_get(self, fields):
        res = super(paymemt_wizard, self).default_get(fields)
        print "==============self._context.get('active_id')=============",self._context
        if self._context.get('active_model')=='sale.order':
            record_id = self.env['sale.order'].browse(self._context.get('active_id'))
        else:
            record_id = self.env['account.invoice'].browse(self._context.get('active_id'))
        res.update({'partner_id': record_id.partner_id.id})
        return res

    @api.multi
    def send_transaction(self):
        if not self.card_id:
            raise UserError(
                _("Please Select the Credit Card to pay!"))
        acc_inv_obj = self.env['account.invoice']
        payment_obj = self.env['account.payment']
        journal_obj = self.env['account.journal']
        journal_id = journal_obj.search([('name', '=', 'Bank')])
        param = self.env['authorized.configuration']
        login = param.search([])[0].authorized_api_login_key
        key = param.search([])[0].authorize_api_transaction_key
        mode = param.search([])[0].authorize_mode
        path = param.search([])[0].authorize_path
        ip = param.search([])[0].authorize_ip_address

        con = authorize_configuration()
        con.connect(login, key, mode)
        if self._context.get('active_model') == 'sale.order':
            sale_id = self.env['sale.order'].browse(self._context.get('active_id'))
            invoice_id = acc_inv_obj.search([('origin','=',sale_id.name)])
        else:
            invoice_id = self.env['account.invoice'].browse(self._context.get('active_id'))

        if not invoice_id:
            sale_id.action_invoice_create()
            invoice_id = acc_inv_obj.search([('origin', '=', sale_id.name)])
            # raise UserError(_("Please create Invoice to Authorized it."))
        if invoice_id:
            if invoice_id.state == 'draft':
                invoice_id.action_invoice_open()
                invoice_id._cr.commit()
            if invoice_id.state == 'open':
                payment = self.env['account.payment'].create({
                    'invoice_ids': [(6, 0, [invoice_id.id])],
                    'amount': invoice_id.amount_total,
                    'payment_date': fields.Date.context_today(self),
                    'communication': invoice_id.name,
                    'partner_id': invoice_id.partner_id.id,
                    'partner_type': invoice_id.type in ('out_invoice', 'out_refund') and 'customer' or 'supplier',
                    'journal_id': journal_id.id,
                    'payment_type': 'inbound',
                    'payment_method_id': 1,
                })
                if self.card_id.payment_id:
                    context = {
                        'invoice_id': invoice_id
                    }

                    # if invoice_id.partner_id.profile_id and self.card_id.payment_id and invoice_id.partner_id.address_id:
                    if invoice_id.partner_id.profile_id and self.card_id.payment_id :
                            result = con.send_transaction(payment, invoice_id,self.card_id.payment_id)
                            print'result',result

                            is_authentication_successful = False
                            is_authentication_failed = False
                            if result.messages[0].message.text == 'Successful.':
                                status = '1'
                                s_id = self.env['sale.order'].search([('name', '=', invoice_id.origin)])
                                if s_id:
                                    s_id.write({
                                        'is_authentication_successful': is_authentication_successful,
                                        'transaction_ids': [(0, 0, {
                                        'cc_number': result.transaction_response['account_number'],
                                        'payment_date': datetime.datetime.now(),
                                        'reason_text': result.transaction_response.messages[0].message.description,
                                        'status': status, 'partner_id': invoice_id.partner_id.id,
                                        'transaction_id': result.transaction_response.trans_id,
                                        'tran_amount': payment.amount})]})
                                    invoice_id.write(
                                        {'auth_payment_ids': [(0, 0, {'payment_date': datetime.datetime.now(),
                                                                      'reason_text':
                                                                          result.transaction_response.messages[
                                                                              0].message.description,
                                                                      'status': status,
                                                                      'partner_id': invoice_id.partner_id.id,
                                                                      'transaction_id': result.transaction_response.trans_id,
                                                                      'tran_amount': payment.amount})]})
                                    self._cr.commit()
                                    payment.with_context(context).post()
                                    return True

                            else:
                                status = '2'
                                raise UserError(
                                    _("Transaction has been declined . Please try other Credit Card!"))
                    else:
                        raise ValidationError('Customer has no Authorized Profile Id,Payment_id,Address Id')
                    return True

    @api.multi
    def open_credit_card_page(self):
        para = self.env["ir.config_parameter"]
        ip_address = para.get_param("web.base.url")
        tran_obj = self.env['sale.transaction']
        partner_obj = self.env['res.partner']
        sale_obj = self.env['sale.order']
        payment_info_obj = self.env['payment.info']
        param = self.env['authorized.configuration']
        login = param.search([])[0].authorized_api_login_key
        key = param.search([])[0].authorize_api_transaction_key
        mode = param.search([])[0].authorize_mode
        path = param.search([])[0].authorize_path
        ip = param.search([])[0].authorize_ip_address
        con = authorize_configuration()
        con.connect(login, key, mode)
        if self._context.get('active_model') == 'sale.order':
            sale_id = sale_obj.browse(self._context.get('active_id'))
            invoice_id = self.env['account.invoice'].search([('origin', '=', sale_id.name)])
        else:
            invoice_id = self.env['account.invoice'].browse(self._context.get('active_id'))
        if not invoice_id:
            sale_id.action_invoice_create()
            invoice_id = self.env['account.invoice'].search([('origin', '=', sale_id.name)])
            # raise UserError(_("Please create Invoice to Authorized it."))
        if invoice_id:
            if invoice_id.state == 'draft':
                invoice_id.action_invoice_open()
                invoice_id._cr.commit()

        if not invoice_id.partner_id.profile_id:
            if invoice_id.partner_id.email and invoice_id.partner_id.name:
                profile_id = con.create_customer(invoice_id.partner_id)
                invoice_id.partner_id.write({'profile_id': profile_id})
                self._cr.commit()
            else:
                raise ValidationError('Customer Must have email,name')
        # if not invoice_id.partner_id.address_id:
        #     if not invoice_id.origin:
        #         if invoice_id.partner_id.name and invoice_id.partner_id.street and invoice_id.partner_id.city and invoice_id.partner_id.state_id.code:
        #             addr_id = con.create_address(invoice_id.partner_id)
        #             invoice_id.partner_id.write({'address_id': addr_id})
        #             self._cr.commit()
        #         else:
        #             raise ValidationError('invoice partner Must have name,street,city,state')
        #     else:
        #         if not invoice_id.partner_shipping_id.address_id:
        #             if invoice_id.partner_shipping_id.name and invoice_id.partner_shipping_id.street and invoice_id.partner_shipping_id.city and invoice_id.partner_shipping_id.state_id.code:
        #                 addr_id = con.create_address(invoice_id.partner_shipping_id)
        #                 invoice_id.partner_shipping_id.write({'address_id': addr_id})
        #                 self._cr.commit()
        #             else:
        #                 raise ValidationError('shipping partner Must have name,street,city,state')

        xml = """<?xml version="1.0" encoding="utf-8"?>
                            <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                                    <merchantAuthentication>
                                            <name>""" + login + """</name>
                                            <transactionKey>""" + key + """</transactionKey>
                                    </merchantAuthentication>
                                    <customerProfileId>""" + invoice_id.partner_id.profile_id + """</customerProfileId>
                                    <hostedProfileSettings>
                                </hostedProfileSettings>
                        </getHostedProfilePageRequest>"""
        if mode == 'test':
            request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api",
                                      xml)
        else:
            request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
        request.add_header('Content-Type', 'text/xml')
        response = urllib2.urlopen(request).read()
        response = parseString(response)
        token = ''
        for node in response.getElementsByTagName("getHostedProfilePageResponse"):
            for cnode in node.childNodes:
                if cnode.nodeName == 'token':
                    token = cnode.childNodes[0].data
                    print "==================token=================", token
        if mode == 'test':
            action_url = "https://test.authorize.net/profile/manage"
        else:
            action_url = "https://secure.authorize.net/profile/manage"
        html_str = '''<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
                                <html>
                                <head>
                                  <meta content="text/html; charset=ISO-8859-1"
                                 http-equiv="content-type">
                                  <title>Hello</title>
                                </head>
                                <body>
                                <form method="post" action=''' + action_url + ''' id="formAuthorizeNetPage" style="display:none;">',
                                <input type="hidden" name="Token" value="''' + token + '''"/>
                                </form>
                                 <button type="button" onclick= "document.getElementById(
    'formAuthorizeNetPage').submit();">Click to open Payment Form</button>
                                </body>
                                </html>'''
        file = path + invoice_id.partner_id.profile_id + ".html"
        print "====file=======>", file
        if not os.path.exists(path):
            os.makedirs(path)
        if os.path.exists(file):
            os.remove(file)
            #                    os.chmod(file, 0777)
        Html_file = open(file, "w")
        Html_file.write(html_str.encode('UTF-8'))
        Html_file.close()

        url = ip_address + '/authorized_net_cim_10/static/src/' + invoice_id.partner_id.profile_id + ".html"
        print "==============url==================", url
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'current'
        }

    @api.multi
    def add_new_cc_inodoo(self):
        payment_info_obj = self.env['payment.info']
        # if self._context.get('active_model') == 'sale.order':
        #     record_id = self.env['sale.order'].browse(self._context.get('active_id'))
        # else:
        #     record_id = self.env['account.invoice'].browse(self._context.get('active_id'))
        param = self.env['authorized.configuration']
        login = param.search([])[0].authorized_api_login_key
        key = param.search([])[0].authorize_api_transaction_key
        mode = param.search([])[0].authorize_mode
        xml = """
            <?xml version="1.0" encoding="utf-8"?>
                <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                  <merchantAuthentication>
                    <name>""" + login + """</name>
                                            <transactionKey>""" + key + """</transactionKey>
                  </merchantAuthentication>
                 <customerProfileId>""" + self.partner_id.profile_id + """</customerProfileId>
                </getCustomerProfileRequest>
                """
        print'xml', xml
        if mode == 'test':
            request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml)
        else:
            request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
        request.add_header('Content-Type', 'text/xml')
        response = parseString(urllib2.urlopen(request).read())
        flag =0
        for node in response.getElementsByTagName("getCustomerProfileResponse"):
            for cnode in node.childNodes:
                if cnode.nodeName == 'profile':
                    for ccnode in cnode.childNodes:
                        if ccnode.nodeName == 'paymentProfiles':
                            for cccnode in ccnode.childNodes:
                                if cccnode.nodeName == 'customerPaymentProfileId':
                                    payment = cccnode.childNodes[0].data
                                    for cc in self.partner_id.payment_id:
                                        if cc.payment_id == payment:
                                            flag+=1
                                            break
                                if flag !=0:
                                    break
                                else:
                                    if cccnode.nodeName == 'payment':
                                        for payment_node in cccnode.childNodes:
                                            if payment_node.nodeName == 'creditCard':
                                                for cr in payment_node.childNodes:
                                                    if cr.nodeName == 'cardNumber':
                                                        card_number = cr.childNodes[0].data
                                                    # if cr.nodeName == 'expirationDate':
                                                    #     ex_date = cr.childNodes[1].data

                                            if payment:
                                                vals = {
                                                    'payment_id': payment,
                                                    'partner_id':self.partner_id.id,
                                                    'name':card_number

                                                }
                                                payment_info_obj.create(vals)
                                                self._cr.commit()
        return True
# paymemt_wizard()



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
