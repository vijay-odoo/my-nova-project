# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import netsvc
from odoo import models, api, fields, _
from odoo.exceptions import UserError , ValidationError
class authorized_configuration (models.Model):
    _name = 'authorized.configuration'
    
    company_id =  fields.Many2one('res.company', string='Company',required=True)
    authorized_api_login_key =  fields.Char(string='Login', required=True , size=200)
    authorize_api_transaction_key =  fields.Char(string='Transaction', required=True , size=200)
    authorize_mode =  fields.Char(string='Mode', required=True , size=200)
    authorize_ip_address =  fields.Char(string='IP', required=True , size=200)
    authorize_path =  fields.Char(string='Path', required=True , size=200)
    
    def create_all_authorize_id(self,inv_id):
        acc_inv_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        partner_cour_obj = self.env['partner.courses']
        partner_obj = self.env['res.partner']
        invoice = acc_inv_obj.browse(inv_id)
        if invoice.partner_id.courses_ids:
            for courses_id in invoice.partner_id.courses_ids:
                for invoice_courses_id in invoice.invoice_line:
                    if courses_id.product_id.id == invoice_courses_id.product_id.id:
                        partner_courses_id = courses_id
                        break

        if not partner_courses_id.profile_id:
            if invoice.partner_id.email and invoice.partner_id.name:
                profile_id = con.create_customer(invoice.partner_id)
                partner_courses_id.write({'profile_id': profile_id})
                self._cr.commit()
            else:
                raise ValidationError('Customer Must have email,name')

        invoice = acc_inv_obj.browse(inv_id)

        # if not partner_courses_id.address_id:
        #     if not invoice.origin:
        #         if invoice.partner_id.name and invoice.partner_id.street and invoice.partner_id.city and invoice.partner_id.state_id.code:
        #             addr_id = con.create_address(invoice.partner_id)
        #             partner_courses_id.write({'address_id': addr_id})
        #             self._cr.commit()
        #         else:
        #             raise ValidationError('Invoice partner Must have name,street,city,state')
        #     else:
        #         s_id = sale_obj.search([('name','=',invoice.origin)])
        #         s_obj = s_id[0]
        #         if not partner_courses_id.address_id:
        #             if s_obj.partner_shipping_id.name and s_obj.partner_shipping_id.street and s_obj.partner_shipping_id.city and s_obj.partner_shipping_id.state_id.code:
        #                 addr_id = con.create_address(s_obj.partner_shipping_id,partner_courses_id)
        #                 partner_courses_id.write({'address_id': addr_id})
        #                 self._cr.commit()
        #             else:
        #                 raise ValidationError('Shipping partner Must have name,street,city,state')
        invoice = acc_inv_obj.browse(inv_id)

        partner_courses_data= partner_cour_obj.browse(partner_courses_id.id)
        if not partner_courses_data.payment_id:
            xml1 = """<?xml version="1.0" encoding="utf-8"?>
                <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                <merchantAuthentication>
                        <name>"""+ login +"""</name>
                        <transactionKey>"""+ key +"""</transactionKey>
                </merchantAuthentication>
                <customerProfileId>""" + partner_courses_data.profile_id + """</customerProfileId>
                    </getCustomerProfileRequest>
                """
            print'xml1',xml1
            if mode == 'test':
                request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml1)
            else:
                request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml1)
            request.add_header('Content-Type', 'text/xml')
            response = parseString(urllib2.urlopen(request).read())
            for node in response.getElementsByTagName("getCustomerProfileResponse"):
                for cnode in node.childNodes:
                    if cnode.nodeName =='profile':
                        for ccnode in cnode.childNodes:
                            if ccnode.nodeName =='paymentProfiles':
                                for cccnode in ccnode.childNodes:
                                    if cccnode.nodeName =='customerPaymentProfileId':
                                        payment = cccnode.childNodes[0].data
                                        if payment:
                                            partner_courses_data.write({'payment_id': payment})
                                            cr.commit()
            invoice = acc_inv_obj.browse(inv_id)
            partner_courses_data= partner_cour_obj.browse(partner_courses_id.id)
            if not partner_courses_data.payment_id:
                res_obj = self[0]
                xml ="""<?xml version="1.0" encoding="utf-8"?>
                            <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                                    <merchantAuthentication>
                                            <name>"""+ login +"""</name>
                                            <transactionKey>"""+key +"""</transactionKey>
                                    </merchantAuthentication>
                                    <customerProfileId>""" + partner_courses_data.profile_id + """</customerProfileId>
                                    <hostedProfileSettings>
                                </hostedProfileSettings>
                        </getHostedProfilePageRequest>"""
                if mode == 'test':
                    request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml)
                else:
                    request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
                request.add_header('Content-Type', 'text/xml')
                response =urllib2.urlopen(request).read()
                response = parseString(response)
                token = ''
                for node in response.getElementsByTagName("getHostedProfilePageResponse"):
                    for cnode in node.childNodes:
                        if cnode.nodeName =='token': 
                            token = cnode.childNodes[0].data
                if mode == 'test':
                    action_url = "https://test.authorize.net/profile/manage"
                else:
                    action_url = "https://secure.authorize.net/profile/manage"
                html_str = '''<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
                                <html>
                                <head>
                                  <meta content="text/html; charset=ISO-8859-1"
                                 http-equiv="content-type">
                                  <title>Hello</title>
                                </head>
                                <body>
                                <form method="post" action='''+action_url +''' id="formAuthorizeNetPage" style="display:none;">',
                                <input type="hidden" name="Token" value="'''+token +'''"/>
                                </form>
                                 <button type="button" onclick= "document.getElementById(
    'formAuthorizeNetPage').submit();">Click to open Payment Form</button>
                                </body>
                                </html>'''
                file = path + partner_courses_data.profile_id+".html" 
                if not os.path.exists (path):
                    os.makedirs (path)
                if os.path.exists(file):
                    os.remove(file)
                Html_file= open(file,"w")
                Html_file.write(html_str.encode('UTF-8'))
                Html_file.close()
#                url = "http://"+ip+"/"+ partner_courses_data.profile_id+".html"  
                url = "http://localhost/authorize" + partner_courses_data.profile_id+".html"
                return {
                        'type': 'ir.actions.act_url',
                        'url': url,
                        'target': 'current'
                }

authorized_configuration()