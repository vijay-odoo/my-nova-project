# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, api, fields,_

class account_invoice(models.Model):
    _inherit = "account.invoice"  
    
    @api.multi
    @api.returns('self')
    def refund(self, date_invoice=None, date=None, description=None, journal_id=None):
        new_ids = []
        for invoice in self:
            invoice = self._prepare_refund(invoice, date=date, description=description, journal_id=journal_id)
            if self._context.get('amount'):
                inv_dict = (invoice['invoice_line'][0])[2]
                inv_dict['name'] = 'Refund'
                inv_dict['price_unit'] = self._context.get('amount')
                inv_dict['price_subtotal'] = self._context.get('amount')
                inv_dict['quantity'] = 1
                inv_dict['product_id'] = False
                invoice['invoice_line'] = [(0,0,inv_dict)]
            new_ids = [self.create(invoice)]
        return new_ids
    
account_invoice()
    