# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import urllib2
from odoo import models, api, fields,_
from odoo.exceptions import UserError , ValidationError
import datetime
import logging
import requests
import os , sys, stat
from xml.dom.minidom import parseString
from authorize_configuration import authorize_configuration
_logger = logging.getLogger(__name__)


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.model_cr
    def init(self):
        cr = self._cr
        cr.execute("""
            update sale_order set is_authentication_successful= CASE WHEN spso.status='1' THEN True
            ELSE False
            END,
            is_authentication_failed=
            CASE WHEN spso.status='2' THEN True
            ELSE False
            END
            from (
            select st.status, st.id, st.order_id from sale_transaction as st
            inner join sale_order as s on s.id = st.order_id
            ) as spso where sale_order.id = spso.order_id
        """)

    is_paid = fields.Boolean('Is Paid')
    is_authentication_successful = fields.Boolean('Authentication successful')
    is_authentication_failed = fields.Boolean('Authentication Decline')
#     @api.multi
#     def _get_state(self):
#         for sale in self:
#             bal = sale.invoice_ids
#             if bal:
#                 tot = 0
# #                for pay in bal[0].payment_ids:
# #                    tot = tot + pay.credit
#                 if tot == bal[0].amount_total:
#                     sale.id = 0.0
#
    @api.multi
    def _get_bal(self):
        for sale in self:
            bal = sale.invoice_ids
            if bal:
                sale.balance = sale.invoice_ids[0].residual

    # bal = fields.Float(compute=_get_state, string='Balance')
    balance = fields.Float(compute=_get_bal,  string='Balance')
    transaction_ids = fields.One2many('sale.transaction', 'order_id', string='Transaction', readonly=True)

    @api.multi
    def send_transaction(self):
        acc_inv_obj = self.env['account.invoice']
        payment_obj = self.env['account.payment']
        journal_obj = self.env['account.journal']
        journal_id = journal_obj.search([('name','=','Bank')])
        context = self._context.copy()
        invoice_id = self.invoice_ids
        print "=============inv_id===================",invoice_id,self
        if not self.invoice_ids:
            self.action_invoice_create()
            # raise UserError(_("Please create Invoice to Authorized it."))
        if self.invoice_ids:
            invoice_id = self.invoice_ids
            if invoice_id.state =='draft':
                invoice_id.action_invoice_open()

                self._cr.commit()
            if invoice_id.state =='open':
                payment = self.env['account.payment'].create({
                    'invoice_ids': [(6, 0, [invoice_id.id])],
                    'amount': invoice_id.amount_total,
                    'payment_date': fields.Date.context_today(self),
                    'communication': invoice_id.name,
                    'partner_id': invoice_id.partner_id.id,
                    'partner_type': invoice_id.type in ('out_invoice', 'out_refund') and 'customer' or 'supplier',
                    'journal_id': journal_id.id,
                    'payment_type': 'inbound',
                    'payment_method_id': 1,
                    })
                if self.partner_id.payment_id:
                   context = {
                       'invoice_id':payment.invoice_ids.id
                   }
                   payment.with_context(context).post()

                   return True
                else:
                    para = self.env["ir.config_parameter"]
                    ip_address = para.get_param("web.base.url")
                    tran_obj = self.env['sale.transaction']
                    voucher = payment
                    if voucher.create_tran:
                        invoice = invoice_id
                        partner_obj = self.env['res.partner']
                        sale_obj = self.env['sale.order']
                        # param = self.env['ir.config_parameter']
                        param = self.env['authorized.configuration']
                        login = param.search([])[0].authorized_api_login_key
                        key = param.search([])[0].authorize_api_transaction_key
                        mode = param.search([])[0].authorize_mode
                        path = param.search([])[0].authorize_path
                        ip = param.search([])[0].authorize_ip_address
                        con = authorize_configuration()
                        con.connect(login, key, mode)
                        print "=======invoice.partner_id.profile_id===>", invoice.partner_id.profile_id
                        if not invoice.partner_id.profile_id:
                            print "=====invoice.partner_id.email====>", invoice.partner_id
                            print "=====invoice.partner_id.email====>", invoice.partner_id.email
                            print "=====invoice.partner_id.name====>", invoice.partner_id.name
                            if invoice.partner_id.email and invoice.partner_id.name:
                                profile_id = con.create_customer(invoice.partner_id)
                                invoice.partner_id.write({'profile_id': profile_id})
                                self._cr.commit()
                            else:
                                raise ValidationError('Customer Must have email,name')
                        if not invoice.partner_id.address_id:
                            if not invoice.origin:
                                if invoice.partner_id.name and invoice.partner_id.street and invoice.partner_id.city and invoice.partner_id.state_id.code:
                                    addr_id = con.create_address(invoice.partner_id)
                                    invoice.partner_id.write({'address_id': addr_id})
                                    self._cr.commit()
                                else:
                                    raise ValidationError('invoice partner Must have name,street,city,state')
                            else:
                                if not self.partner_shipping_id.address_id:
                                    if self.partner_shipping_id.name and self.partner_shipping_id.street and self.partner_shipping_id.city and self.partner_shipping_id.state_id.code:
                                        addr_id = con.create_address(self.partner_shipping_id)
                                        self.partner_shipping_id.write({'address_id': addr_id})
                                        self._cr.commit()
                                    else:
                                        raise ValidationError('shipping partner Must have name,street,city,state')
                        if not invoice.partner_id.payment_id:
                            xml1 = """<?xml version="1.0" encoding="utf-8"?>
                                    <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                                    <merchantAuthentication>
                                            <name>""" + login + """</name>
                                            <transactionKey>""" + key + """</transactionKey>
                                    </merchantAuthentication>
                                    <customerProfileId>""" + invoice.partner_id.profile_id + """</customerProfileId>
                                        </getCustomerProfileRequest>
                                    """
                            print'xml1', xml1
                            if mode == 'test':
                                request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml1)
                            else:
                                request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml1)
                            request.add_header('Content-Type', 'text/xml')
                            response = parseString(urllib2.urlopen(request).read())
                            for node in response.getElementsByTagName("getCustomerProfileResponse"):
                                for cnode in node.childNodes:
                                    if cnode.nodeName == 'profile':
                                        for ccnode in cnode.childNodes:
                                            if ccnode.nodeName == 'paymentProfiles':
                                                for cccnode in ccnode.childNodes:
                                                    if cccnode.nodeName == 'customerPaymentProfileId':
                                                        payment = cccnode.childNodes[0].data
                                                        if payment:
                                                            invoice.partner_id.write({'payment_id': payment})
                                                            self._cr.commit()
                            if not invoice.partner_id.payment_id:
                                res_obj = payment
                                xml = """<?xml version="1.0" encoding="utf-8"?>
                                                <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                                                        <merchantAuthentication>
                                                                <name>""" + login + """</name>
                                                                <transactionKey>""" + key + """</transactionKey>
                                                        </merchantAuthentication>
                                                        <customerProfileId>""" + invoice.partner_id.profile_id + """</customerProfileId>
                                                        <hostedProfileSettings>
                                                    </hostedProfileSettings>
                                            </getHostedProfilePageRequest>"""
                                if mode == 'test':
                                    request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml)
                                else:
                                    request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
                                request.add_header('Content-Type', 'text/xml')
                                response = urllib2.urlopen(request).read()
                                response = parseString(response)
                                token = ''
                                for node in response.getElementsByTagName("getHostedProfilePageResponse"):
                                    for cnode in node.childNodes:
                                        if cnode.nodeName == 'token':
                                            token = cnode.childNodes[0].data
                                            print "==================token=================",token
                                if mode == 'test':
                                    action_url = "https://test.authorize.net/profile/manage"
                                else:
                                    action_url = "https://secure.authorize.net/profile/manage"
                                # r = requests.post(action_url, data={'Token': token})
                                # print "==========url0================",r.url
                                # return {
                                #                 'type': 'ir.actions.act_url',
                                #                 'url': r.url,
                                #                 'target': 'current'
                                #             }
                                html_str = '''<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
                                                    <html>
                                                    <head>
                                                      <meta content="text/html; charset=ISO-8859-1"
                                                     http-equiv="content-type">
                                                      <title>Hello</title>
                                                    </head>
                                                    <body>
                                                    <form method="post" action=''' + action_url + ''' id="formAuthorizeNetPage" style="display:none;">',
                                                    <input type="hidden" name="Token" value="''' + token + '''"/>
                                                    </form>
                                                     <button type="button" onclick= "document.getElementById(
                        'formAuthorizeNetPage').submit();">Click to open Payment Form</button>
                                                    </body>
                                                    </html>'''
                                file = path + invoice.partner_id.profile_id + ".html"
                                print "====file=======>", file
                                if not os.path.exists(path):
                                    os.makedirs(path)
                                if os.path.exists(file):
                                    os.remove(file)
                                    #                    os.chmod(file, 0777)
                                Html_file = open(file, "w")
                                Html_file.write(html_str.encode('UTF-8'))
                                Html_file.close()

                                url = ip_address + '/authorized_net_cim_10/static/src/' + invoice.partner_id.profile_id + ".html"
                                print "==============url==================", url
                                return {
                                    'type': 'ir.actions.act_url',
                                    'url': url,
                                    'target': 'current'
                                }

                            # return webbrowser.open_new(url)
                #
                    if invoice.partner_id.profile_id and invoice.partner_id.payment_id and invoice.partner_id.address_id:
                        if voucher.create_tran:
                            result = con.send_transaction(voucher, invoice)
                            print'result', result
                            is_authentication_successful = False
                            is_authentication_failed = False
                            if result.messages[0].message.text == 'Successful':
                                status = '1'
                                is_authentication_successful = True
                            else:
                                status = '2'
                                is_authentication_failed = True
                            s_id = sale_obj.search([('name', '=', invoice.origin)])
                            if s_id:
                                # s_id.write({'transaction_ids': [(0,0,{'payment_date':datetime.datetime.now(),'reason_text':result.transaction_response.response_reason_text,'status':status, 'partner_id':invoice.partner_id.id,'transaction_id': result.transaction_response.trans_id,'tran_amount': voucher.amount})]})

                                s_id.write({
                                    'is_authentication_successful': is_authentication_successful or False,
                                    'is_authentication_failed': is_authentication_failed or False,
                                    'transaction_ids': [(0, 0, {
                                    'cc_number': result.transaction_response['account_number'],
                                    'payment_date': datetime.datetime.now(),
                                    'reason_text': result.transaction_response.messages[0].message.description,
                                    'status': status, 'partner_id': invoice.partner_id.id,
                                    'transaction_id': result.transaction_response.trans_id,
                                    'tran_amount': voucher.amount})]})
                                # invoice.write({'auth_payment_ids': [(0,0,{'payment_date':datetime.datetime.now(),'reason_text':result.transaction_response.response_reason_text,'status':status, 'partner_id':invoice.partner_id.id,'transaction_id': result.transaction_response.trans_id,'tran_amount': voucher.amount})]})
                                invoice.write({'auth_payment_ids': [(0, 0, {'payment_date': datetime.datetime.now(),
                                                                            'reason_text':
                                                                                result.transaction_response.messages[
                                                                                    0].message.description,
                                                                            'status': status,
                                                                            'partner_id': invoice.partner_id.id,
                                                                            'transaction_id': result.transaction_response.trans_id,
                                                                            'tran_amount': voucher.amount})]})
                                self._cr.commit()
                    else:
                        raise ValidationError('Customer has no Authorized Profile Id,Payment_id,Address Id')
                # return super(account_payment, self).post()
































                # invoice_id.pay_and_reconcile(journal_id, invoice_id.amount_total, invoice_id.date_invoice)
                # pay_id.write({'communication':invoice_id.name,'journal_id':journal_id.id,'create_tran':True,'amount':invoice_id.amount_total})
        # data_inv = acc_inv_obj.read([inv_id], ['state'])
        # acc_obj = inv_id
        # res = acc_obj.invoice_pay_customer()

sale_order()

class sale_transaction(models.Model):
    _name = 'sale.transaction'

    order_id = fields.Many2one('sale.order', string='Order')
    cc_number=fields.Char('CC')
    partner_id = fields.Many2one('res.partner', string='Student')
    product_id = fields.Many2one('product.product', string='Lesson')
    invoice_id = fields.Many2one('account.invoice', string='Invoice')
    refund = fields.Boolean('Refund', readonly=True, default=False)
    void_transaction = fields.Boolean('Void', readonly=True)
    transaction_id = fields.Char(string='Transaction ID')
    tran_amount = fields.Float(string='Amount')
    payment_date = fields.Date(string='Payment Date')
    reason_text = fields.Text(string='Comment')
    status = fields.Selection([('1', 'Successful'),
                               ('2', 'Decline')], string='Status')

    @api.multi
    def refund_get(self):
        tran_obj = self[0]
        partner = tran_obj.order_id.partner_id
        if self._context.get('amt'):
        	amt = self._context.get('amt')
        else:
        	amt = tran_obj.tran_amount

        res_users_obj = self.env['res.users']
        acc_inv_obj = self.env['account.invoice']
        inv_id = tran_obj.order_id.invoice_ids[0].id
        invoice = acc_inv_obj.browse(inv_id)

        param = self.env['ir.config_parameter']
        auth_mode = param.browse((param.search([('key','=','authorize_mode')]))[0])
        mode = str(auth_mode.value)
        api_login = param.browse((param.search([('key','=','authorized_api_login_key')]))[0])
        login = str(api_login.value)
        tran_key = param.browse((param.search([('key','=','authorize_api_transaction_key')]))[0])
        key = str(tran_key.value)
        xml = """<?xml version="1.0" encoding="utf-8"?>
        <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
          <merchantAuthentication>
            <name>"""+ login +"""</name>
            <transactionKey>"""+ key +"""</transactionKey>
          </merchantAuthentication>
          <transaction>
            <profileTransRefund>
              <amount>"""+ str(amt)  +"""</amount>
              <customerProfileId>""" + partner.profile_id +"""</customerProfileId>
              <customerPaymentProfileId>""" + partner.payment_id + """</customerPaymentProfileId>
              <transId>"""+ tran_obj.transaction_id +"""</transId>
            </profileTransRefund>
          </transaction>
        </createCustomerProfileTransactionRequest>
        """
        if mode == 'test':
            request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml)
        else:
            request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
        request.add_header('Content-Type', 'text/xml')
        response = urllib2.urlopen(request).read()
        response = parseString(response)
        for node in response.getElementsByTagName("createCustomerProfileTransactionResponse"):
            for cnode in node.getElementsByTagName('messages'):
                for ccnode in cnode.childNodes:
                    if ccnode.nodeName =='resultCode':
                        res = ccnode.childNodes[0].data
                        if res == 'Ok':
                            if self._context.get('amt'):
                                print self.create({'invoice_id':inv_id, 'partner_id':tran_obj.order_id.partner_id.id, 'order_id':tran_obj.order_id.id, 'refund':True, 'transaction_id':tran_obj.transaction_id, 'tran_amount':context.get('amt'), 'payment_date':datetime.datetime.now()}, context=context)
                                print tran_obj.write({'tran_amount': tran_obj.tran_amount - context.get('amt')})
                            else:
                                tran_obj.write({'refund': True})
                            date = False
                            period = False
                            description = False
                            company = res_users_obj.browse(uid).company_id
                            journal_id = self.env['account.journal'].search([('type','=','sale_refund')])[0]
                            if invoice.state in ['draft', 'proforma2', 'cancel']:
                                raise ValidationError(('Cannot %s draft/proforma/cancel invoice.') % (mode))
                            if invoice.reconciled and mode in ('cancel', 'modify'):
                                raise ValidationError(('Cannot %s invoice which is already reconciled, invoice should be unreconciled first. You can only refund this invoice.') % (mode))
                            if invoice.period_id.id:
                                period = invoice.period_id.id
                            else:
                                period = invoice.period_id and invoice.period_id.id or False

                            if invoice.date_invoice:
                                date = invoice.date_invoice
                                if not invoice.period_id.id:
                                        self._cr.execute("select name from ir_model_fields \
                                                        where model = 'account.period' \
                                                        and name = 'company_id'")
                                        result_query = self._cr.fetchone()
                                        if result_query:
                                            self._cr.execute("""select p.id from account_fiscalyear y, account_period p where y.id=p.fiscalyear_id \
                                                and date(%s) between p.date_start AND p.date_stop and y.company_id = %s limit 1""", (date, company.id,))
                                        else:
                                            self._cr.execute("""SELECT id
                                                    from account_period where date(%s)
                                                    between date_start AND  date_stop  \
                                                    limit 1 """, (date,))
                                        res = self._cr.fetchone()
                                        if res:
                                            period = res[0]
                            else:
                                date = invoice.date_invoice
                            if invoice.name:
                                description = 'Refund'

                            if not period:
                                raise ValidationError('No period found on the invoice.')
                            if tran_obj.tran_amount != tran_obj.order_id.amount_total:
#                                        if amount != tran_obj.order_id.amount_total:
                                self._context.update({'amount': amt})
                                #context.update({'amount': amount})
                            refund_id = acc_inv_obj.refund([invoice.id], date, description, journal_id)
                        else:
                            for ccnode in cnode.childNodes:
                                if ccnode.nodeName =='message':
                                    for cccnode in ccnode.childNodes:
                                        if cccnode.nodeName =='text':
                                            res = cccnode.childNodes[0].data
                                            raise ValidationError(str(res))

        return True

    @api.multi
    def make_void_transaction(self):
        tran_obj = self[0]
        partner = tran_obj.order_id.partner_id

        param = self.env['ir.config_parameter']

        api_login = param.browse((param.search([('key','=','authorized_api_login_key')]))[0])
        login = str(api_login.value)
        tran_key = param.browse((param.search([('key','=','authorize_api_transaction_key')]))[0])
        key = str(tran_key.value)
        auth_mode = param.browse((param.search([('key','=','authorize_mode')]))[0])
        mode = str(auth_mode.value)

        xml = """<?xml version="1.0" encoding="utf-8"?>
                    <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                    <merchantAuthentication>
                        <name>"""+ login +"""</name>
                        <transactionKey>""" + key +"""</transactionKey>
                    </merchantAuthentication>
                    <transaction>
                        <profileTransVoid>
                            <customerProfileId>"""+ partner.profile_id + """</customerProfileId>
                            <customerPaymentProfileId>""" + partner.payment_id +"""</customerPaymentProfileId>
                            <transId>""" + tran_obj.transaction_id + """</transId>
                        </profileTransVoid>
                    </transaction>
</createCustomerProfileTransactionRequest>
        """
        if mode == 'test':
            request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml)
        else:
            request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
        request.add_header('Content-Type', 'text/xml')
        response = urllib2.urlopen(request).read()
        response = parseString(response)
        for node in response.getElementsByTagName("createCustomerProfileTransactionResponse"):
            for cnode in node.getElementsByTagName('messages'):
                for ccnode in cnode.childNodes:
                    if ccnode.nodeName =='resultCode':
                        res = ccnode.childNodes[0].data
                        if res == 'Ok':
                            tran_obj.write({'void_transaction': True})
                        else:
                            for ccnode in cnode.childNodes:
                                if ccnode.nodeName =='message':
                                    for cccnode in ccnode.childNodes:
                                        if cccnode.nodeName =='text':
                                            res = cccnode.childNodes[0].data
                                            raise ValidationError(str(res))
        return True


sale_transaction()
