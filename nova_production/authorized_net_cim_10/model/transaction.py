# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, api, fields,_


class payment_info(models.Model):
    _name = 'payment.info'

    name = fields.Char('CC Numbar')
    payment_id = fields.Char('Payment ID')
    partner_id = fields.Many2one('Customer Name')



class res_partner(models.Model):
	_inherit = 'res.partner'
        
        profile_id = fields.Char(string='Profile ID on Authorized.net', size=64)
        payment_id = fields.One2many('payment.info','partner_id',string='Payment ID on Authorized.net', size=64)
        address_id = fields.Char(string='Address ID on Authorized.net', size=64)


        
res_partner()


