# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import authorize

class authorize_configuration(object):
    def connect(self, login_id, transaction_key, mode):
        if mode=='test':
            authorize.Configuration.configure(
            authorize.Environment.TEST,
           login_id,
            transaction_key,
            )
        else:
            authorize.Configuration.configure(
            authorize.Environment.PRODUCTION,
           login_id,
            transaction_key,
            )
        
    def send_transaction(self, voucher, invoice,payment_id):
        lines = []
        if voucher.create_tran:
            partner = invoice.partner_id
            pro_id = partner.profile_id.strip()
            pay_id = payment_id.strip()
            # a_id = partner.address_id.strip()
            for line in invoice.invoice_line_ids:
                val = {
                        'item_id':line.product_id.default_code or '-',
                        'name':line.product_id.product_tmpl_id.name[:31] or ' ',
                        'description':line.name[:254] or ' ',
                        'quantity': line.quantity,
                        'unit_price': abs(line.price_unit),
                        'taxable': 'true'
                }
                lines.append(val)
            
            order = {
                    'invoice_number': invoice.number or ' ',
                    'description': invoice.name or ' ',
                    'order_number': invoice.origin or ' '
            }
            print'order',order
            print'order',lines
            # print'order,pro_id,pay_id,a_id',partner,pro_id,pay_id,a_id
            result = authorize.Transaction.sale({
                    'customer_id': pro_id, 
                    'payment_id': pay_id,
                    # 'address_id': a_id,
                    'amount': voucher.amount or 0.0,
                    'line_items': lines,
                    'order': order
                })
            
            return result
        
    def send_transaction_recurring(self, amount, invoice):
        lines = []
        partner = invoice.partner_id
        pro_id = partner.profile_id.strip()
        pay_id = partner.payment_id.strip()
        # a_id = partner.address_id.strip()
        for line in invoice.invoice_line:
            val = {
                    'item_id':line.product_id.default_code or '-',
                    'name':line.product_id.product_tmpl_id.name[:31] or ' ',
                    'description':line.name[:254] or ' ',
                    'quantity': line.quantity,
                    'unit_price': abs(line.price_unit),
                    'taxable': 'true'
            }
            lines.append(val)
        order = {
                'invoice_number': invoice.number or ' ',
                'description': invoice.name or ' ',
                'order_number': invoice.origin or ' '
        }
        result = authorize.Transaction.sale({
                'customer_id': pro_id, 
                'payment_id': pay_id,
                # 'address_id': a_id,
                'amount': amount or 0.0,
                'line_items': lines,
                'order': order
            })
        return result
        
        
    def create_customer(self, partner):
	fname=''
	lname=''
        if partner.is_company:
            type = 'business'
            name = partner.name
            cname = partner.name
        else:
            type = 'individual'
            l = len((partner.name).split(' '))
            if l >= 3:
                lname = (partner.name).split(' ')[l-2] +' '+ (partner.name).split(' ')[l-1]
                fname= (partner.name).split(' ')[0]
            elif l == 2:
                lname = (partner.name).split(' ')[l-1]
                fname = (partner.name).split(' ')[l-2]
            else:
                lname = ''
                fname = (partner.name).split(' ')[l-1]
                # name = (partner.name).split(' ')
            cname = partner.parent_id.name
        result = authorize.Customer.create({
                                        'email': partner.email or ' ',
                                        'description': partner.name or ' ',
                                        'customer_type': type,
                                        'billing': {
                                             # 'first_name': partner.name or ' ',
                                             'first_name': fname or ' ',
                                             'last_name': lname or ' ',
                                             # 'last_name': name or ' ',
                                             'company': cname or ' ',
                                             'address': partner.street or ' ',
                                             'city': partner.city,
                                             'state': partner.state_id.code or ' ',
                                             'zip': partner.zip or ' ',
                                             'country': partner.country_id.code or ' ',
                                             'phone_number': partner.phone and ''.join(e for e in partner.phone if e.isalnum()) or ' ',
                                             'fax_number': partner.fax or ' ',
                                     }
                                })
        print "=========result==============",result
        return result['customer_id']
                                
    # def create_address(self, partner):
	 # fname=''
	 # lname=''
    #      if partner.is_company:
    #         name = partner.name
    #         cname = partner.name
    #      else:
    #         l = len((partner.name).split(' '))
    #         if l >= 3:
    #             lname = (partner.name).split(' ')[l - 2] + ' ' + (partner.name).split(' ')[l - 1]
    #             fname = (partner.name).split(' ')[0]
    #         elif l == 2:
    #             lname = (partner.name).split(' ')[l - 1]
    #             fname = (partner.name).split(' ')[l - 2]
    #         else:
    #             lname = ''
    #             fname = (partner.name).split(' ')[l - 1]
    #         cname = partner.parent_id.name
    #
    #      result = authorize.Address.create(partner.profile_id, {
    #                                             'first_name': fname or ' ',
    #                                             'last_name': lname or ' ',
    #                                             'company': cname or ' ',
    #                                             'address': partner.street or ' ',
    #                                             'city': partner.city,
    #                                             'state': partner.state_id.code or ' ',
    #                                             'zip': partner.zip,
    #                                             'country': partner.country_id.code or ' ',
    #                                             'phone_number': partner.phone and ''.join(e for e in partner.phone if e.isalnum()) or ' ',
    #                                             'fax_number': partner.fax or ' ',
    #                                         })
    #      return result.address_id
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
