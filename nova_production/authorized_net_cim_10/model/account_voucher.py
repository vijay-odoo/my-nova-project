# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, api, fields,_
from odoo.exceptions import UserError , ValidationError
import urllib2
import os , sys, stat
import datetime
from datetime import date, timedelta
from xml.dom.minidom import parseString
from authorize_configuration import authorize_configuration
import webbrowser
import logging
_logger = logging.getLogger(__name__)

class account_payment(models.Model):
    _inherit = "account.payment"

    create_tran = fields.Boolean('Payment Through Authorized.net',default=True)

#     @api.multi
#     def post(self):
#         print "=============self=============",self
#         para = self.env["ir.config_parameter"]
#         ip_address = para.get_param("web.base.url")
#         acc_inv_obj = self.env['account.invoice']
#         payment_info_obj = self.env['payment.info']
#         tran_obj = self.env['sale.transaction']
#         voucher = self[0]
#         print "==========self._context.get==============",self._context.get('invoice_id')
#         if self._context.get('invoice_id'):
#             inv_id = self._context.get('invoice_id')
#         else:
#             inv_id = self._context.get('active_id',False)
#         if voucher.create_tran:
#             invoice = acc_inv_obj.browse(inv_id)
#             partner_obj = self.env['res.partner']
#             sale_obj = self.env['sale.order']
#             # param = self.env['ir.config_parameter']
#             param = self.env['authorized.configuration']
#             login = param.search([])[0].authorized_api_login_key
#             key = param.search([])[0].authorize_api_transaction_key
#             mode = param.search([])[0].authorize_mode
#             path = param.search([])[0].authorize_path
#             ip = param.search([])[0].authorize_ip_address
#             # api_login = param.search([('key','=','authorized_api_login_key')])
#             # login = str(api_login.value)
#             # tran_key = param.search([('key','=','authorize_api_transaction_key')])
#             # key = str(tran_key.value)
#             # auth_mode = param.search([('key','=','authorize_mode')])
#             # mode = str(auth_mode.value)
#             # ip_address = param.search([('key','=','authorize_ip_address')])
#             # ip = str(ip_address.value)
#             # auth_path = param.search([('key','=','authorize_path')])
#             # path = str(auth_path.value)
#             con = authorize_configuration()
#             con.connect(login, key, mode)
#             print "=======invoice.partner_id.profile_id===>",invoice.partner_id.profile_id
#             if not invoice.partner_id.profile_id:
#                 print "=====invoice.partner_id.email====>",invoice.partner_id
#                 print "=====invoice.partner_id.email====>",invoice.partner_id.email
#                 print "=====invoice.partner_id.name====>",invoice.partner_id.name
#                 if invoice.partner_id.email and invoice.partner_id.name:
#                     profile_id = con.create_customer(invoice.partner_id)
#                     invoice.partner_id.write({'profile_id': profile_id})
#                     self._cr.commit()
#                 else:
#                     raise ValidationError('Customer Must have email,name')
#
#             invoice = acc_inv_obj.browse(inv_id)
#
#             if not invoice.partner_id.address_id:
#                 if not invoice.origin:
#                     if invoice.partner_id.name and invoice.partner_id.street and invoice.partner_id.city and invoice.partner_id.state_id.code:
#                         addr_id = con.create_address(invoice.partner_id)
#                         invoice.partner_id.write({'address_id': addr_id})
#                         self._cr.commit()
#                     else:
#                         raise ValidationError('invoice partner Must have name,street,city,state')
#                 else:
#                     s_id = sale_obj.search([('name','=',invoice.origin)])
#                     s_obj = s_id[0]
#                     if not s_obj.partner_shipping_id.address_id:
#                         if s_obj.partner_shipping_id.name and s_obj.partner_shipping_id.street and s_obj.partner_shipping_id.city and s_obj.partner_shipping_id.state_id.code:
#                             addr_id = con.create_address(s_obj.partner_shipping_id)
#                             s_obj.partner_shipping_id.write({'address_id': addr_id})
#                             self._cr.commit()
#                         else:
#                             raise ValidationError('shipping partner Must have name,street,city,state')
#             invoice = acc_inv_obj.browse(inv_id)
#             if not invoice.partner_id.payment_id:
#                 xml1 = """<?xml version="1.0" encoding="utf-8"?>
#                     <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
#                     <merchantAuthentication>
#                             <name>"""+ login +"""</name>
#                             <transactionKey>"""+ key +"""</transactionKey>
#                     </merchantAuthentication>
#                     <customerProfileId>""" + invoice.partner_id.profile_id + """</customerProfileId>
#                         </getCustomerProfileRequest>
#                     """
#                 print'xml1',xml1
#                 if mode == 'test':
#                     request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml1)
#                 else:
#                     request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml1)
#                 request.add_header('Content-Type', 'text/xml')
#                 response = parseString(urllib2.urlopen(request).read())
#                 ex_date = ''
#                 card_number = ''
#                 for node in response.getElementsByTagName("getCustomerProfileResponse"):
#                     for cnode in node.childNodes:
#                         if cnode.nodeName =='profile':
#                             for ccnode in cnode.childNodes:
#                                 if ccnode.nodeName =='paymentProfiles':
#                                     for cccnode in ccnode.childNodes:
#                                         if cccnode.nodeName =='customerPaymentProfileId':
#                                             payment = cccnode.childNodes[0].data
#                                         if cccnode.nodeName == 'payment':
#                                             for payment_node in cccnode.childNodes:
#                                                 if payment_node.nodeName == 'creditCard':
#                                                     for cr in payment_node.childNodes:
#                                                         if cr.nodeName == 'cardNumber':
#                                                             card_number = cr.childNodes[0].data
#                                                         # if cr.nodeName == 'expirationDate':
#                                                         #     ex_date = cr.childNodes[1].data
#
#                                                         if payment:
#                                                             vals = {
#                                                                 'payment_id': payment,
#                                                                 'partner_id':invoice.partner_id.id,
#                                                                 'name':card_number
#
#                                                             }
#                                                             payment_info_obj.create(vals)
#                                                             self._cr.commit()
#                 invoice = acc_inv_obj.browse(inv_id)
#
#                 if not invoice.partner_id.payment_id:
#                     res_obj = self[0]
#                     xml ="""<?xml version="1.0" encoding="utf-8"?>
#                                 <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
#                                         <merchantAuthentication>
#                                                 <name>"""+ login +"""</name>
#                                                 <transactionKey>"""+key +"""</transactionKey>
#                                         </merchantAuthentication>
#                                         <customerProfileId>""" + invoice.partner_id.profile_id + """</customerProfileId>
#                                         <hostedProfileSettings>
#                                     </hostedProfileSettings>
#                             </getHostedProfilePageRequest>"""
#                     if mode == 'test':
#                         request = urllib2.Request("https://apitest.authorize.net/xml/v1/request.api", xml)
#                     else:
#                         request = urllib2.Request("https://api.authorize.net/xml/v1/request.api", xml)
#                     request.add_header('Content-Type', 'text/xml')
#                     response =urllib2.urlopen(request).read()
#                     response = parseString(response)
#                     token = ''
#                     for node in response.getElementsByTagName("getHostedProfilePageResponse"):
#                         for cnode in node.childNodes:
#                             if cnode.nodeName =='token':
#                                 token = cnode.childNodes[0].data
#                     if mode == 'test':
#                         action_url = "https://test.authorize.net/profile/manage"
#                     else:
#                         action_url = "https://secure.authorize.net/profile/manage"
#                     html_str = '''<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
#                                     <html>
#                                     <head>
#                                       <meta content="text/html; charset=ISO-8859-1"
#                                      http-equiv="content-type">
#                                       <title>Hello</title>
#
#                                     </head>
#                                     <body>
#                                     <form method="post" action='''+action_url +''' id="formAuthorizeNetPage" style="display:none;">',
#                                     <input type="hidden" name="Token" value="'''+token +'''"/>
#                                     </form>
#                                      <button type="button" onclick= "document.getElementById(
#         'formAuthorizeNetPage').submit();">Click to open Payment Form</button>
#                                     </body>
#                                     </html>'''
#                     cwd = os.getcwd()
#                     print"==============",cwd
#                     # _logger.info("%s Translation terms have been posted to Gengo successfully", (cwd))
#                     # path = "/opt/odoo-10.0-2017jan/odoo/addons/authorized_net_cim_10/static/src/"
#                     file = path + invoice.partner_id.profile_id+".html"
#                     # file = ip_address+"/static/src/" + invoice.partner_id.profile_id+".html"
#                     print "====file=======>",file
#                     # os.chmod(path, 0777)
#                     if not os.path.exists(path):
#                         os.makedirs (path)
#                     if os.path.exists(file):
#                         os.remove(file)
# #                    os.chmod(file, 0777)
#                     Html_file= open(file,"w")
#                     Html_file.write(html_str.encode('UTF-8'))
#                     Html_file.close()
#
#                     url = ip_address+'/authorized_net_cim_10/static/src/'+ invoice.partner_id.profile_id+".html"
#                     print "==============url==================", url
#                     return {
#                             'type': 'ir.actions.act_url',
#                             'url': url,
#                             'target': 'current'
#                     }
#                     # return webbrowser.open_new(url)
#
#         #     if invoice.partner_id.profile_id and invoice.partner_id.payment_id and invoice.partner_id.address_id:
#         #         if voucher.create_tran:
#         #             result = con.send_transaction(voucher, invoice)
#         #             print'result',result
#         #
#         #
#         #             if result.messages[0].message.text == 'Successful.':
#         #                 status = '1'
#         #             else:
#         #                 status = '2'
#         #                 invoice.partner_id.write({'payment_id':''})
#         #                 s_id = sale_obj.search([('name', '=', invoice.origin)])
#         #                 if s_id:
#         #                     s_id.write({'transaction_ids': [(0, 0, {
#         #                         'cc_number': result.transaction_response['account_number'],
#         #                         'payment_date': datetime.datetime.now(),
#         #                         'reason_text': result.transaction_response.messages[0].message.description,
#         #                         'status': status, 'partner_id': invoice.partner_id.id,
#         #                         'transaction_id': result.transaction_response.trans_id,
#         #                         'tran_amount': voucher.amount})]})
#         #                     invoice.write({'auth_payment_ids': [(0, 0, {'payment_date': datetime.datetime.now(),
#         #                                                                 'reason_text':
#         #                                                                     result.transaction_response.messages[
#         #                                                                         0].message.description,
#         #                                                                 'status': status,
#         #                                                                 'partner_id': invoice.partner_id.id,
#         #                                                                 'transaction_id': result.transaction_response.trans_id,
#         #                                                                 'tran_amount': voucher.amount})]})
#         #                     self._cr.commit()
#         #                     return True
#         #
#         #             s_id = sale_obj.search([('name','=',invoice.origin)])
#         #             if s_id:
#         #                 # s_id.write({'transaction_ids': [(0,0,{'payment_date':datetime.datetime.now(),'reason_text':result.transaction_response.response_reason_text,'status':status, 'partner_id':invoice.partner_id.id,'transaction_id': result.transaction_response.trans_id,'tran_amount': voucher.amount})]})
#         #                 s_id.write({'transaction_ids': [(0,0,{'cc_number':result.transaction_response['account_number'],'payment_date':datetime.datetime.now(),'reason_text':result.transaction_response.messages[0].message.description,'status':status, 'partner_id':invoice.partner_id.id,'transaction_id': result.transaction_response.trans_id,'tran_amount': voucher.amount})]})
#         #                 # invoice.write({'auth_payment_ids': [(0,0,{'payment_date':datetime.datetime.now(),'reason_text':result.transaction_response.response_reason_text,'status':status, 'partner_id':invoice.partner_id.id,'transaction_id': result.transaction_response.trans_id,'tran_amount': voucher.amount})]})
#         #                 invoice.write({'auth_payment_ids': [(0,0,{'payment_date':datetime.datetime.now(),'reason_text':result.transaction_response.messages[0].message.description,'status':status, 'partner_id':invoice.partner_id.id,'transaction_id': result.transaction_response.trans_id,'tran_amount': voucher.amount})]})
#         #                 self._cr.commit()
#         #     else:
#         #         raise ValidationError('Customer has no Authorized Profile Id,Payment_id,Address Id')
#         # return super(account_payment, self).post()
#

account_payment()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
