# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################
from openerp import fields ,models,api
import logging
_logger = logging.getLogger(__name__)
from openerp.exceptions import Warning

Selection=[
	('test', 'Testing Shipping'), 
	('production', 'Production Shipping')
]
class website_config_settings(models.TransientModel):	
	_inherit='base.config.settings'
	_name="fedex.config.settings"	
	fedex_key=fields.Char('key',required=True, size=64)
	fedex_password=fields.Char('Password',required=True, size=64)	
	fedex_account_no=fields.Char('Account No.',required=True, size=64)
	fedex_meter_no=fields.Char('Meter No.',required=True, size=64)
	fedex_integration_id=fields.Char('Integration Id',required=True, size=64)
	fedex_enviroment = fields.Selection(selection=Selection,required = 1,
									string = 'Environment',  default='test',
									help='Environment : Please keep it to be testing during the testing , before going live you can set it to   production')
	@api.model
	def get_default_fedex_values(self, fields):
		ir_values = self.env['ir.values']
		fedex_config_values_list_tuples = ir_values.get_defaults('fedex.config.settings')	
		fedex_config_values = {}
		for item in fedex_config_values_list_tuples:
			fedex_config_values.update({item[1]:item[2]})
		return fedex_config_values
	@api.multi
	def set_fedex_values(self):
		ir_values = self.env['ir.values']		
		for config in self:				
			ir_values.set_default('fedex.config.settings', 
				'fedex_key',	config.fedex_key or '')
			ir_values.set_default('fedex.config.settings', 
				'fedex_password',config.fedex_password or '')
			ir_values.set_default('fedex.config.settings', 
				'fedex_account_no',config.fedex_account_no or '')
			ir_values.set_default('fedex.config.settings', 
				'fedex_meter_no',config.fedex_meter_no or '')
			ir_values.set_default('fedex.config.settings', 
				'fedex_integration_id',config.fedex_integration_id or '')
			ir_values.set_default('fedex.config.settings', 
				'fedex_enviroment',config.fedex_enviroment or '')
		return True






