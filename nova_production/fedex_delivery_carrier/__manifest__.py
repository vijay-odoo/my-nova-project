# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Fedex Shipping Integration",
  "summary"              :  "Integrate Fedex shipping functionality directly within Odoo ERP applications to deliver increased logistical efficiencies.",
  "category"             :  "Website/Shipping Logistics",
  "version"              :  "1.0.2",
  "author"               :  "Webkul Software Pvt. Ltd.",
  "maintainer"           :  "Prakash Kumar",
  "website"              :  "https://store.webkul.com/Odoo-Website-Fedex-Shipping-Integration.html",
  "description"          :  "http://webkul.com/blog/odoo-fedex-shipping-integration/",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=fedex_delivery_carrier&version=10.0",
  "depends"              :  ['odoo_shipping_service_apps'],
  "data"                 :  [
                             'data/data.xml',
                             'data/delivery_demo.xml',
                             'security/ir.model.access.csv',
                             'views/fedex_delivery_carrier.xml',
                             'views/product_packaging.xml',
                             'views/res_config.xml',
                            ],
  "demo"                 :  [],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "price"                :  149,
  "currency"             :  "EUR",
  "pre_init_hook"        :  "pre_init_check",
  "external_dependencies":  {'python': ['fedex']},
}