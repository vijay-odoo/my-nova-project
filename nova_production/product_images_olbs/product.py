# -*- coding: utf-8 -*-
##############################################################################
#                                                                            #
#    Globalteckz Software Solutions and Services                             #
#    Copyright (C) 2016-2017 OpenERP Experts(<http://www.globalteckz.com>).  #
#                                                                            #
#    This program is free software: you can redistribute it and/or modify    #
#    it under the terms of the GNU Affero General Public License as          #
#    published by the Free Software Foundation, either version 3 of the      #
#    License, or (at your option) any later version.                         #
#                                                                            #
#    This program is distributed in the hope that it will be useful,         #  
#    but WITHOUT ANY WARRANTY; without even the implied warranty of          #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           #
#    GNU Affero General Public License for more details.                     #
#                                                                            #
#                                                                            #
##############################################################################
from odoo import models, fields, api, _
from odoo.tools.translate import _
import os

class product_product(models.Model):
    _inherit = "product.product"
#
#    def copy(selfdefault=None):
#        if not default:
#            default = {}
#        default.update({
#            'default_code': False,
#            'images_ids': False,
#        })
#        return super(product_product, self).copy( default, context=context)

#    def get_main_image(self):
#        if isinstance(id, list):
#            id = id[0]
#        images_ids = self.read(cr, uid, id, ['image_ids'], context=context)['image_ids']
#        if images_ids:
#            return images_ids[0]
#        return False
#    
#    def _get_main_image(self, cr, uid, ids, field_name, arg, context=None):
#        res = {}
#        img_obj = self.pool.get('product.images')
#        for id in ids:
#            image_id = self.get_main_image(cr, uid, id, context=context)
#            if image_id:
#                image = img_obj.browse(cr, uid, image_id, context=context)
#                res[id] = image.file
#            else:
#                res[id] = False
#        return res

   
    image_ids = fields.One2many(
            'product.images',
            'product_id',
            string='Product Images'
    )
    default_code = fields.Char(string='Reference', size=64, require='True')
    product_image = fields.Binary(compute='_get_main_image', method=True)


    
product_product()
