# -*- coding: utf-8 -*-

from odoo import models, api, fields


class sale_order(models.Model):
    _inherit = "sale.order"

    is_payment_padding = fields.Boolean('Payment padding', compute="_get_payment_status")

    @api.one
    def _get_payment_status(self):
        for order in self:
            invoice_ids = self.env['account.invoice'].search([('origin', '=', order.name), ('type', 'in', ['out_invoice'])])
            if invoice_ids:
                status = ['draft', 'proforma', 'proforma2', 'open']
                payment_padding = invoice_ids.filtered(lambda r: r.state in status)
                if payment_padding:
                    order.is_payment_padding = True
