# -*- coding: utf-8 -*-

{
    'name': 'Sales Order Payment Status',
    'version': '1.0',
    'category': 'Sales',
    'description': """
Sale Order Payment Status
====================================
    """,
    'depends': ['account', 'sale'],
    'data': ['views/sale_views.xml'],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
