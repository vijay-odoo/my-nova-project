# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################
from  collections import defaultdict
from odoo import api, fields, models, _
import logging
from odoo.exceptions import Warning, ValidationError, UserError

_logger = logging.getLogger(__name__)

APIUoM = [
    ('LB', 'LB'),
    ('KG', 'KG'),
    ('OZ', 'OZ')
]


class DeliveryCarrier(models.Model):
    _inherit = "delivery.carrier"
    @api.multi
    def _shipping_genrated_message(self,message):
        partial_id = self.env['wk.wizard.message'].create( {'text':message})
        return {
            'name':"Shipping information!",
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'wk.wizard.message',
            'res_id': partial_id.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
        }

    @api.model
    def get_extra_charges(self, order):
        if self.extra_price_source == 'fixed':
            return self.extra_service_price
        total_delivery = 0
        for line in order.order_line:
            if line.is_delivery:
                total_delivery += line.price_total
        return (order.amount_total - total_delivery) * self.extra_service_price / 100

    @api.multi
    def get_price_available(self, order):
        result = super(DeliveryCarrier, self).get_price_available(
            order) + self.get_extra_charges(order)
        return result

    @api.multi
    def get_shipping_price_from_so(self, orders):
        result = super(DeliveryCarrier,
                       self).get_shipping_price_from_so(orders)
        if result >= 0:
            result += self.get_extra_charges(orders)
            return [result]
        else:
            return [result]

    def _get_extra_price_source(self):
        return [('fixed', 'Fixed Amount'), ('percentage', '%  of Sale Order Amount')]
    _extra_price_source_selection = lambda self, * \
        args, **kwargs: self._get_extra_price_source(*args, **kwargs)
    image = fields.Binary(string='Website Image')
    extra_price_source = fields.Selection(selection=_extra_price_source_selection, string='Based On',
                                          default='fixed', required=True)
    extra_service_price = fields.Float(string='Charges',
                                       help="Extra  Added Service Charge ")

    void_shipment = fields.Boolean(
        string='Void Shipment', help="Enable it if want 'Shipment Cancellation'", default=True)
    uom_id = fields.Many2one(comodel_name='product.uom',
                             string='Odoo Product UoM', help='Shipping Equivalent UoM in Odoo')
    delivery_uom = fields.Selection(
        selection=APIUoM, string='API UoM', help='Default UoM in Odoo')
    packaging_id = fields.Many2one(comodel_name='product.packaging', string='Packaging')
    is_ground_delivery = fields.Boolean(string='Is Ground Delivery')
    free_delivery_above = fields.Float(string='Free Delivery Above', help="Free Delivery Above")

    @api.multi
    def _compute_ground_delivery(self):
        for res in self:
            if res.fedex_servicetype and res.fedex_servicetype.code == 'GROUND_HOME_DELIVERY':
                res.is_ground_delivery = True

    @api.onchange('fedex_servicetype')
    def _onchange_fedex_servicetype(self):
        for res in self:
            if res.fedex_servicetype and res.fedex_servicetype.code == 'GROUND_HOME_DELIVERY':
                res.is_ground_delivery = True
            else:
                res.is_ground_delivery = False

    @api.model
    def _get_config(self, key='delivery.carrier'):
        config_list_tuples = self.env[
            'ir.values'].get_defaults(key)
        config = {}
        for item in config_list_tuples:
            config.update({item[1]: item[2]})
        return config

    @api.model
    def _get_default_uom(self):
        uom_categ_id = self.env.ref('product.product_uom_categ_kgm').id
        return self.env['product.uom'].search([('category_id', '=', uom_categ_id), ('factor', '=', 1)], limit=1)  

    @api.model          
    def wk_group_by(self,group_by,items):        
        data = defaultdict(list)
        for item in items:
            data[item.get(group_by)].append(item)
        return data.items()  

    @api.model
    def _get_weight(self, order=None, pickings=None):
        weight, volume, quantity = 0, 0, 0
        items = order.order_line if order else pickings.move_lines
        for line in items:
            if order and line.state == 'cancel':
                continue
            if order and (not line.product_id or line.is_delivery):
                continue
            q = self._get_default_uom()._compute_quantity(
                line.product_uom_qty, self.uom_id)
            weight += (line.product_id.weight or 0.0) * q
            volume += (line.product_id.volume or 0.0) * q
            quantity += q
        if not self._context.get('ignore_weight'):
            if not weight:
                raise Warning('ERROR {0}:\nProduct in {0} Must Have Weight For Getting Shipping Charges.'.format(
                    order and order.name or pickings.name))
        return weight

    @api.model
    def update_order_package(self,items,order):
        package_obj =self.env['product.package']
        wk_packaging_ids=[]
        for item in items:
            item['order_id']=order.id
            wk_packaging_ids.append(package_obj.create(item).id)
        order.write({'wk_packaging_ids':[(6,0,wk_packaging_ids)]})
        return True
    @api.model
    def get_package_attribute(self,line,packaging_id,partial_package):
        product_qty,product_weight=1,1
        if not partial_package:
            product_id = line.product_id
            product_qty,product_weight = int(line.product_uom_qty) and int(line.product_uom_qty) or 1,product_id.weight and product_id.weight or 1 
        else:
            product_weight = sum(map(lambda item:item.get('weight'),line))
        return  product_qty,product_weight 



    @api.model
    def wk_get_product_package(self,line,packaging_id,partial_package=None):
        """Return package count 
            major package [package_count,capacity], minor package [package_count=1,capacity]

        """
        dimension=dict(height=packaging_id.height,width=packaging_id.width,length=packaging_id.length,packaging_id=packaging_id.id)
        result=list()        
        product_qty,product_weight = self.get_package_attribute(line,packaging_id,partial_package)
        max_qty, max_weight = int(packaging_id.qty) and int(packaging_id.qty) or 1 ,packaging_id.max_weight  and packaging_id.max_weight or 1              
        qty_capacity = int(max_weight//product_weight)
        if qty_capacity and     qty_capacity<max_qty:
            # _logger.info("Condition-A -")
            package_count=product_qty/qty_capacity
            if package_count:
                # _logger.info("Condition-A1 -")
                multi_pckg_qty_capacity = dimension.copy()
                multi_pckg_qty_capacity.update(dict(
                    weight=product_weight*qty_capacity,
                    full_capacity=True
                ))
             
                result+=[multi_pckg_qty_capacity]*package_count

            if product_qty%qty_capacity:
                # _logger.info("Condition-A2 -")
                single_pckg_qty_capacity = dimension.copy()
                single_pckg_qty_capacity.update(dict(
                    weight=product_weight*(product_qty%qty_capacity),
                    full_capacity=False
                ))
                result+=[single_pckg_qty_capacity]*1            
                
        else:
            # _logger.info("-Condition-B---")            
            package_count=product_qty/max_qty
            if package_count:
                # _logger.info("-Condition-B1---")            

                multi_pckg_max_qty =dimension.copy()
                multi_pckg_max_qty.update(dict(
                    weight = product_weight*max_qty,
                    full_capacity=True
                    ))
                result+=[multi_pckg_max_qty]*package_count
            if product_qty%max_qty:
                # _logger.info("-Condition-B2---")          
                single_pckg_max_qty=dimension.copy()
                single_pckg_max_qty.update(dict(
                    weight = product_weight*(product_qty%max_qty),
                    full_capacity=False
                    ))
                result+=[single_pckg_max_qty]*1

        return result

    @api.model
    def wk_validate_data(self,order=None,pickings=None):
        if pickings:
            if not pickings.package_ids:
                raise Warning('Create the package before sending to shipper.')
            else:
                package_ids=pickings.package_ids.filtered(lambda package_id:not package_id.packaging_id)
                if len(package_ids):
                    raise Warning('Packaging is not set for package %s.'%(','.join(package_ids.mapped('name'))))
    @api.model
    def wk_get_packaging_id(self,product_id=None,package_id=None):
        pck_id=self.packaging_id 
        packaging_id=None
        if product_id:
            packaging_ids = product_id.wk_packaging_ids.filtered(lambda pck_id:pck_id.package_carrier_type==self.delivery_type)
            packaging_id = packaging_ids and packaging_ids[0]  or pck_id
        elif package_id:
            packaging_id = package_id.packaging_id or pck_id
        
        if packaging_id: return packaging_id            
        raise Warning('Packaging is not set of product and carrier as well.')


    @api.model
    def wk_group_by_packaging(self,order=None,pickings=None):
        packagings = defaultdict(list)
        if order:
            for line in order.order_line:
                if  line.state == 'cancel':
                    continue
                product_id = line.product_id
                if (not product_id or line.is_delivery):
                    continue            
                packaging_id =self.wk_get_packaging_id(product_id=product_id)
                packagings[packaging_id].append(line)                
               
        else:
            for package_id in pickings.package_ids:
                packaging_id =self.wk_get_packaging_id(package_id=package_id)
                packagings[packaging_id].append(package_id)     

        return dict(packagings.items()) 

    @api.model
    def wk_merge_half_package(self,items):
        data = defaultdict(list)
        for item in filter(lambda item:not item.get('full_capacity'),items):
            packaging_id = item.get('packaging_id')
            if type(packaging_id)==int:
                packaging_id =self.env['product.packaging'].browse(packaging_id)  
            data[packaging_id].append(item)
        new_dict=dict()
        for key,value  in data.items():
            if len(value)>1:
                new_dict[key]=value
                for val in value: 
                    items.remove(val)
        # _logger.info("new_dict-packagings--%r-----",new_dict)
        for packaging_id,lines in new_dict.items():
            items.extend(self.wk_get_product_package(lines,packaging_id,partial_package=True))
        return items

    @api.model
    def wk_get_order_package(self,order):
        result=[]   
        if order.create_package=='auto':
            packagings =self.wk_group_by_packaging(order)

            # _logger.info("-packagings--%r-----",packagings)
            for packaging_id,lines in packagings.items():
                for line in lines:
                    result.extend(self.wk_get_product_package(line,packaging_id)) 
            # _logger.info("Before merge %r",result)
            result=self.wk_merge_half_package(result)
            # _logger.info("After merge %r",result) 

            self.update_order_package(result,order)  
        else:
            return map(lambda wk_packaging_id:dict(
                packaging_id = wk_packaging_id.packaging_id.id,
                weight = wk_packaging_id.weight,
                width=wk_packaging_id.width,
                length=wk_packaging_id.length,
                height=wk_packaging_id.height
                ),order.wk_packaging_ids)

        return result



    @api.model
    def get_package_count(self, weight_limit, order=None, pickings=None):
        WeightValue = self._get_weight(
            order=order) if order else self._get_weight(pickings=pickings)
        assert (WeightValue != 0.0), _(
            'Product in Order Must Have Weight For Getting Shipping Charges.')
        last_package = WeightValue % weight_limit
        total_package = int(WeightValue // weight_limit)
        return WeightValue, weight_limit, last_package, total_package + int(bool(last_package))    

    @api.model
    def _get_api_weight(self, shipping_weight):
        q = self._get_default_uom()._compute_quantity(
            1, self.uom_id)
        weight = (shipping_weight or 0.0) * q
        return int(weight)

    @api.model
    def _get_per_order_line_weight(self, line):
        return (line.product_id.weight or 0.0) * self._get_default_uom()._compute_quantity(line.product_uom_qty, self.uom_id)

    def get_shipment_currency(self,order=None, pickings=None):
        currency = 'USD'
        if order:
            currency = order.currency_id.name
        elif pickings:
            if pickings.sale_id.currency_id:
                currency = pickings.sale_id.currency_id.name
            else:
                currency = pickings.company_id.currency_id.name
        return currency