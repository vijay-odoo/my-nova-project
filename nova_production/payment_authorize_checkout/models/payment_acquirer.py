# -*- coding: utf-'8' "-*-"

import logging
import urllib2
from operator import itemgetter
from urllib import urlencode

from openerp import api, fields, models, _
from openerp.tools.float_utils import float_compare
from openerp.addons.payment.models.payment_acquirer import ValidationError

_logger = logging.getLogger(__name__)
_known_tuple_types = {}


class AcquirerAuthorizeAIM(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('authorize_checkout', 'Authorize Checkout (AIM)')])
    authorize_login_id = fields.Char(
        string='API Login Id', required_if_provider='authorize',
        help='The merchant’s valid API login ID. '
             'Submit the API login ID used to submit transactions.')
    authorize_txn_key = fields.Char(
        string='API Transaction Key',
        required_if_provider='authorize',
        help='The merchant’s valid transaction key. Submit the transaction '
             'key obtained by the merchant from the Merchant Interface.')

    def _get_authorize_checkout_urls(self):
        """ Authorize URLs """
        if self.environment == 'prod':
            return {'authorize_checkout_url': 'https://secure.authorize.net/gateway/transact.dll'}
        else:
            return {'authorize_checkout_url': 'https://test.authorize.net/gateway/transact.dll'}

    @api.multi
    def authorize_checkout_compute_fees(self, amount, currency_id, country_id):
        if not self.fees_active:
            return 0.0
        country = self.env['res.country'].browse(country_id)
        if country and self.company_id.country_id.id == country.id:
            percentage = self.fees_dom_var
            fixed = self.fees_dom_fixed
        else:
            percentage = self.fees_int_var
            fixed = self.fees_int_fixed
        fees = (percentage / 100.0 * amount + fixed) / (1 - percentage / 100.0)
        return fees

    @api.multi
    def authorize_checkout_form_generate_values(self, tx_values):
        payflow_tx_values = dict(tx_values)
        payflow_tx_values.update({
            'currency_code': tx_values.get('currency') and tx_values.get('currency').name,
        })
        if self.fees_active:
            payflow_tx_values['handling'] = '%.2f' % payflow_tx_values.pop('fees', 0.0)
        return payflow_tx_values

    @api.multi
    def authorize_checkout_get_form_action_url(self):
        # Override tx_url divert to controller for getting credit card information
        authorize_url = '/shop/authorize'
        return authorize_url

    @api.multi
    def get_secure_transaction(self, vals):
        response_result = []
        results = []
        txn_params = {}
        delimiter = '|'
        auth_aim_url = self._get_authorize_checkout_urls()

        authorize_credential = {
            'x_login': self.authorize_login_id,
            'x_tran_key': self.authorize_txn_key,
        }
        base_params = dict(authorize_credential)
        base_params.update({
            'x_version': '3.1',
            'x_test_request': 'TRUE' if not self.environment == 'prod' else 'FALSE',
            'x_delim_char': ',',
            'x_delim_data': 'TRUE',
            'x_relay_response': 'FALSE',
            'x_encap_char': delimiter,
            'x_type': 'AUTH_CAPTURE',
            'x_market_type': 0,
            'x_duplicate_window': 180
        })
        transaction_params = dict(base_params)
        transaction_params.update({
            'x_amount': float(vals.get('amount')) + float(vals.get('handling', 0.0)),
            'x_currency_code': vals.get('currency_code'),
            'x_card_num': vals.get('cc_number').replace(" ", ""),
            'x_exp_date': vals.get('cc_expiry').replace(" ", ""),
            'x_card_code': vals.get('cc_cvc'),
            'x_invoice_num': vals.get('reference')
        })
        billing_params = dict(transaction_params)
        billing_params.update({
            'x_first_name': vals.get('bill_to_first_name'),
            'x_last_name': vals.get('bill_to_last_name'),
            'x_address': vals.get('bill_to_address1'),
            'x_city': vals.get('bill_to_city'),
            'x_state': vals.get('bill_to_state'),
            'x_zip': vals.get('bill_to_zip'),
            'x_country': vals.get('bill_to_country'),
            'x_phone': vals.get('bill_to_phone'),
            'x_email': vals.get('bill_to_email'),
            'x_cust_id': vals.get('bill_to_customer')
        })
        shipping_params = dict(billing_params)
        shipping_params.update({
            'x_ship_to_first_name': vals.get('ship_to_first_name'),
            'x_ship_to_last_name': vals.get('ship_to_last_name'),
            'x_ship_to_address': vals.get('ship_to_address1'),
            'x_ship_to_city': vals.get('ship_to_city'),
            'x_ship_to_state': vals.get('ship_to_state'),
            'x_ship_to_zip': vals.get('ship_to_zip'),
            'x_ship_to_country': vals.get('ship_to_country'),
        })
        encoded_args = urlencode(shipping_params)

        error = []
        try:
            request = urllib2.Request(auth_aim_url['authorize_checkout_url'], encoded_args)
            response = urllib2.urlopen(request)
            response_result += str(response.read()).split(delimiter)
        except Exception, e:
            error.append('Authorize transaction request failed - %s', e)
            _logger.exception('Authorize transaction request failed - %s', e)
        finally:
            response.close()

        results_data = self.namedtuple('Results', 'First \
            ResponseCode ResponseSubcode ResponseReasonCode ResponseText \
            AuthCode AVSResponse TransactionID \
            InvoiceNumber Description Amount PaymentMethod TransactionType CustomerID \
            FirstName LastName Company \
            BillingAddress BillingCity BillingState BillingZip BillingCountry \
            Phone Fax Email \
            ShippingFirstName ShippingLastName ShippingCompany ShippingAddress ShippingCity \
            ShippingState ShippingZip ShippingCountry \
            TaxAmount DutyAmount FreightAmount TaxExemptFlag PONumber MD5Hash \
            CVVResponse CAVVResponse AccountNumber CardType')
        response_result = [x for x in response_result if x != ',']
        results = results_data(*tuple(r for r in response_result))

        res = dict()
        if results.ResponseCode in ['1', '4']:
            res.update({
                'ResponseCode': results.ResponseCode,
                'ResponseText': results.ResponseText,
                'AuthCode': results.AuthCode,
                'TransactionID': results.TransactionID,
                'InvoiceNumber': results.InvoiceNumber,
                'Amount': results.Amount,
                'CustomerID': results.CustomerID
            })
        elif results.ResponseCode in ['2', '3']:
            message = 'Your credit card was declined by your bank: %s' % results.ResponseText
            _logger.warning(message)
            error.append(message)
            txn_params.update({
                'acquirer_id': self.id,
                'reference': vals.get('reference'),
                'amount': float(vals.get('amount')) + float(vals.get('handling', 0.0)),
                'handling': vals.get('handling'),
                'currency_code': vals.get('currency_code'),
                'errors': error
            })
        return results, res, txn_params

    def _split_partner_name(self, partner_name):
        return [' '.join(partner_name.split()[-1:]), ' '.join(partner_name.split()[:-1])]

    def namedtuple(self, typename, fieldnames):
        """
        >>> import namedtuples
        >>> tpl = namedtuples.namedtuple(['a', 'b', 'c'])
        >>> tpl(1, 2, 3)
        (1, 2, 3)
        >>> tpl(1, 2, 3).b
        2
        >>> tpl(c=1, a=2, b=3)
        (2, 3, 1)
        >>> tpl(c=1, a=2, b=3).b
        3
        >>> tpl(c='pads with nones')
        (None, None, 'pads with nones')
        >>> tpl(b='pads with nones')
        (None, 'pads with nones', None)
        >>>
        """
        # Split up a string, some people do this
        if isinstance(fieldnames, basestring):
            fieldnames = fieldnames.replace(',', ' ').split()
        # Convert anything iterable that enumerates fields to a tuple now
        fieldname_tuple = tuple(str(field) for field in fieldnames)
        # See if we've cached this
        if fieldname_tuple in _known_tuple_types:
            return _known_tuple_types[fieldname_tuple]
        # Make the type
        new_tuple_type = type(typename, (NamedTupleBase,), {})
        # Set the hidden field
        new_tuple_type._fields = fieldname_tuple
        # Add the getters
        for i, field in enumerate(fieldname_tuple):
            setattr(new_tuple_type, field, property(itemgetter(i)))
        # Cache
        _known_tuple_types[fieldname_tuple] = new_tuple_type
        # Done
        return new_tuple_type


class NamedTupleBase(tuple):
    """Base class for named tuples with the __new__ operator set, named tuples
       yielded by the namedtuple() function will subclass this and add
       properties."""
    def __new__(cls, *args, **kws):
        """Create a new instance of this fielded tuple"""
        # May need to unpack named field values here
        if kws:
            values = list(args) + [None] * (len(cls._fields) - len(args))
            fields = dict((val, idx) for idx, val in enumerate(cls._fields))
            for kw, val in kws.iteritems():
                assert kw in kws, "%r not in field list" % kw
                values[fields[kw]] = val
            args = tuple(values)
        return tuple.__new__(cls, args)


class AuthorizeAIMPaymentTransaction(models.Model):
    _inherit = 'payment.transaction'

    authorize_res_text = fields.Char('Response Text', help='Response Reason Text details '
                                     'the specific reason for the transaction status.')

    @api.model
    def _authorize_checkout_form_get_tx_from_data(self, data):
        reference = data.get('InvoiceNumber')
        if not reference:
            error_msg = _('Authorize: received data with missing InvoiceNumber (%s)') % (reference)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        txt_number = data.get('TransactionID')
        if not txt_number:
            error_msg = _('Authorize: received data with missing TransactionID (%s)') % (txt_number)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        tx_ids = self.env['payment.transaction'].search([('reference', '=', reference)])
        if not tx_ids or len(tx_ids) > 1:
            error_msg = 'Authorize Advance Inegration: received data for reference %s' % (reference)
            if not tx_ids:
                error_msg += ': no order found'
            else:
                error_msg += ': multiple order found'
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return tx_ids

    @api.model
    def _authorize_checkout_form_get_invalid_parameters(self, data):
        invalid_params = []
        # Check reference order number
        invoice_number = data.get('InvoiceNumber')
        if self.acquirer_reference and invoice_number != self.acquirer_reference:
            invalid_params.append(('InvoiceNumber', invoice_number, self.acquirer_reference))

        # Check transaction amount
        if float_compare(float(data.get('Amount', '0.0')), (self.amount + self.fees), 2) != 0:
            invalid_params.append(('Amount', data.get('Amount'), '%.2f' % self.amount))
        return invalid_params

    @api.model
    def _authorize_checkout_form_validate(self, data):
        response = data.get('ResponseCode')
        tx_vals = {
            'acquirer_reference': data.get('TransactionID'),
            'authorize_res_text': data.get('ResponseText')
        }

        if response == '1':
            state_msg = 'Authorize Approved: for %s transaction' % self.reference
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'done',
                'state_message': _(state_msg),
                'date_validate': fields.datetime.now()
            })
            return self.write(tx_vals)
        elif response == '3':
            state_msg = 'Authorize Error: for %s transaction.' % self.reference
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'error',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
        elif response == '4':
            state_msg = 'Authorize Held for review: for %s transaction.' % self.reference
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'pending',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
        else:
            state_msg = 'Authorize Unrecognized Response: for %s transaction.' % self.reference
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'error',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
