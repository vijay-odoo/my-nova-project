# -*- coding: utf-8 -*-

import logging
import werkzeug

from openerp import http
from openerp.http import request

_logger = logging.getLogger(__name__)


class AuthorizeAIMController(http.Controller):
    def customer_data(self, order, PaymentAcquirer):
        bill_vals = order.partner_invoice_id
        ship_vals = False
        if order and (order.partner_shipping_id != order.partner_invoice_id):
            ship_vals = order.partner_shipping_id
        bill_information = {
            'bill_to_first_name': PaymentAcquirer._split_partner_name(bill_vals.name)[0],
            'bill_to_last_name': PaymentAcquirer._split_partner_name(bill_vals.name)[1],
            'bill_to_address1': bill_vals.street,
            'bill_to_city': bill_vals.city,
            'bill_to_state': bill_vals.state_id.code or '',
            'bill_to_zip': bill_vals.zip or '',
            'bill_to_country': bill_vals.country_id.name,
            'bill_to_email': bill_vals.email or '',
            'bill_to_phone': bill_vals.phone,
            'bill_to_customer': bill_vals.id,
            'ship_to_same_as_bill': True if not ship_vals else False
        }
        values = dict(bill_information)
        if ship_vals:
            ship_information = {
                'ship_to_first_name': PaymentAcquirer._split_partner_name(ship_vals.name)[0],
                'ship_to_last_name': PaymentAcquirer._split_partner_name(ship_vals.name)[1],
                'ship_to_address1': ship_vals.street,
                'ship_to_city': ship_vals.city,
                'ship_to_state': ship_vals.state_id.code or '',
                'ship_to_zip': ship_vals.zip or '',
                'ship_to_country': ship_vals.country_id.name,
                'ship_to_same_as_bill': False
            }
            values.update(ship_information)
        return values

    @http.route(['/shop/authorize'], type='http', auth="public", website=True)
    def authorize_checkout_payment(self, **post):
        PaymentAcquirer = request.env['payment.acquirer']
        PaymentAcquirer = PaymentAcquirer.browse(int(post.get('acquirer_id', 0)))
        order = request.website.sale_get_order()
        values = self.customer_data(order, PaymentAcquirer)
        values.update({
            'website_sale_order': order,
            'amount': post.get('amount'),
            'handling': post.get('handling'),
            'reference': post.get('reference'),
            'currency_code': post.get('currency_code'),
            'acquirer_id': post.get('acquirer_id'),
            'authorize_url': '/payment/authorize_checkout/get_secure_transaction',
        })
        return request.render("payment_authorize_checkout.authorize_payment", values)

    @http.route('/payment/authorize_checkout/get_secure_transaction', type='http',
                auth="public", website=True)
    def authorize_checkout_get_token(self, **post):
        PaymentAcquirer = request.env['payment.acquirer']
        PaymentAcquirer = PaymentAcquirer.browse(int(post.get('acquirer_id', 0)))
        params, res, txn_params = PaymentAcquirer.get_secure_transaction(post)
        if txn_params:
            order = request.website.sale_get_order()
            values = self.customer_data(order, PaymentAcquirer)
            values.update(txn_params)
            values.update({'website_sale_order': order})
            return request.render("payment_authorize_checkout.authorize_payment", values)
        response = res.get('ResponseCode')
        if response in ['1', '4']:
            res = request.env['payment.transaction'].sudo().form_feedback(res, 'authorize_checkout')
        return werkzeug.utils.redirect('/shop/payment/validate')
