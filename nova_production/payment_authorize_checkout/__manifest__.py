# -*- coding: utf-8 -*-
#############################################
#  Copyright (c) 2015-Present Forfens Tech.
#############################################

{
    'name': 'Website Authorize Payment Acquirer',
    'version': '1.0',
    'summary': 'Authorize Checkout - Advanced Integration Method (AIM)',
    'description': """
Advanced Integration Method(AIM) is Authorize.Net's recommended connection method and offers \
the most secure and flexible integration for all types of transactions, including mobile, \
websites and other business applications.
    """,
    'category': 'Website',
    'author': 'Forfens Tech',
    'images': ['static/description/main_screenshot.png'],
    'depends': ['website_sale', 'payment', 'website_payment'],
    'data': [
        'views/payment_acquirer_views.xml',
        'views/payment_acquirer_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'support': 'forfens.com@gmail.com',
    'installable': True,
    'application': True,
    'price': 79,
    'currency': 'EUR',
    'live_test_url': '',
}
