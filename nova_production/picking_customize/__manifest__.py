# -*- coding: utf-8 -*-

{
    'name': 'Inventory (Put in Pack)',
    'version': '1.0',
    'category': 'Inventory',
    'description': """
Inventory (Put in Pack)
====================================
    """,
    'depends': ['sale', 'stock'],
    'data': ['views/picking_views.xml'],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
