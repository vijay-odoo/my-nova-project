# -*- coding: utf-8 -*-

from odoo import models, api, fields, _
from odoo.exceptions import UserError


class stock_picking(models.Model):
    _inherit = "stock.picking"

    is_put_in_pack = fields.Boolean('is put in pack')

    @api.multi
    def put_in_pack(self):
        res = super(stock_picking, self).put_in_pack()
        for rec in self:
            rec.is_put_in_pack = True
        return res

    @api.multi
    def do_new_transfer(self):
        if self.is_put_in_pack:
            return super(stock_picking, self).do_new_transfer()
        else:
            raise UserError(_('Please Put in Pack is Mandatory after You can Validate.'))
