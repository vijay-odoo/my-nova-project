# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################
from collections import defaultdict
from dateutil.parser import parse

import logging
from odoo import api, fields, models, _
from odoo.exceptions import  ValidationError#, Warning
_logger = logging.getLogger(__name__)
try:
    from fedex.services.track_service import FedexTrackRequest
    from fedex.base_service import FedexError, SchemaValidationError, FedexFailure
    from fedex.config import FedexConfig
except Exception as e:
    _logger.error("#WKDEBUG-1  python  fedEx library not installed .")
fedex_provider_tracking_link = "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber="
class Website(models.Model):
    _inherit = "website"
    @api.model
    def website_track_shipment(self, tracknumbers,delivery_type):
        delivery = self.env['delivery.carrier']
        if hasattr(delivery, 'website_%s_track_shipment' % delivery_type):
            return getattr(delivery, 'website_%s_track_shipment' % delivery_type)(tracknumbers)

class FedexDeliveryCarrier(models.Model):
    _inherit = "delivery.carrier"

    @api.model
    def website_fedex_track_shipment(self, tracknumbers):
        event_details = dict()
        for tracking_ref in filter(lambda t:len(t.strip()),tracknumbers):#,789314620989,'789382417259','789308795285']:

            customer_transaction_id = "*** TrackService Request v10 using Python ***"  # Optional transaction_id
            track = FedexTrackRequest(self.config_fedex(), customer_transaction_id=tracking_ref)

            # Track by Tracking Number
            track.SelectionDetails.PackageIdentifier.Type = 'TRACKING_NUMBER_OR_DOORTAG'
            track.SelectionDetails.PackageIdentifier.Value =tracking_ref
            track.ProcessingOptions='INCLUDE_DETAILED_SCANS'
            track.send_request()
            for match in track.response.CompletedTrackDetails[0].TrackDetails:
                tracking_number = match.TrackingNumber
                event_details[tracking_number] = dict(error_message='',tracking_details = [],status_data=dict())
                if hasattr(match, 'Notification'):
                    severity=match.Notification.Severity
                    if severity!='SUCCESS':
                        err=''
                        # err+=_(" Notification Severity: {}".format(match.Notification.Severity))
                        err+=_(" Error Code: {}".format(match.Notification.Code))
                        err+=_(" {}".format(match.Notification.Message))
                        event_details[tracking_number]['error_message']=err

                if hasattr(match, 'Events'):
                    if hasattr(match, 'StatusDetail'):
                        status = match.StatusDetail
                        status_message ='%s'%(status.Description)
                        delivery_signature=''
                        if hasattr(match, 'DeliverySignatureName'):
                            delivery_signature = match.DeliverySignatureName
                        event_details[tracking_number]['status_data']=dict(
                            status=status_message,
                            delivery_signature=delivery_signature
                        )

                    match_details = defaultdict(list)
                    for event_match in match.Events:
                        event_date = (event_match.Timestamp)
                        day_data =((event_date.strftime("%d/%m/%Y %A")))
                        tmp_data = dict(event_match)
                        tmp_data['Timestamp'] = event_date.strftime("%H:%M:%p")
                        tmp_data['Address'] = dict(event_match.Address)
                        match_details[day_data].append(tmp_data)
                        # if hasattr(event_match, 'StatusExceptionDescription'):
                        #     vals['exception_description'] = event_match.StatusExceptionDescription
                    event_details[tracking_number]['tracking_details'].append(match_details.items())
        return dict(
            result = event_details.items()
        )
