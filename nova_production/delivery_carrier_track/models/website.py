# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
##########################################################################
from odoo import api, fields, models, _
class Website(models.Model):
    _inherit = "website"
    @api.model
    def website_track_shipment(self, tracknumbers,delivery_type):
        delivery = self.env['delivery.carrier']
        if hasattr(delivery, 'website_%s_track_shipment' % delivery_type):
            return getattr(delivery, 'website_%s_track_shipment' % delivery_type)(tracknumbers)
