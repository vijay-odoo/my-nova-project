# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# License URL <https://store.webkul.com/license.html/>
#################################################################################
import logging
from odoo import http
from odoo.http import request
from odoo.addons.website.controllers.main import Website
_logger = logging.getLogger(__name__)

class Website(Website):
    @http.route(['/delivery/track'], type='http', methods=['GET', 'POST'], auth="public", website=True)
    def delivery_track(self, **post):

        tracknumbers  = post.get('tracknumbers')
        values={
            'tracknumbers':tracknumbers,
            'message':''
        }
        if tracknumbers:
            tracking_results = request.website.website_track_shipment(post.get('tracknumbers').split(','),delivery_type='fedex')
            # _logger.info("tracking_results==%r==%r"%(post,tracking_results))
            # tracking_results = {'result': {123: {'tracking_details': [], 'error_message': u' Notification Severity: ERROR Notification Code: 6035 Notification Message: Invalid tracking numbers.   Please check the following numbers and resubmit.'}}}
            values['message']+=tracking_results.get('message','')
            values['tracking_results']=tracking_results.get('result') or [],
        _logger.info("11values==%r==%r"%(post,values))

        return request.render('delivery_carrier_track.track', values)
